SRCROOT = $(CURDIR)
include Makefile.common

build:
	$(MAKE) -C mozjs17.0.0
	$(MAKE) -C src $@
	$(MAKE) -C po $@

ifeq ($(SVNVER),exported)
# These rules can only be called from inside an exported tree
install:
	install -d $(DESTDIR)$(libdir)/mw
	cp -a $(INSTALLFILES) $(DESTDIR)$(libdir)/mw/
	install -d $(DESTDIR)$(initddir)
	install mwserv.init $(DESTDIR)$(initddir)/mwserv
	install mozjs/$(libdir)/libmozjs-17.0.so $(DESTDIR)$(libdir)
	$(MAKE) -C src $@
	$(MAKE) -C po $@

deb:
	cp -a debian-template debian
	dch --create --package mw3 -v $(VERSION) -c debian/changelog "Build mw3 $(VERSION) package"
	debuild -e VERSION_TWEAK=$(VERSION_TWEAK) -us -uc -b

tarball:
	tar zchvf ../$(MWVERSION).tar.gz -C .. $(MWVERSION)

rpm: tarball
	rpmbuild -ta ../$(MWVERSION).tar.gz

else
# These rules can only be called from an svn working directory
ifdef TMPDIR
# A temp dir exists so svn export and palm off the invocation to the exported makefile
export:
	rm -rf $(TMPDIR)/$(MWVERSION)
	git checkout-index -a -f --prefix=$(TMPDIR)/$(MWVERSION)/
	# Store the svn rev so we don't need to use svnversion or sed any more
	echo $(VERSION_TWEAK) > $(TMPDIR)/$(MWVERSION)/mw.rev

install tarball rpm: export
	$(MAKE) -C $(TMPDIR)/$(MWVERSION) $@
	rm -rf $(TMPDIR)/$(MWVERSION)

deb: export
	$(MAKE) -C $(TMPDIR)/$(MWVERSION) $@
	mv $(TMPDIR)/mw3{,-dbg}_$(VERSION)*.deb $(CURDIR)/
	rm -rf $(TMPDIR)/$(MWVERSION)
else
# A temp dir doesn't exist so create it and invoke this makefile again
install deb export tarball rpm:
	$(MAKE) TMPDIR=`mktemp -d` $@

endif
endif

clean:
	$(MAKE) -C src $@
	$(MAKE) -C po $@
	$(MAKE) -C src/webclient $@
	$(MAKE) -C src/server $@
	$(MAKE) -C src/client $@

version:
	@echo $(VERSION)

.PHONY: build clean rpm deb tarball export install version
