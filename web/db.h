#ifndef DB_H
#define DB_H


extern void db_init(int cache_people);
extern struct folder *folder_by_name(char *name);
extern struct folder *folder_by_number(int n);
extern struct person *person_by_name(char *name);
extern int open_text(struct folder *ff, struct Header *hh);
extern int message_iterate(struct folder *ff, struct person *pp, int (*fxn)(struct folder *ff, struct Header *hh, void *arg1, void *arg2), void *arg1, void *arg2);
extern int message_by_number(struct folder *ff, struct person *pp, struct Header *hh, int n);

#endif /* DB_H */
