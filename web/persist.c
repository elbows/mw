#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "persist.h"

#define PERSIST_PATH "/home/httpd/tmp"

struct persist {
	struct persist *next;
	char *key;
	char *data;
};

static struct persist *plist = NULL;
char *pname = NULL;

static char *__persist_find(char *key, int *rem)
{
	struct persist *p = plist;
	struct persist *pp = NULL;
	int l = strlen(key);

	for(; p ; pp = p, p = p->next) {
		if (strlen(p->key) != l)
			continue;
		if (strcmp(p->key, key))
			continue;
		if (*rem) {
			if (!pp)
				plist = p->next;
			else
				pp->next = p->next;
			free(p);
			*rem = 0;
			return NULL;
		}
		return p->data;
	}

	return NULL;
}

char *persist_find(char *key)
{
	int rem = 0;

	return __persist_find(key, &rem);
}

int persist_remove(char *key)
{
	int rem = -1;
	__persist_find(key, &rem);
	return rem;
}

int persist_add(char *key, char *data)
{
	struct persist *p;

	persist_remove(key);

	p = malloc(sizeof(struct persist) + strlen(key) + strlen(data) + 2);
	if (!p)
		return -1;
	memset(p, 0, sizeof(struct persist));
	p->key = (char *)(p + 1);
	p->data = p->key + strlen(key) + 1;
	strcpy(p->key, key);
	strcpy(p->data, data);
	p->next = plist;
	plist = p;

	return 0;
}

char *persist_sync(char *prefix)
{
	FILE *fp;
	struct persist *p = plist;

	if (!pname)
		pname = tempnam(PERSIST_PATH, prefix);

	if (!pname) {
		perror("tempnam");
		return NULL;
	}


	if ((fp = fopen(pname, "w")) == NULL)
		return NULL;

	while(p != NULL) {
		fprintf(fp, "%s: %s\n", p->key, p->data);
		p = p->next;
	}

	fclose(fp);

	return pname;
}

static void strip(char *str)
{
	int i = strlen(str);
	i--;

	while((i >= 0) && (!isprint(*(str+i)))) {
		*(str + i) = 0;
		i--;
	}
}

static void add_line(char *line)
{
	char *key = strtok(line, ":");
	char *data;

	if (!key)
		return;

	data  = strtok(NULL, ":");

	if (!data)
		return;

	while(isspace(*key))
		key++;

	if (*key == '#')
		return;

	while(isspace(*data))
		data++;

	strip(data);

	persist_add(key, data);
}

static int persist_read(void)
{
	FILE *fp;
	char line[2048];

	if (!pname)
		return -1;

	if ((fp = fopen(pname, "r")) == NULL)
		return -1;

	while(fgets(line, 2048, fp))
		add_line(line);

	fclose(fp);

	return 0;
}

int persist_init(char *name)
{
	if (name == NULL)
		return -1;

	if (strlen(name) < strlen(PERSIST_PATH))
		return -1;

	if (memcmp(name, PERSIST_PATH, strlen(PERSIST_PATH)))
		return -1;

	pname = name;

	return persist_read();
}

#ifdef TEST
int main()
{
	char *name;

	persist_add("Field1", "Some Text");
	persist_add("Field2", "Some more");
	name = persist_sync("TEST");

	if (!name)
		printf("No name\n");

	plist = NULL;
	pname = NULL;

	persist_init(name);
	printf("Field1: [%s]\n", persist_find("Field1"));
	printf("Field2: [%s]\n", persist_find("Field2"));
	printf("Field3: [%s]\n", persist_find("Field3"));
}
#endif
