#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>

#include "bb.h"
#include "perms.h"

#define MAX_FOLS 50
#define MAX_PEOPLE 500

static struct folder flist[MAX_FOLS];
static int n_fols = 0;
static struct person plist[MAX_PEOPLE];
static int n_users = 0;

static struct person def_user = {
	"JoePublic",
	"*",
	0,
	{ 0, 0 },
	0,
	0,
	{ 0, },
	"Default WWW User",
	"admin@sucs.swan.ac.uk",
	0,
	0,
	0,
	"Reading files for CGI engine",
	0,

	0,
	0,
	0,
	0,

	0
};

void db_init(int cache_users)
{
	FILE *fp;
static struct folder new_folder = { 11, "NEW", "New Messages", -1, -1, 0, 0, };

	if ((fp = fopen(HOMEPATH "/" FOLDERFILE, "r")) == NULL)
		return;

	memcpy(flist, &new_folder, sizeof(struct folder));

	n_fols = 1 + fread(flist + 1, sizeof(struct folder), MAX_FOLS-1, fp);

	fclose(fp);

	if (!cache_users)
		return;

	if ((fp = fopen(HOMEPATH "/" USERFILE, "r")) == NULL)
		return;

	n_users = fread(plist, sizeof(struct person), MAX_PEOPLE, fp);

	fclose(fp);
}


struct folder *folder_by_number(int n)
{
	if (n < 0)
		return NULL;
	if (n >= n_fols)
		return NULL;

	return &flist[n];
}

struct folder *folder_by_name(char *name)
{
	int i;
	int l = strlen(name);

	for(i = 0; i < n_fols; i++) {
		if (l != strlen(flist[i].name))
			continue;
		if (strcmp(name, flist[i].name) != 0)
			continue;

		return &flist[i];
	}

	return NULL;
}

static struct person *__person_cache_find(char *name)
{
	struct person *pp = NULL;
	int i;
	int l = strlen(name);

	for(i = 0; i < n_users; i++) {
		if (l != strlen(plist[i].name))
			continue;
		if (strcmp(name, plist[i].name))
			continue;

		pp = &plist[i];
		break;
	}

	return pp;
}

struct person *person_by_name(char *name)
{
	static struct person pp, *pp2;
	FILE *fp;
	int l;

	if (name == NULL)
		return &def_user;

	if ((pp2 = __person_cache_find(name)) != NULL)
		return pp2;

	fp = fopen(HOMEPATH "/" USERFILE, "r");
	if (fp == NULL)
		return &def_user;

	l = strlen(name);

	while(fread(&pp, sizeof(struct person), 1, fp) == 1) {
		if (strlen(pp.name) != l)
			continue;
		if (strcmp(pp.name, name) != 0)
			continue;

		fclose(fp);
		return &pp;
	}

	fclose(fp);

	return NULL;
}

static FILE *open_index(struct folder *ff)
{
	char path[512];

	snprintf(path, 512, HOMEPATH "/%s" INDEX_END, ff->name);

	return fopen(path, "r");
}

int open_text(struct folder *ff, struct Header *hh)
{
	int fd;
	int err;
	char path[512];

	snprintf(path, 512, HOMEPATH "/%s" TEXT_END, ff->name);

	if ((fd = open(path, O_RDONLY)) < 0)
		return fd;

	if (lseek(fd, hh->datafield, SEEK_SET) < 0) {
		err = errno;
		close(fd);
		errno = err;
		return -1;
	}

	return fd;
}

static int check_num(struct folder *ff, struct Header *hh, void *arg1, void *arg2)
{
	struct Header *hh2 = (struct Header *)arg1;

	if (hh->Ref != hh2->Ref)
		return 0;

	memcpy(hh2, hh, sizeof(struct Header));

	return 1;
}


int message_iterate(struct folder *ff, struct person *pp, int (*fxn)(struct folder *ff, struct Header *hh, void *arg1, void *arg2), void *arg1, void *arg2)
{
	FILE *fp;
	static struct Header hh;
	int rv = 0;

	errno = EPERM;
	if (!allowed_r(ff, pp))
		return -1;

	fp = open_index(ff);
	if (fp == NULL)
		return -1;


	while(fread(&hh, sizeof(struct Header), 1, fp) == 1) {
		if (is_private(ff, pp) && strcmp(pp->name, hh.to) && !u_god(pp->status))
			continue;

		if ((rv = fxn(ff, &hh, arg1, arg2)) != 0)
			break;
	}

	return rv;
}

int message_by_number(struct folder *ff, struct person *pp, struct Header *hh, int n)
{
	hh->Ref = n;

	return message_iterate(ff, pp, check_num, hh, NULL);
}


