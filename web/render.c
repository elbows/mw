#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <sysexits.h>
#include <stdlib.h>
#include <string.h>

#define obstack_chunk_alloc html_malloc
#define obstack_chunk_free free


#include "render.h"


static void *html_malloc(int size)
{
	void *p;

	if ((p = malloc(size)) == NULL) {
		perror("render.c: html_alloc");
		exit(EX_TEMPFAIL);
	}

	return p;
}

static void def_print_start(struct html_doc_head *hdh, struct html_object *o)
{
	if (o->start_data) {
		fputs((char *)o->start_data, hdh->fp);
		fputc('\n', stdout);
	}
}

static void def_print_stop(struct html_doc_head *hdh, struct html_object *o)
{
	if (o->stop_data) {
		fputs((char *)o->stop_data, hdh->fp);
		fputc('\n', stdout);
	}
}

struct html_object *render_alloc(struct html_doc_head *hdh, struct html_object *container)
{
	struct html_object *o, *c;

	o = obstack_alloc(&hdh->stack, sizeof(struct html_object));

	if (hdh->obj == NULL)
		hdh->obj = o;

	memset(o, 0, sizeof(struct html_object));

	o->start = def_print_start;
	o->stop = def_print_stop;

	if (container) {
		c = container;
		if (!c->contents) {
			c->contents = 
			c->last_contents = o;
		} else {
			c->last_contents->next = o;
			c->last_contents = o;
		}
	}

	return o;
}

void __render(struct html_doc_head *hdh, struct html_object *obj)
{
	if (obj == NULL)
		return;

	while(obj) {
		obj->start(hdh, obj);
		__render(hdh, obj->contents);
		obj->stop(hdh, obj);

		obj = obj->next;
	}
}

int render_init(struct html_doc_head *hdh)
{
	hdh->obj = NULL;
	hdh->fp = stdout;
	return obstack_init(&hdh->stack);
}

int html_fdcat(struct html_doc_head *hdh, int fd, int len)
{
	char buf[4096];
	int try;
	int rv;

	do {
		try = (len < 4096) ? len : 4096;
		rv = read(fd, buf, try);
		if (rv <= 0)
			return rv;
		obstack_grow(&hdh->stack, buf, rv);
		len -= rv;
	} while(len >= 0);

	return 0;
}

void html_esccat(struct html_doc_head *hdh, char *str)
{
	while(*str) {
		switch(*str) {
			case ' ':
				obstack_1grow(&hdh->stack, '+');
				break;
			case '"':
				html_strcat(hdh, "%22");
				break;
			case '%':
				html_strcat(hdh, "%25");
				break;
			case '&':
				html_strcat(hdh, "%26");
				break;
			case '+':
				html_strcat(hdh, "%2B");
				break;
			case '=':
				html_strcat(hdh, "%3D");
				break;
			case '?':
				html_strcat(hdh, "%3F");
				break;
			default:
				obstack_1grow(&hdh->stack, *str);
		}
		str++;
	}
}

#ifdef TEST

int main()
{
	struct html_doc_head hdh;
	struct html_object *doc, *head, *body, *title;

	render_init(&hdh);

	doc = html_tag(&hdh, "<HTML>", "</HTML>", NULL);
	head = html_tag(&hdh, "<HEAD>", "</HEAD>", doc);
	body = html_tag(&hdh, "<BODY>", "</BODY>", doc);
	title = html_tag(&hdh, "<TITLE>", "</TITLE>", head);
	html_tag(&hdh, "This a test of the html render code", NULL, title);
	html_tag(&hdh, "This is some test text for the body of the document", NULL, body);

	render(&hdh);

	render_done(&hdh);

	return 0;
}

#endif
