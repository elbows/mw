[1;4mMiscellaneous Instructions[0m

[1mNAME[0m
     [1mCALL[0m   - Call a function and store its result in a variable
     [1mJUMP[0m - Jump to location
     [1mRETURN[0m - Returns from the current function

[1mSYNOPSIS[0m
     [1mCALL[0m [4mfunction[0m [4mvariable[0m [[4marguments...[0m]
     [1mJUMP[0m [4mlocation[0m [[4marguments...[0m]
     [1mRETURN[0m [[4mvalue[0m]

[1mDESCRIPTION[0m
     [1mCALL[0m calls a subroutine which returns a value. The returned
     value (if any) is stored in the specified variable. [1mCALL[0m cannot
     be used to jump to a label within a function.

     [1mJUMP[0m branches execution to the listed location.

     If the supplied location is a local branch, then execution
     branches there and continues as normal, i.e. a GOTO
     Otherwise, if it is a function, then that function is executed,
     when that function finishes execution will continue from the
     same location.

     There is a special argument that can be used with [1mJUMP[0m. Like the
     arguments '$1', '$*' etc.. there is an argument '$^' which only
     works when given to a JUMP instruction. It will append the arguments
     $2, $3 ... $n from the calling function. For example the code:

         [1mfunction[0m test
           [1mJUMP[0m test2 hello $1 world $^ $2
         [1mendfunc[0m

         [1mfunction[0m test2
           [1mPRINT[0m $*
         [1mendfunc[0m

     will give:

         > ,test a b c
         hello a world b c b
         > ,test a
         hello a world

     [1mRETURN[0m causes execution of the current function to stop,
     and returns execution to the location from which this function
     was called. If this was the first function called, then script
     execution ends.  If a return value is given and the current
     function was called using the [1mCALL[0m instruction, this value is
     stored in the variable specified by the caller.  Otherwise no
     variables are affected.

