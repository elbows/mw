#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <getopt.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <string.h>
#include <time.h>
#include <socket.h>
#include <who.h>
#include "servsock.h"

/* unused, but necessary to link other util functions */
int idle = 0;
int internet = 0;
int userposn = 0;

#define MWUSER "mw"
	
time_t uptime = 0;

struct servopts {
	uint16_t port;
	int foreground;
	int help;
};

static void usage(char *name)
{
	printf("Usage:\n  %s [--help|-h][--port|-p <port>][--foreground|-f]\n", name);
}

static int getopts(int argc, char **argv, struct servopts *opts)
{
	int c;
	int optidx = 0;
	long num;
	static struct option loptspec[] = {
		{"port", required_argument, 0, 'p'},
		{"help", no_argument, 0, 'h'},
		{"foreground", no_argument, 0, 'f'},
		{0, 0, 0, 0}
	};

	while (1) {
		c = getopt_long(argc, argv, "p:hf", loptspec, &optidx);
		if (c == -1)
			break;

		switch (c) {
			case 'p':
				errno = 0;
				num = strtol(optarg, NULL, 0);
				if (errno != 0 || num < 1 || num > UINT16_MAX) {
					fprintf(stderr, "Bad port number\n");
					return 1;
				}
				opts->port = (uint16_t)num;
				break;
			case 'f':
				opts->foreground = 1;
				break;
			case 'h':
				opts->help = 1;
				return 0;
			case '?':
			default:
				return 1;
		}
	}

	if (optind < argc) {
		fprintf(stderr, "Unrecognised arguments: ");
		while (optind < argc)
			fprintf(stderr, "%s ", argv[optind++]);
		fprintf(stderr, "\n");
		return 1;
	}
	return 0;
}

int main(int argc, char **argv)
{
	struct servopts opts = {.port = IPCPORT_DEFAULT, .foreground = 0};
	int mainsock = -1;
	int err = getopts(argc, argv, &opts);

	if (err || opts.help) {
		usage(argv[0]);
		return err;
	}

	init_server();

	mainsock = open_mainsock(opts.port);

	if (mainsock < 0) {
		fprintf(stderr, "Failed.\n");
		return 1;
	}

	if (geteuid() == 0) {
		struct passwd *pwbuff = getpwnam(MWUSER);
		if (!pwbuff) {
			fprintf(stderr, "Username %s does not exist.\n", MWUSER);
			return 1;
		}
		if (setgid(pwbuff->pw_gid)) {
			fprintf(stderr, "Failed to setgid. %s\n", strerror(errno));
			return 1;
		}
		if (setuid(pwbuff->pw_uid)) {
			fprintf(stderr, "Failed to setuid. %s\n", strerror(errno));
			return 1;
		}
	}

	/* at server start nobody is logged in, wipe who list */
	int fd = who_open(O_TRUNC|O_WRONLY);
	if (fd < 0)
		return 1;
	close(fd);

	if (!opts.foreground) daemon(0,0);

	uptime = time(0);
	watch_mainsock(mainsock);
	printf("Done.\n");
}
