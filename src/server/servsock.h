#ifndef SERVSOCK_H
#define SERVSOCK_H
#include <user.h>

/* servsock.c */
int open_mainsock(uint16_t port);
ipc_connection_t *add_connection(int fd);
void accept_connection(int mainsock);
void drop_connection(ipc_connection_t *conn);
void watch_mainsock(int mainsock);
void write_socket(ipc_connection_t *conn);
void process_msg(ipc_connection_t *conn, ipc_message_t *msg);
void msg_attach_to_all(ipc_message_t *msg);
void msg_attach_to_pid(ipc_message_t *msg, uint32_t pid);
int msg_attach_to_userid(ipc_message_t *msg, uint32_t userposn);
int msg_attach_to_username(ipc_message_t *msg, const char *username);
void msg_attach_to_channel(ipc_message_t *msg, int channel, const char * exclude);
void msg_attach(ipc_message_t *msg, ipc_connection_t *conn);
void init_server(void);
void send_error(ipc_connection_t *conn, ipc_message_t *orig, const char *format, ...);
void msg_apply_gag(struct person * from, ipc_message_t * msg, const char * field);
#endif /* SERVSOCK_H */
