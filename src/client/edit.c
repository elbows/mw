/*********************************************************
 *     The Milliways III System is copyright 1992        *
 *      J.S.Mitchell. (arthur@sugalaxy.swan.ac.uk)       *
 *       see licence for furthur information.            *
 *********************************************************/
#include <stdio.h>
#include <inttypes.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdarg.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <sys/stat.h>
#include "talker_privs.h"
#include "special.h"
#include "incoming.h"
#include "ipc.h"
#include "files.h"
#include "strings.h"
#include "str_util.h"
#include "perms.h"
#include "getpass.h"
#include "edit.h"
#include "init.h"
#include "read.h"
#include "intl.h"
#include "bb.h"
#include "userio.h"
#include "who.h"
#include "mesg.h"

const char *partlist_user[]={
"edit", "status", "special", "groups", "passwd", "chatprivs", "chatmode",
"realname", "username", "contact", "timeout", "lastread", "view", "room",
"clearignore", "protection", "doing", NULL};

extern int busy;
extern int mesg_waiting;
extern struct person *user;
extern long userposn;

static void edit_all(struct person *usr)
{
	int i,r;
	char tmp[80];	
	
	printf("Editing user %s\n",usr->name);
	printf("Enter new data for each field <cr> to leave unchanged.\n");
	
	printf("realname: %s\nnew name: ",usr->realname);
	get_str(tmp,REALNAMESIZE);
	if (strlen(tmp)!=0) strcpy(usr->realname,tmp);
	printf("email: %s\nnew email: ",usr->contact);
	get_str(tmp,CONTACTSIZE);
	if (strlen(tmp)!=0) strcpy(usr->contact,tmp);
	printf("subscribed %lo %lo\nsubscibed: ",(unsigned long)usr->folders[0],(unsigned long)usr->folders[1]);
	get_str(tmp,10);
	if (strlen(tmp)!=0) usr->folders[0]=atoi(tmp);
	printf("last message read (folder:message) -\n");
	for (i=0;i<11;i++) printf("%2d:%03d ",i,usr->lastread[i]);
	printf("\n");
	for (i=11;i<22;i++) printf("%2d:%03d ",i,usr->lastread[i]);
	printf("\n");
	for (i=22;i<32;i++) printf("%2d:%03d ",i,usr->lastread[i]);
	printf("\n");
	for (;;)
	{
		printf("set read - <folder> <message>:");
		get_str(tmp,20);
		if (strlen(tmp)==0) break;
		sscanf(tmp,"%d %d",&i,&r);
		usr->lastread[i]=r;
		sprintf(tmp, "%d:%d", i, r);
		ipc_send_to_username(usr->name, IPC_LASTREAD, tmp);
	}
	show_user_stats(usr->status,tmp,true);
	printf("current status %s\nnew status: ",tmp);
	get_str(tmp,20);
	if (*tmp) 
	{
		usr->status=user_stats(tmp,usr->status);
		show_user_stats(usr->status,tmp,false);
		printf("status now set to [%s]\n",tmp);
	}
}

static void show_change(char *old, char *new, const char *fmt, ...) __attribute__((format(printf,3,4)));
static void show_change(char *old, char *new, const char *fmt, ...)
{
	va_list va;
	char text[MAXTEXTLENGTH];
	char add[MAXTEXTLENGTH];
	char sub[MAXTEXTLENGTH];
	char buff[2];
	int i;

	va_start(va, fmt);
        vsnprintf(text, MAXTEXTLENGTH-1, fmt, va);
	va_end(va);

	/* calulate sub */
	sub[0] = 0;
	for (i=0; i < strlen(old); i++)
	{
		if ((strchr(new, old[i]) == NULL) && (new[i]!='-') && (old[i]!='-'))
		{
			buff[0] = old[i];
			buff[1] = 0;
			strncat(sub, buff, MAXTEXTLENGTH - strlen(sub) - 1);
		}
	}
	
	/* calulate add */
	add[0] = 0;
	for (i=0; i < strlen(new); i++)
	{
		if ((strchr(old, new[i]) == NULL) && (new[i]!='-'))
		{
			buff[0] = new[i];
			buff[1] = 0;
			strncat(add, buff, MAXTEXTLENGTH - strlen(sub) - 1);
		}
	}

	if (add[0]!=0)
	{
		strncat(text, " +", MAXTEXTLENGTH - strlen(text) - 1);
		strncat(text, add, MAXTEXTLENGTH - strlen(text) - 1);
	}
	if (sub[0]!=0)
	{
		strncat(text, " -", MAXTEXTLENGTH - strlen(text) - 1);
		strncat(text, sub, MAXTEXTLENGTH - strlen(text) - 1);
	}
	if (add[0]==0 && sub[0]==0) return;
	broadcast(3, "%s", text);
}

void edit_user(const char *args, const char *name)
{
	int32_t usrposn;
	struct person usr;

	if (!is_old(&usr,name,&usrposn))
	{
		printf(_("Username %s not found.\n"),name);
		return;
	}
	
	if (stringcmp(args,"edit",-1))
		edit_all(&usr);
	else
	if (stringcmp(args,"status",2))
	{
		char stats[12];
		char *oldstats;

		show_user_stats(usr.status,stats,false);
		oldstats = strdup(stats);
		printf(_("Current status set to [%s]\n"),oldstats);

		show_user_stats(255,stats,false);
		printf(_("New status [+-=][%s]: "),stats);
		get_str(stats,9);
		if (*stats)
		{
			char statout[11];
			int i;
			i=user_stats(stats,usr.status);
			if (u_del(i))
			{
				printf(_("Do you really want to delete this user ? "));
				get_str(statout,4);
				if (statout[0]=='y' || statout[0]=='Y')
					usr.status=i;
			}
			else
				usr.status=i;

			show_user_stats(usr.status,statout,false);
			printf(_("Status set to [%s].\n"),statout);
			ipc_send_to_username(usr.name, IPC_STATUS, stats);
			show_change(oldstats, statout, "%s has just changed %s's status", user->name, usr.name);
			mwlog("CHANGE(STATUS) of %s by %s", usr.name, stats);
		}
		free(oldstats);
	}else
	if (stringcmp(args,"special",2))
	{
		char stats[20];
		char *oldstats;

		show_special(usr.special,stats,false);
		oldstats = strdup(stats);
		printf(_("Current specials set to [%s]\n"),oldstats);

		show_special(65535,stats,false);
		printf(_("New specials [+-=][%s]: "),stats);
		get_str(stats,18);
		if (*stats)
		{	char statout[20];
			usr.special=set_special(stats,usr.special);
			show_special(usr.special,statout,false);
			printf(_("Specials set to [%s].\n"),statout);
			ipc_send_to_username(usr.name, IPC_SPECIAL, stats);
			show_change(oldstats, statout, "%s has just changed %s's specials", user->name, usr.name);
			mwlog("CHANGE(SPECIAL) of %s by %s", usr.name, stats);
		}
		free(oldstats);
	}else
	if (stringcmp(args,"chatprivs",5))
	{
		char stats[40];
		char *oldstats;

		oldstats = strdup(display_cpflags(usr.chatprivs));
		printf(_("Current chatprivs set to [%s]\n"),oldstats);
		printf(_("New chatprivs [+-=][%s]: "),display_cpflags((unsigned long)0xffffffff));
		get_str(stats,38);
		if (*stats)
		{	
			usr.chatprivs=cp_setbycode(usr.chatprivs, stats);
			printf(_("Chatprivs set to [%s].\n"),display_cpflags(usr.chatprivs));
			ipc_send_to_username(usr.name, IPC_CHATPRIVS, stats);
			show_change(oldstats, display_cpflags(usr.chatprivs), "%s has just changed %s's chatprivs", user->name, usr.name);
			mwlog("CHANGE(CHATPRIV) of %s by %s", usr.name, stats);
		}
		free(oldstats);
	}else
	if (stringcmp(args,"protection",2))
	{
		char text[10];
		char stats[10];
		char *oldstats;
		int cpl, apl;

		show_protection(usr.chatmode, usr.chatprivs, stats, 9);
		oldstats = strdup(stats);
		cpl = (usr.chatmode & CM_PROTMASK) >> CM_PROTSHIFT;
		apl = (usr.chatprivs & CP_PROTMASK) >> CP_PROTSHIFT;
		printf(_("Current protection set to %s\n"),oldstats);
		printf(_("New levels [0-4]/[0-4]: "));
		get_str(stats,8);
		if ((sscanf(stats, "%d/%d", &cpl, &apl) == 2 ||
		     sscanf(stats, "/%d", &apl) == 1 ||
		     sscanf(stats, "%d", &cpl) == 1) &&
		    apl >= 0 && apl <= 4 && cpl >= 0 && cpl <= 4)
		{
			usr.chatprivs = (usr.chatprivs & ~CP_PROTMASK) |
					(apl << CP_PROTSHIFT);
			usr.chatmode = (usr.chatmode & ~CM_PROTMASK) |
					(cpl << CM_PROTSHIFT);
			printf(_("Protection set to %d/%d.\n"), cpl, apl);
			snprintf(text, 9, "%d", cpl);
			ipc_send_to_username(usr.name, IPC_PROTLEVEL, text);
			snprintf(text, 9, "%d", apl);
			ipc_send_to_username(usr.name, IPC_PROTPOWER, text);

			broadcast(3, "%s has just changed %s's protection from %s to %d/%d.", user->name, usr.name, oldstats, cpl, apl);
			mwlog("CHANGE(PROTLEVEL) of %s to %d/%d", usr.name, cpl, apl);
		} else if (*stats)
		{
			printf(_("Invalid protection level.\n"));
		}
		free(oldstats);
	}else
	if (stringcmp(args,"chatmode",5))
	{
		char stats[40];
		char *oldstats;

		oldstats = strdup(display_cmflags(usr.chatmode));
		printf(_("Current chatmode set to [%s]\n"),oldstats);
		printf(_("New chatmode [+-=][%s]: "),display_cmflags((unsigned long)0xffffffff));
		get_str(stats,38);
		if (*stats)
		{	
			usr.chatmode=cm_setbycode(usr.chatmode, stats);
			printf(_("Chatmode set to [%s].\n"),display_cmflags(usr.chatmode));
			ipc_send_to_username(usr.name, IPC_CHATMODE, stats);
			show_change(oldstats, display_cmflags(usr.chatmode), "%s has just changed %s's chatmodes", user->name, usr.name);
			if (*stats)
			{
				mwlog("CHANGE(CHATMODE) of %s by %s", usr.name, stats);
			}
		}
		free(oldstats);
	}else
	if (stringcmp(args,"groups",1))
	{
		char stats[11];
		char *oldstats;

		show_fold_groups(usr.groups,stats,true);
		oldstats = strdup(stats);
		printf(_("User currently in groups [%s]\n"),oldstats);
		printf(_("New Groups [+-=][12345678]: "));
		get_str(stats,10);
		if (*stats)
		{
			usr.groups=folder_groups(stats,usr.groups);
			ipc_send_to_username(usr.name, IPC_GROUPS, stats);
			mwlog("CHANGE(GROUPS) of %s by %s", usr.name, stats);

			show_fold_groups(usr.groups,stats,false);
			show_change(oldstats, stats, "%s has just changed %s's groups", user->name, usr.name);
			printf(_("Groups changed to [%s]\n"),stats);
		}
		free(oldstats);
	}else
	if (stringcmp(args,"passwd",2))
	{
		char pass1[PASSWDSIZE+1],pass2[PASSWDSIZE+1];
		char salt[3];
		pick_salt(salt);
		strcpy(pass1,crypt(get_pass(_("New Passwd: ")),salt));
		strcpy(pass2,crypt(get_pass(_("Again: ")),salt));
		if (strcmp(pass1,pass2))
		{
			printf(_("Passwords did not match.\nNot done.\n"));
			return;
		}else
		{
			strcpy(usr.passwd,pass1);
			printf(_("Password changed.\n"));
			broadcast(3, "%s has just changed %s's password.", user->name, usr.name);
			mwlog("CHANGE(PASSWD) of %s", usr.name);
			ipc_send_to_username(usr.name, IPC_PASSWD, usr.passwd);
		}
	}else
	if (stringcmp(args,"realname",2))
	{
		char realname[REALNAMESIZE+1];
		char *oldreal;

		oldreal = strdup(usr.realname);
		printf(_("Real Name: %s\n"),oldreal);
		printf(_("Enter new name (%d chars): "),REALNAMESIZE);
		get_str(realname,REALNAMESIZE);
		if (*realname)
		{
			strcpy(usr.realname,realname);
			ipc_send_to_username(usr.name, IPC_REALNAME, usr.realname);
			printf(_("New name set.\n"));
			broadcast(3, "%s has just changed %s's realname from \"%s\" to \"%s\".", user->name, usr.name, oldreal, realname);
			mwlog("CHANGE(REALNAME) of %s to %s", usr.name, realname);
		}
		free(oldreal);
	}else
	if (stringcmp(args,"room",2))
	{
		char	room[5];
		char	text[6];
		int	newroom;
		int	oldroom;

		oldroom = usr.room;
		printf(_("User currently in room [%d]\n"),oldroom);
		printf(_("New Room: [0-65535]: "));
		get_str(room,5);
		if (*room)
		{
			if (sscanf(room,"%d",&newroom)!=1)
			{
				printf(_("Invalid Room ID (0-65535 only)\n"));
				return;
			}

			if (newroom<0 || newroom>65535)
			{
				printf(_("Invalid Room ID (0-65535 only)\n"));
				return;
			}
			usr.room = (unsigned short)newroom;
			sprintf(text, "s%05d", newroom);
			ipc_send_to_username(usr.name, IPC_CHANNEL, text);
			printf(_("Room changed to %d.\n"), usr.room);
			broadcast(3, "%s has just changed %s's room from %d to %d.", user->name, usr.name, oldroom, newroom);
			mwlog("CHANGE(ROOM) of %s to %d", usr.name, newroom);
		}
	}else
	if (stringcmp(args,"username",1))
	{
		char username[NAMESIZE+1];
		char oldname[NAMESIZE+1];

		printf(_("WARNING: This command can be very dangerous !\n"));
		printf(_("User Name: %s\n"),usr.name);
		printf(_("Enter new username (%d chars): "),NAMESIZE);
		get_str(username,NAMESIZE);

		strcpy(oldname,usr.name);
		strip_name(username);

		if (*username)
		{
			char answer[10];
			struct person uu;
			int32_t uup;

			if (is_old(&uu,username,&uup))
			{
				/* it exists, so is it the right person */
				if (usrposn!=uup)
				{
					printf(_("You cannot change a username to one that already exists.\n"));
					return;
				}else
					printf(_("Attempting to update existing user.\n"));
			}
	
			printf(_("Are you sure you want to change user '%s' into user '%s' ? "),oldname,username);
			fflush(stdout);
			get_str(answer,5);
			if (answer[0]=='y' || answer[0]=='Y')
			{	
				strcpy(usr.name,username);
				ipc_send_to_username(oldname, IPC_USERNAME, usr.name);
				printf(_("New name set.\n"));
				broadcast(3, "%s has just renamed %s to %s.", user->name, oldname, username);
				mwlog("CHANGE(USERNAME) of %s to %s", oldname, username);
			}else
				printf(_("Change Cancelled.\n"));
		}else
			printf(_("Change Cancelled.\n"));
	}else
	if (stringcmp(args,"contact",2))
	{
		char contact[CONTACTSIZE+1];
		printf(_("Contact address: %s\n"),usr.contact);
		printf(_("New address (%d chars): "),CONTACTSIZE);
		get_str(contact,CONTACTSIZE);
		if (*contact)
		{
			strcpy(usr.contact,contact);
			ipc_send_to_username(usr.name, IPC_CONTACT, usr.contact);
			printf(_("New address set.\n"));
			broadcast(3, "%s has just changed %s's contact to %s.", user->name, usr.name, contact);
			mwlog("CHANGE(CONTACT) of %s to %s", usr.name, contact);
		}
	}else
	if (stringcmp(args,"doing",2))
	{
		char doing[DOINGSIZE];
		printf(_("Current Status: %s\n"),usr.doing);
		printf(_("New Status (%d chars): "),DOINGSIZE-1);
		get_str(doing,DOINGSIZE-1);
		if (*doing)
		{
			strcpy(usr.doing,doing);
			usr.dowhen = time(0);
			ipc_send_to_username(usr.name, IPC_DOING, usr.doing);
			printf(_("New status set.\n"));
			broadcast(3, "%s has just changed %s's status to %s.", user->name, usr.name, doing);
			mwlog("CHANGE(STATUS) of %s to %s", usr.name, doing);
		} else {
			*(usr.doing) = 0;
			usr.dowhen = 0;
			ipc_send_to_username(usr.name, IPC_DOING, usr.doing);
			printf(_("New status set.\n"));
			broadcast(3, "%s has just cleared %s's status.", user->name, usr.name);
			mwlog("CHANGE(STATUS) cleared %s", doing);
		}
	}else
	if (stringcmp(args,"timeout",1))
	{
		char	tt[20];
		int	tv;
		int	units=1;
		printf(_("Timeout: %ld sec.\n"),(long)usr.timeout);
		printf(_("New timeout value: "));
		get_str(tt,CONTACTSIZE);
		if (*tt)
		{
			if (strchr(tt,'m') || strchr(tt,'M'))
				units = 60;
			else
			if (strchr(tt,'h') || strchr(tt,'H'))
				units = 3600;
			else
			if (strchr(tt,'d') || strchr(tt,'D'))
				units = 86400;

			tv = atoi(tt) * units;

			if (tv==0)
			{
				if (usr.timeout != 0)
				{
					usr.timeout = 0;
					ipc_send_to_username(usr.name, IPC_TIMEOUT, tt);
					printf(_("TIMEOUT now disabled.\n"));
					broadcast(3, "%s has just disabled %s's timeout.", user->name, usr.name);
					mwlog("CHANGE(TIMEOUT) of %s to disabled", usr.name);
				}
				else
				{
					printf(_("TIMEOUT was already disabled.\n"));
				}
			}
			else if (tv<600 && (tv!=0))
			{
				printf(_("TIMEOUT must be be at least 10 minutes (600), or 0 to disable.\n"));
			}
			else
			{
				usr.timeout=tv;
				snprintf(tt,20,"%d",tv);
				ipc_send_to_username(usr.name, IPC_TIMEOUT, tt);
				broadcast(3, "%s has just changed %s's timeout to %"PRId32" seconds.", user->name, usr.name, usr.timeout);
				printf(_("New timeout set to %"PRId32" seconds.\n"), usr.timeout);
				mwlog("CHANGE(TIMEOUT) of %s to %"PRId32" seconds", usr.name, usr.timeout);
			}
		}
	}else
	if (stringcmp(args,"lastread",1))
	{
		char temp[FOLNAMESIZE+1];
		int folnum;
		printf(_("Lastread in folder? "));
		get_str(temp,FOLNAMESIZE);
		folnum=foldernumber(temp);
		if (folnum==-1)
		{
			printf(_("Folder not found.\n"));
		}else
		{
			printf(_("Last read message number %d\n"),usr.lastread[folnum]);
			printf(_("New lastread: "));
			get_str(temp,5);
			if (*temp)
			{
				int lastread = atoi(temp);
				usr.lastread[folnum] = lastread;
				printf(_("Lastread changed to %d\n"), lastread);
				sprintf(temp, "%d:%d", folnum, lastread);
				ipc_send_to_username(usr.name, IPC_LASTREAD, temp);
			}else
				printf(_("Not changed.\n"));
		}
	}else
	if (stringcmp(args,"view",1))
	{
		char stats[10],specs[20];
		long timeon;

		show_user_stats(usr.status,stats,true);
		show_special(usr.special,specs,true);
		printf(_("Username: %s\nReal Name: %s\n"),usr.name,usr.realname);
		printf(_("Contact: %s\nStatus [%s]\tSpecials [%s]\n"),usr.contact,stats,specs);
		
		show_fold_groups(usr.groups,stats,true);
		printf(_("Groups [%s]\n"),stats);

		show_protection(usr.chatmode, usr.chatprivs, stats, 9);
		printf(_("Talker: Modes=[%s]  Privs=[%s]  Protection=[%s]\n"),
		       display_cmflags(usr.chatmode),
		       display_cpflags(usr.chatprivs),
		       stats);

		if (usr.timeout==0)
			printf(_("Timeout disabled.\n"));
		else
			printf(_("Timeout set to %s.\n"),itime(usr.timeout));
		timeon=usr.lastlogout;
		printf(_("Last Login: %s"),ctime(&timeon));
		time_on(usr.timeused);
		if (usr.dowhen != 0 && usr.doing[0] != 0)
		printf(_("Status: %s (%s ago)\n"), usr.doing, itime(time(0)-usr.dowhen));
	} else
	if (stringcmp(args,"clearignore",2))
	{
		char answer[10];

		printf(_("Are you sure you want to clear %s's ignore list? "),usr.name);
		fflush(stdout);
		get_str(answer,5);
		if (answer[0]=='y' || answer[0]=='Y')
		{	
			ipc_send_to_username(usr.name, IPC_CLEARIGN, "");
			printf(_("Ignorelist Cleared.\n"));
			broadcast(3, "%s has just cleared %s's ignore list", user->name, usr.name);
			mwlog("CHANGE(IGNORELIST) of %s to cleared", usr.name);
		}else
			printf(_("Clear Cancelled.\n"));
	}else
	{
		printf(_("Unknown Command\n"));
		return;
	}
	update_user(&usr,usrposn);
}


const char *partlist_folder[]={"status", "groups", "name", "size", "view", "topic",
"delete", NULL};

static void users_lastread(int folnum)
{
	struct person usr;
	int ufile;
	
	ufile=userdb_open(O_RDWR);
	Lock_File(ufile);
	while (read(ufile,&usr,sizeof(usr))>0)
	{
		lseek(ufile,-1*sizeof(usr),1);
		usr.lastread[folnum]=0;
		usr.folders[0]|=(1<<folnum);
		write(ufile,&usr,sizeof(usr));
	}
	Unlock_File(ufile);
	close(ufile);
}

void edit_folder(const char *args, const char *name)
{
	char fullpath[256];
	int folnum;
	int afile;
	struct folder fold;
	char tmp[80]; 
	
	if ((folnum=foldernumber(name))==-1)
	{
		printf(_("Unknown folder name.\n"));
		return;
	}
	if (!get_folder_number(&fold,folnum)) return;
	if (stringcmp(args,"status",2))
	{
		show_fold_stats(fold.status,tmp,true);
		printf(_("Folder %s\nCurrent status:-\n"),fold.name);
		printf(_("User not in group [%s]\n"),tmp);
		show_fold_stats(fold.g_status,tmp,true);
		printf(_("User in group [%s]\n"),tmp);
		printf(_("Change to :-\n"));
		printf(_("User not in group [+-=][arwRWpm]: "));
		get_str(tmp,10);
		if (*tmp)
		{
			fold.status=folder_stats(tmp,fold.status);
			snprintf(fullpath, 255, "FOLDER(STATUS) of %s by %s", fold.name, tmp);
			show_fold_stats(fold.status,tmp,true);
			printf(_("Status changed to [%s]\n"),tmp);
			if(!f_active(fold.status))
				printf(_("WARNING: folder may get written over by the next folder created.\n"));
		}
		printf(_("User in group [+-=][arwRWpm]: "));
		get_str(tmp,10);
		if (*tmp)
		{
			fold.g_status=folder_stats(tmp,fold.g_status);
			snprintf(fullpath, 255, "%s %s", fullpath, tmp);
			show_fold_stats(fold.g_status,tmp,true);
			printf(_("Status changed to [%s]\n"),tmp);
			if(!f_active(fold.g_status))
				printf(_("WARNING: folder may get written over by the next folder created.\n"));
		}
		mwlog("%s", fullpath);
	}else
	if (stringcmp(args,"groups",2))
	{
		show_fold_groups(fold.groups,tmp,true);
		printf(_("Folder %s is currently in groups [%s]\n"),fold.name,tmp);
		printf(_("Folder groups [+-=][12345678]: "));
		get_str(tmp,10);
		if (*tmp)
		{
			fold.groups=folder_groups(tmp,fold.groups);
			mwlog("FOLDER(GROUPS) of %s by %s", fold.name, tmp);
			show_fold_groups(fold.groups,tmp,true);
			printf(_("Groups changed to [%s]\n"),tmp);
		}
	}else
	if (stringcmp(args,"name",5))
	{
		printf(_("Current folder name = %s\n"),fold.name);
		printf(_("Change to ? (%d chars): "),FOLNAMESIZE);
		get_str(tmp,FOLNAMESIZE);
		if (*tmp)
		{
			char oldpath[PATHSIZE];	
			mwlog("FOLDER(RENAME) of %s to %s", fold.name, tmp);

			sprintf(oldpath,"%s/%s%s",STATEDIR,fold.name,INDEX_END);
			sprintf(fullpath,"%s/%s%s",STATEDIR,tmp,INDEX_END);
			if (!access(oldpath,00)) rename(oldpath,fullpath);
			sprintf(oldpath,"%s/%s%s",STATEDIR,fold.name,TEXT_END);
			sprintf(fullpath,"%s/%s%s",STATEDIR,tmp,TEXT_END);
			if (!access(oldpath,00)) rename(oldpath,fullpath);
			if (f_moderated(fold.status)||f_moderated(fold.g_status))
			{
			sprintf(oldpath,"%s/%s%s%s",STATEDIR,fold.name,INDEX_END,MOD_END);
			sprintf(fullpath,"%s/%s%s%s",STATEDIR,tmp,INDEX_END,MOD_END);
			if (!access(oldpath,00)) rename(oldpath,fullpath); 
			sprintf(oldpath,"%s/%s%s%s",STATEDIR,fold.name,TEXT_END,MOD_END);
			sprintf(fullpath,"%s/%s%s%s",STATEDIR,tmp,TEXT_END,MOD_END);
			if (!access(oldpath,00)) rename(oldpath,fullpath);
			}
		
			strcpy(fold.name,tmp);
			printf(_("Name changed to %s\n"),fold.name);
		}
	}else
	if (stringcmp(args,"size",4))
	{
		printf(_("First Message in folder is %d\n"),fold.first);
		printf(_("New first message: "));
		get_str(tmp,10);
		if (*tmp)
		{
			fold.first=atoi(tmp);
			printf(_("First message set to %d\n"),fold.first);
		}
		printf(_("Last Message in folder is %d\n"),fold.last);
		printf(_("New Last message: "));
		get_str(tmp,10);
		if (*tmp)
		{
			fold.last=atoi(tmp);
			printf(_("Last message set to %d\n"),fold.last);
		}
	}else
	if (stringcmp(args,"view",2))
	{
		char buff[10];
		show_fold_stats(fold.status,buff,true);
		printf(_("\nFolder Name: %s\nTopic: %s\nMessage range %d to %d\n"),
		fold.name,fold.topic,fold.first,fold.last);
		printf(_("Status (out of group) [%s]"),buff);
		show_fold_stats(fold.g_status,buff,true);
		printf(_("    (in group) [%s]\n"),buff);
		show_fold_groups(fold.groups,buff,true);
		printf(_("Groups [%s]\n"),buff);
	}
	else
	if (stringcmp(args,"topic",2))
	{
		printf(_("Current folder topic = '%s'\n"),fold.topic);
		printf(_("New topic (%d chars): "),TOPICSIZE);
		get_str(tmp,TOPICSIZE);
		if (*tmp)
		{
			strcpy(fold.topic,tmp);
			printf(_("Topic changed to '%s'\n"),fold.topic);
			mwlog("FOLDER(TOPIC) of %s to %s", fold.name, tmp);
		}
	}else
	if (stringcmp(args,"delete",6))
	{
		printf(_("Do you really want to delete folder %s  ?(yes/no) "),fold.name);
		get_str(tmp,4);
		if (stringcmp(tmp,"yes",-1))
		{
			sprintf(fullpath,"%s/%s%s",STATEDIR,fold.name,INDEX_END);
			if (!access(fullpath,00)) if (unlink(fullpath)) {perror(fullpath);exit(-1);}
			sprintf(fullpath,"%s/%s%s",STATEDIR,fold.name,TEXT_END);
			if (!access(fullpath,00)) if (unlink(fullpath)) {perror(fullpath);exit(-1);}
			if (f_moderated(fold.status)||f_moderated(fold.g_status))
			{
			sprintf(fullpath,"%s/%s%s%s",STATEDIR,fold.name,INDEX_END,MOD_END);
			if (!access(fullpath,00)) if (unlink(fullpath)) {perror(fullpath);exit(-1);}
			sprintf(fullpath,"%s/%s%s%s",STATEDIR,fold.name,TEXT_END,MOD_END);
			if (!access(fullpath,00)) if (unlink(fullpath)) {perror(fullpath);exit(-1);}
			}
			fold.status=0;
			fold.g_status=0;
			fold.name[0]=0;
			users_lastread(folnum);
			printf(_("Folder Deleted.\n"));
			mwlog("FOLDER(DELETE) %s", fold.name);
		}else
		{
			printf(_("Not Done.\n"));
			return;
		}
	}else
		return;

	afile=openfolderfile(O_RDWR);
	Lock_File(afile);
	lseek(afile,folnum*sizeof(struct folder),0);
	write(afile,&fold,sizeof(struct folder));
	Unlock_File(afile);
	close(afile);	
}	

const char *partlist_mesg[]={
"edit", "to", "subject", "delete", "undelete", "status", NULL};

void mesg_edit(const char *args, char *foldername, int msgno, struct person *usr)
{
	int fd, text;
	struct Header head;
	char *buff;
	
	if ((fd=err_open(buildpath(STATEDIR,foldername,INDEX_END,""),O_RDWR,0))<0)
		return;
	if ((text=err_open(buildpath(STATEDIR,foldername,TEXT_END,""),O_RDWR,0))<0)
		return;
	
	/* go and get message */
	while (get_data(fd,&head) && head.Ref<msgno);
	if (head.Ref!=msgno)
	{
		printf(_("Message %d not found.\n"),msgno);
		return;
	}
	buff=(char *)malloc(head.size);
	lseek(text,head.datafield,0);
	read(text,buff,head.size);	

	if (!u_god(usr->status)	&& !u_mod(usr->status) && strcasecmp(head.from,usr->name))
	{
		printf(_("You cannot edit other peoples messages.\n"));
	}else
	if (stringcmp(args,"edit",4))
	{
		int tfile;
		int mask;
		char fullpath[20];
		char foo[128];
		long size;
		int child;
		const char *myeditor;

		if (perms_drop()==-1) 
		{
			printf(_("Failed to set user id, aborting.\n"));
			perror("setuid");
			return;
		}

		sprintf(fullpath,"/tmp/mw3XXXXXX");
		mask=umask(~0600);
		tfile = mkstemp(fullpath);
		if (tfile < 0)
		{
			perror("creating temp file");
			perms_restore();
			return;
		}
		write(tfile,buff,head.size);
		close(tfile);
		umask(mask);

		if (!u_reg(usr->status) || (myeditor=getenv("EDITOR"))==NULL)
			myeditor=EDITOR;

		sprintf(foo,"%s %s",myeditor,fullpath);

		busy++;
		if ((child=fork())==0)
		{
			/* we are child */
			signal(SIGUSR1,SIG_IGN);
			execl(myeditor, myeditor, fullpath, NULL);
			exit(0); /* should never get here */
		}else
		if (child==-1)
		{
			printf(_("ERROR: Could not spawn editor: %s\n"),strerror(errno));
		}else
		{
			int a;
		
			do {
				if (mesg_waiting) handle_mesg();
				a=wait(NULL);
				if (a==-1 && errno!=EINTR)
				printf("ERROR: child wait (%d) %s\n",errno, strerror(errno));
			}while (a==-1 && errno==EINTR);
		}
		busy--;

		if ((tfile=open(fullpath,O_RDONLY))<0)
		{
			perror("reading temp file");
			perms_restore();
			return;
		}
		size=lseek(tfile,0,2);
		lseek(tfile,0,0);
		buff=(char *)realloc(buff,size);
		read(tfile,buff,size);
		head.size=size;
		close(tfile);
		perms_restore();
		/* end of setuid stuff */

		Lock_File(text);
		head.datafield=lseek(text,0,2);
		write(text,buff,size);
		Unlock_File(text);
	}else
	if (stringcmp(args,"to",-1))
	{
		char foo[SUBJECTSIZE+1];
		printf(_("Currently to '%s'.\n"),head.to);
		printf(_("to: "));
		get_str(foo,SUBJECTSIZE);
		if (*foo)
		{
			printf(_("Message now to '%s'\n"),foo);
			strcpy(head.to,foo);
		}else
			printf(_("Not Done.\n"));
	}else
	if (stringcmp(args,"subject",3))
	{
		char foo[SUBJECTSIZE+1];
		printf(_("Current subject is '%s'.\n"),head.subject);
		printf(_("Subject: "));
		get_str(foo,SUBJECTSIZE);
		if (*foo)
		{
			printf(_("Subject '%s'\n"),foo);
			strcpy(head.subject,foo);
		}else
			printf(_("Not Done.\n"));
	}else
	if (stringcmp(args,"delete",3))
	{
		head.status|=(1<<1);
		printf(_("Message Marked for deletion.\n"));
	}else
	if (stringcmp(args,"undelete",5))
	{
		head.status&=~(1<<1);
		printf(_("Message has been undeleted.\n"));
	}else
	if (stringcmp(args,"status",2))
	{
		char answer[11];
		show_mesg_stats(head.status,answer,1);
		printf(_("Status mode is currently %s\n"),answer);
		show_mesg_stats(255,answer,1);
		printf(_("New status [%s]: "),answer);
		get_str(answer,10);
		if (*answer)
		{
			head.status=mesg_stats(answer,head.status);
			show_mesg_stats(head.status,answer,1);
			printf(_("Status set to [%s]\n"),answer);
		}
	}
	else
		printf(_("What do you want to do ?\n"));
	Lock_File(fd);
	lseek(fd,-1*sizeof(head),1);
	write(fd,&head,sizeof(head));
	Unlock_File(fd);
	close(fd);
	close(text);
	free(buff);
}			

void time_on(long u)
{
	int s,m,h,d;

	d=u/(60*60*24);
	h=u%(60*60*24);
	h=h/(60*60);
	m=u%(60*60);
	m=m/60;
	s=u%60;

	printf(_("Total Login Time of "));
	if (d>0)
		printf(_("%d day%s, "),d,d>1?"s":"");
	if (h>0)
		printf(_("%d hour%s, "),h,h>1?"s":"");
	if (m>0)
		printf(_("%d minute%s, "),m,m>1?"s":"");
	printf(_("%d second%s.\n"),s,s==1?"":"s");
}

void edit_contact(void)
{
	char	contact[CONTACTSIZE+1];

	printf(_("Contact address: %s\n"),user->contact);
	printf(_("New address (%d chars): "), CONTACTSIZE);
	get_str(contact, CONTACTSIZE);
	if (*contact)
	{
		strcpy(user->contact, contact);
		printf(_("New address set.\n"));
		broadcast(3, "Contact for %s has just changed to %s.", user->name, contact);
		mwlog("CHANGE(CONTACT) of %s to %s", user->name, contact);
	}
	update_user(user,userposn);
}
