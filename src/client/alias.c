/*
 * Alias/Bind handling
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "talker.h"
#include "main.h"
#include "intl.h"
#include "userio.h"

#include "alias.h"
Alias alias_list=NULL;
Alias bind_list=NULL;
Alias rpc_list=NULL;
Alias event_list=NULL;
Alias onoff_list=NULL;
Alias ipc_list=NULL;
Alias force_list=NULL;
Alias shutdown_list=NULL;
Alias eventin_list=NULL;

/* completely destroys the given list */
void DestroyAllLinks(Alias *list)
{
	Alias free_me;

	while (*list!=NULL)
	{
		free((*list)->from);
		free((*list)->to);
		free_me = *list;
		*list = (*list)->next;
		free(free_me);
	}
}

/* removes the link given by 'name' in the list */
int DestroyLink(Alias *list, const char *name)
{
 	Alias prev, aptr;
	int found = 0;

	aptr = *list;
	prev=NULL;
	while (aptr!=NULL)
	{
		if (!strcasecmp(aptr->from, name))
		{
			found = 1;
		 	free(aptr->from);
		 	free(aptr->to);
		 	if (prev == NULL)
		 	{
		 		/* first */
		 		*list = aptr->next;
		 	}
		 	else if (aptr->next == NULL)
		 	{
		 		/* last */
		 		prev->next = NULL;
		 	}
		 	else
		 	{
		 		/* middle */
		 		prev->next = aptr->next;
		 	}
		 	free(aptr);
		 	break;
		}
		prev = aptr;
		aptr = aptr->next;
	}
	return(found);
}

/* adds the 'from/to' node to the given list, or redefines if in existance */
int AddLink(Alias *list, const char *from, const char *to)
{
	Alias new;
	int redefine = 0;

	new=*list;
	while (new!=NULL && strcasecmp(new->from, from))
	 	new=new->next;

	if (new!=NULL)
	{
		DestroyLink(list, from);
		redefine = 1;
	}

	new=(Alias)malloc(sizeof(struct alias_record));
	new->from=malloc(sizeof(char) * (strlen(from) + 1));
	new->to=malloc(sizeof(char) * (strlen(to) + 1));
	strcpy(new->from,from);
	strcpy(new->to,to);
	new->next=*list;
	*list=new;

	return (redefine);
}

/* displays the list */
void ShowLinks(Alias list, const char *prompt, const char *link, int count)
{
 	char	buff[10];
 	Alias	al;
 	int	max;
 	int	screen_height = screen_h();

	if (list != NULL)
	{
	 	al = list;
		max = count + 2;
	 	while (al != NULL)
 	 	{
	 	 	max++;
 			al=al->next;
 		}

		printf("\n");
		count++;
		if ((count >= (screen_height - 2)) && (count <= max))
		{
			printf(_("---more---\r"));
			get_str(buff, 5);
			printf("	  \r");
			if (*buff=='q' || *buff=='Q') return;
			count=0;
		}
		printf("%s\n", prompt);
	 	count++;
		if ((count >= (screen_height - 2)) && (count <= max))
		{
			printf(_("---more---\r"));
			get_str(buff, 5);
			printf("	  \r");
			if (*buff=='q' || *buff=='Q') return;
			count=0;
		}

		al = list;
		while (al != NULL)
		{
			printf("%s %s %s\n", al->from, link, al->to);
			count++;
			if ((count >= (screen_height - 2)) && (count <= max))
			{
				printf(_("---more---\r"));
				get_str(buff, 5);
				printf("	  \r");
				if (*buff=='q' || *buff=='Q') return;
				count=0;
			}
			al=al->next;
		}
	}
}

char *FindLinks(Alias list, const char *from)
{
	Alias al;

	al = list;
	while (al!=NULL && strcasecmp(from, al->from))
 	{
		al=al->next;
	}
	if (al==NULL)
		return(NULL);
	else
		return(strdup(al->to));
}

/* bind listing autocompletion for scripts and tab-completion */
char *list_bind(const char *text, int state)
{
	static Alias	bind;
	static int	len;
	char		*name;

	if (state == 0)
	{
		bind = bind_list;
		len = strlen(text);
	}

	while (bind != NULL)
	{
		name = bind->from;
		bind = bind->next;
		if ((len == 0) || !strncasecmp(name, text, len))
			return dupstr(name, "");
	}
	return NULL;
}


/* bind listing autocompletion for readline */
char *list_bind_rl(const char *text, int state)
{
	static Alias	bind;
	static int	len;
	char		*name;

	if (state == 0)
	{
		bind = bind_list;
		len = strlen(text);
		len--;
	}

	text++;

	while (bind != NULL)
	{
		name = bind->from;
		bind = bind->next;
		if ((len == 0) || !strncasecmp(name, text, len))
			return dupstr(name, ",");
	}
	return NULL;
}

char *NextLink(Alias list, char *prev)
{
	Alias al;

	al = list;
	if (prev)
	{
		while(al && strcasecmp(al->from, prev)) al = al->next;
		if (al) al = al->next;
	}

	if (!al) return(NULL);
	else return(al->from);
}
