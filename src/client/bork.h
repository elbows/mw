#ifndef BORK_H
#define BORK_H

#include "gagtable.h"

char *apply_bork(char *text, const struct pattern *list, int caps);

#endif /* BORK_H */
