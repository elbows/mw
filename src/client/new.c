/*********************************************************
 *     The Milliways III System is copyright 1992        *
 *      J.S.Mitchell. (arthur@sugalaxy.swan.ac.uk)       *
 *       see licence for furthur information.            *
 *********************************************************/

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#include <unistd.h>
#include "alarm.h"
#include "incoming.h"
#include "str_util.h"
#include "strings.h"
#include "talker.h"
#include "perms.h"
#include "read.h"
#include "main.h"
#include "add.h"
#include "intl.h"
#include "new.h"
#include "bb.h"
#include "userio.h"

extern int remote;
extern int busy;

void list_new_items(struct person *user, int flag)
/* flag true if only new messages to be listed */
{
	int file;
	int count=0,line=0;
	struct folder folstuff;
	struct folder *fol=&folstuff;
	int screen_height=screen_h();
	
	if (nofolders())
		{printf(_("There are no folders to read !\n")); return;}	
	file=openfolderfile(O_RDONLY);
	while (get_folder_entry(file,fol))
	{
		if (f_active(fol->status) 
		   && (allowed_r(fol,user)
		   || allowed_w(fol,user))) /*allowed*/
		if (!flag || (flag && (fol->last-user->lastread[count])>0 
		          && allowed_r(fol,user)
		          && get_subscribe(user,count))) /* subscribed */
		{
			printf("%*s (",FOLNAMESIZE,fol->name);
			putchar(allowed_r(fol,user)?'r':'-');
			putchar(allowed_w(fol,user)?'w':'-');
			putchar(is_private(fol,user)?'p':'-');
			putchar(is_moderated(fol,user)?'m':'-');
			putchar(get_subscribe(user,count)?'S':'-');
			line++;
			if (flag)
			{
				printf(_(")     Last read message %4d of %-4d  %4d new\n"),
				user->lastread[count],fol->last,
				fol->last-user->lastread[count]);
			}else
			{
				printf(_(") %4d Msgs  Topic: %s\n"),
				fol->last==0?0:fol->last-fol->first+1,
				fol->topic);
			}
		}
		count++;
		if (line>=(screen_height-1))
		{
			char tmp[4];
			printf(_("---more--- Press <return> to continue.\r"));
			get_str(tmp,3);
			if (tmp[0]=='q' || tmp[0]=='Q') {close(file);return;}
			line=0;
		}
	}
	close(file);
}

static int read_new(struct person *user, struct folder *data, int folnum)
{
	int indexfile,datafile;
	long posn;
	struct Header head;
	char tmp[80];

	if (access(buildpath(STATEDIR,data->name,INDEX_END,""),0)) 
		{printf(_("Error: cannot find index file for folder %s\n"),data->name);return(-1);}
	if (access(buildpath(STATEDIR,data->name,TEXT_END,""),0)) 
		{printf(_("Error: cannot find text file for folder %s\n"),data->name); return(-1);}
	if ((indexfile=err_open(buildpath(STATEDIR,data->name,INDEX_END,""),O_RDONLY,0))<0)
		{exit(-1);}
	if ((datafile=err_open(buildpath(STATEDIR,data->name,TEXT_END,""),O_RDONLY,0))<0)
		{exit(-1);}

	/* try jumping to expected location */	
	posn=(user->lastread[folnum]-data->first+1)*sizeof(struct Header);
	lseek(indexfile,posn,0);
	/* read the article, if failed, or overshot then rewind to start */
	if (!get_data(indexfile,&head) || head.Ref > user->lastread[folnum]) {
		lseek(indexfile,0,0);
		get_data(indexfile,&head);
	}

	while (head.Ref<=user->lastread[folnum] && get_data(indexfile,&head));
	if (!remote) printf("\n");
	do{
	    user->lastread[folnum]=head.Ref;
	    if (is_private(data,user)&&!stringcmp(head.to,user->name,-1))
	    { /* private  and not to you */
	    }else
	    if (head.status&(1<<1))
	    {
	    	if (!remote) printf(_("Skipping deleted message.\n"));
	    }else
	    if (head.status&(1<<3) && stringcmp(user->name,head.from,-1))
	    {
	    	/* was remotely posted by you. */
	    }else
	    {
		if (remote)
		{
			printf("MWYS\n");
			printf("%s\n",data->name);
		}
		display_article(&head,datafile);
		if (!remote)
		{
			DisplayStack();
			printf(_("Hit return for next message (%s - %d of %d)>"),data->name,head.Ref,data->last);
			get_str(tmp,80);
			reset_timeout(user->timeout);
			if (stringcmp(tmp,"catchup",2))
			{
				user->lastread[folnum]=data->last;
				printf(_("Catching up on folder %s\n"),data->name);
				break;
			}else
			if (stringcmp(tmp,"skip",-1))
			{
				printf(_("Skipping this folder. %d messages left unread.\n"),data->last-head.Ref);
				break;
			}else
			if (stringcmp(tmp,"reply",3))
			{
				long foo;
				foo=lseek(indexfile,0,1);
				add_msg(folnum,user,head.Ref);
				lseek(indexfile,foo,0);
				get_folder_number(data,folnum);
			}else
			if (stringcmp(tmp,"unsubscribe",5))
			{
				set_subscribe(user,folnum,false);
				printf(_("Unsubscribing from %s.\n"),data->name);
				break;
			}else
			if (strlen(tmp)) 
			{
				extern int last_mesg,currentfolder;
				close(datafile);
				close(indexfile);
				stack_str(tmp);
				currentfolder=folnum;
				last_mesg=head.Ref;
				return(true);
			}
		}
	    }
	}while (get_data(indexfile,&head));
	close(datafile);
	close(indexfile);
	return(0);
}

void new(struct person *user)
{		
	struct folder *data;	
	int folderfile;
	int folnum=0;
	
	data=(struct folder *)malloc(sizeof(*data));

	if (nofolders())
	{
		printf(_("There are no folders to read !\n"));
		free(data);
		return;
	}	
	folderfile=openfolderfile(O_RDONLY);
	while (get_folder_entry(folderfile,data))
	{
		if (data->status&1 /*active*/
		&& allowed_r(data,user) /* allowed to read */ 
	    	&& get_subscribe(user,folnum)  /* currently subscribed */
		&& !(data->last<data->first || data->last==0)
		&& !(data->last<=user->lastread[folnum])) 
		{
			if (!remote)
			{
				printf(_("Scanning folder %-*s\r"),FOLNAMESIZE,data->name);
				fflush(stdout);
			}
			if (read_new(user,data,folnum)) break;
		}
		folnum++;
	}
	free(data);
	close(folderfile);
}

void latest(struct person *user)
{
	int ffile,ifile;
	struct folder fold;
	char datestr[30],sub[40];
	
	if (nofolders())
	{
		printf(_("There are no folders to read !\n"));
		return;
	}
	ffile=openfolderfile(O_RDONLY);

	printf(_("    Folder  Mesg        Date                 From    Subject\n"));
	printf(_("    ======  ====        ====                 ====    =======\n"));
	while (get_folder_entry(ffile,&fold))
	{
		if (f_active(fold.status) && allowed_r(&fold,user))
		{
			char *x;
			time_t now, then;
			struct tm *t_then;

			x=buildpath(STATEDIR,fold.name,INDEX_END,"");
			printf("%*s  ",FOLNAMESIZE,fold.name);
			if (access(x,00))
				printf(_("            <empty>\n"));
			else
			{
				struct Header hdr;
				if ((ifile=err_open(x,O_RDONLY,0))<0)
					return;
				lseek(ifile,-1*sizeof(hdr),SEEK_END);
				read(ifile,&hdr,sizeof(hdr));

				now=time(0);
				then = hdr.date;
				t_then=localtime(&then);
				if (t_then == NULL)
					snprintf(datestr, 30, "Invalid Date");
				else
				if (now - hdr.date > 7776000)
				strftime(datestr, 30, "%a %b %d %Y ", t_then);
				else
				strftime(datestr, 30, "%a %b %d %H:%M", t_then);

				strncpy(sub,hdr.subject,30);
				sub[27]=0;
				if (strlen(hdr.subject)>30)
					strcat(sub,"...");
				printf("%4d  %s  %*s  %s\n",fold.last,datestr,
				NAMESIZE,hdr.from,sub);
				close(ifile);
			}
		}
	}
	close(ffile);
}
