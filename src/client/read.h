#ifndef READ_H
#define READ_H

#include <folders.h>
#include <mesg.h>

int get_mesg_header(struct folder *data, int msgnum, struct Header *head);
int get_data(int afile, struct Header *tmp);
void more(char *buff, int size);
void display_article(struct Header *tmp, int datafile);
int read_msg(int folnum, int msgnum, struct person *user);
void ls(int folnum, struct person *user, int many);

#endif /* READ_H */
