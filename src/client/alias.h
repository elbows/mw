#ifndef ALIAS_H
#define ALIAS_H

typedef struct alias_record 
{
	char *from;
	char *to;
	struct alias_record *next;
} *Alias;

void DestroyAllLinks(Alias *list);
int DestroyLink(Alias *list, const char *name);
int AddLink(Alias *list, const char *from, const char *to);
void ShowLinks(Alias list, const char *prompt, const char *link, int count);
char *FindLinks(Alias list, const char *from);
char *list_bind(const char *text, int state);
char *list_bind_rl(const char *text, int state);
char *NextLink(Alias list, char *prev);

#endif /* ALIAS_H */
