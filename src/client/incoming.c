/*********************************************************
 *     The Milliways III System is copyright 1992        *
 *      J.S.Mitchell. (arthur@sugalaxy.swan.ac.uk)       *
 *       see licence for furthur information.            *
 *********************************************************/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include "incoming.h"
#include "main.h"
#include "talker_privs.h"
#include "talker.h"
#include "special.h"
#include "rooms.h"
#include "alias.h"
#include "script.h"
#include "ipc.h"
#include "perms.h"
#include "mesg.h"
#include "echo.h"
#include "intl.h"
#include "bb.h"
#include "who.h"
#include "user.h"
#include "util.h"

extern Alias rpc_list;
extern Alias event_list;
extern Alias onoff_list;
extern Alias ipc_list;
extern struct person *user;
extern long userposn;
extern struct room myroom;
extern int quietmode;
extern ipc_connection_t *ipcsock;

int new_mail_waiting=0;
int mesg_waiting = 0;
struct IgnoreList *ignored;
char *mrod_user=NULL;

static int MesgStacked=0;
static int events_cancelled = 0;
static struct mstack *MesgStack=NULL;

static void accept_pipe_cmd(ipc_message_t *msg, struct person *mesg_user);
static void force_text(ipc_message_t *msg, const char *text, const char *from);
static void force_ipc(char *text, char *from);
static void force_rpc(char *text, char *from);
static void force_checkonoff(char *text, char *from);
static void force_wiz(char *text, char *from);
static void force_chatmode(char *text, unsigned long theirprivs, const char *from);
static void force_status(char *text, char *from);
static void force_channel(char *text, char *from, unsigned long theirprivs);
static void force_kick(char *text, char *from, unsigned long theirprivs);
static void force_gag(char *text, unsigned long theirprivs, const char *from);
static void zod(char *from, char *msg);
static void mrod(char *from, char *msg);
static void force_newmail(void);
static void force_clearign(void);
static void force_protlevel(char *text, unsigned long theirprivs, const char *from);
static void force_protpower(char *text);

#define _MIN(a,b) (a<b)?a:b

void InsertMesg(struct mstack *new)
{
	new->next=NULL;
	if (MesgStack==NULL)
	{
		MesgStack=new;
		MesgStacked=1;
		events_cancelled = 0;
	}else
	{
		struct mstack *ptr;
		ptr=MesgStack;
		while (ptr->next!=NULL) ptr=ptr->next;
		ptr->next=new;
		/* Stack must be emptied before events can start again */
		if (events_cancelled)
			ptr->flags &= ~MST_EVENT;
		MesgStacked++;
	}
}

void StackMesg(char *text, char *from, int flags)
{
	struct mstack *new;
	new=(struct mstack *)malloc(sizeof(struct mstack));
	new->text=(char *)malloc(strlen(text)+1);
	new->from=(char *)malloc(strlen(from)+1);
	strcpy(new->text,text);
	strcpy(new->from,from);
	new->flags = flags;
	new->preamble = 0;
	InsertMesg(new);
}

static void StackEvent(char *text, char *from, int flags)
{
	struct mstack *new;
	new=(struct mstack *)malloc(sizeof(struct mstack));
	new->text=(char *)malloc(strlen(text)+1);
	new->from=(char *)malloc(strlen(from)+1);
	strcpy(new->text,text);
	strcpy(new->from,from);
	new->flags = MST_SCREV;
	new->preamble = flags;
	InsertMesg(new);
}

void ClearStack(void) {
	struct mstack *old;
	
	/* Nuke the entire stack */
	while (MesgStack!=NULL) {
		old=MesgStack;
		MesgStack=old->next;
		free(old->text);
		free(old->from);
		free(old);
	};
	MesgStacked=0;
}

void DisplayStack(void)
{
	struct mstack	*new;
	struct mstack	*old;
	char		*event_name;

	new=MesgStack;
	while (new!=NULL)
	{
		if (new->flags & MST_EVENT)
		{
			if (cp_flags(user->chatprivs,CP_SCRIPT,CM_MODE_ANY))
			{
				event_name = NULL;
				script_output=1;
				while ((event_name = NextLink(event_list, event_name)) != NULL)
				{
					ExecEvent(event_name, new->text, "text", new->from, new->preamble);
				}
				if (script_output) display_message(new->text, new->flags & MST_BEEP, 1);
				script_output = 1;
			}
			else display_message(new->text, new->flags & MST_BEEP, 1);
		}
		else if (new->flags & MST_SCREV)
		{
			switch(new->preamble)
			{
			case EST_RPC:
			{
				char *msg;
				char *funcname;
				char callfunc[MAXTEXTLENGTH];

				msg = remove_first_word(new->text);
				snprintf(callfunc, (MAXTEXTLENGTH-1<strlen(new->text))?MAXTEXTLENGTH-1:strlen(new->text) - strlen(msg), "%s", new->text);
				script_output=1;

				if ((funcname = FindLinks(rpc_list, callfunc)) != NULL)
				{
					ExecEvent(funcname, msg, "RPC", new->from, 0);
					free(funcname);
				}
				free(msg);
				break;
			}
			case EST_IPC:
			{
				char	*ipc_name;

				/* go through list of ipc functions */
				ipc_name = NULL;
				script_output=1;
				while ((ipc_name = NextLink(ipc_list, ipc_name)) != NULL)
				{
					ExecEvent(ipc_name, new->text, "IPC", new->from, 0);
				}
				break;
			}
			case EST_CHECKONOFF:
			{
				char	*aargs[6];
				char	*backup;
				char	*onoff_name, *sep, *head, *head2;
				char	*uname, *reason = NULL;
				int	ccode, method, isquiet;

				/* go through list and find checkonoff function */
				onoff_name = NULL;
				onoff_name = NextLink(onoff_list, onoff_name);

				/* if no function found, skip everything else */
				if (onoff_name == NULL) break;

				/* backup the input text */
				backup = strdup(new->text);

				/* get the first comma in the checkonoff code */
				sep = strchr(new->text, ',');

				/* if this comma does not exist, use the original version of checkonoff */
				if (sep == NULL)
				{
					/* simply convert to a number */
					ccode = atoi(new->text);

					/* due to backwards compatibility issues, we can only use 0 or 1 */
					if (ccode < 0) { free(backup); break; }
					if (ccode > 1) { free(backup); break; }

					/* create memory for argument strings */
					aargs[0] = malloc(sizeof(char) * 13);
					aargs[1] = malloc(sizeof(char) * 13);
					aargs[2] = malloc(sizeof(char) * 3);
					aargs[3] = malloc(sizeof(char) * NAMESIZE + 1);
					aargs[4] = malloc(sizeof(char) * 3);
					aargs[5] = malloc(sizeof(char) * MAXTEXTLENGTH);

					/* set up the argument strings */
					snprintf(aargs[0], 12, "%d", ccode);
					aargs[1][0] = 0;
					aargs[2][0] = 0;
					aargs[3][0] = 0;
					snprintf(aargs[4], 2, "1");
					aargs[5][0] = 0;

					/* run the event */
					ExecEvent2(onoff_name, "CheckOnOff", new->from, 0, 6, aargs);

					/* free memory and break out */
					free(backup);
					free(aargs[0]);
					free(aargs[1]);
					free(aargs[2]);
					free(aargs[3]);
					free(aargs[4]);
					free(aargs[5]);
					break;
				}

				/* set head to the character after the comma, and terminate the code before the comma */
				head = sep + 1;
				*sep = 0;

				/* get the checkonoff code number */
				ccode = atoi(new->text);

				/* get the next comma in the checkonoff code */
				sep = strchr(head, ',');
				/* if no comma found, then this must be an interim checkonoff */
				if (sep == NULL)
				{
					/* due to backwards compatibility issues, we can only use 0 or 1 */
					if (ccode < 0) { free(backup); break; }
					if (ccode > 1) { free(backup); break; }

					/* simply convert the next argument to a number */
					isquiet = atoi(head);

					/* create memory for argument strings */
					aargs[0] = malloc(sizeof(char) * 13);
					aargs[1] = malloc(sizeof(char) * 13);
					aargs[2] = malloc(sizeof(char) * 3);
					aargs[3] = malloc(sizeof(char) * NAMESIZE + 1);
					aargs[4] = malloc(sizeof(char) * 3);
					aargs[5] = malloc(sizeof(char) * MAXTEXTLENGTH);

					/* set up the argument strings */
					snprintf(aargs[0], 12, "%d", ccode);
					aargs[1][0] = 0;
					snprintf(aargs[2], 2, "%d", isquiet);
					aargs[3][0] = 0;
					snprintf(aargs[4], 2, "2");
					aargs[5][0] = 0;

					/* run the event */
					ExecEvent2(onoff_name, "CheckOnOff", new->from, 0, 6, aargs);

					/* free memory and break out */
					free(backup);
					free(aargs[0]);
					free(aargs[1]);
					free(aargs[2]);
					free(aargs[3]);
					free(aargs[4]);
					free(aargs[5]);
					break;
				}

				/* set second text head to the character after the comma, and terminate the method before the comma */
				head2 = sep + 1;
				*sep = 0;

				/* get the checkonoff method number */
				method = atoi(head);

				/* get the next comma in the checkonoff code */
				sep = strchr(head2, ',');
				/* if no comma found, then this must be broken! */
				if (sep == NULL)
				{
					printf("Invalid checkonoff broadcast received! (%s)\n", backup);
					free(backup);
					break;
				}

				/* set uname to the character after the comma, and terminate the method before the comma */
				uname = sep + 1;
				*sep = 0;

				/* get the checkonoff quiet flagr */
				isquiet = atoi(head2);

				/* get the optional next comma in the checkonoff code */
				sep = strchr(uname, ',');
				/* if comma is found, then the reason is there */
				if (sep != NULL)
				{
					reason = sep + 1;
					*sep = 0;
				}

				/* limit the method information */
				switch (ccode)
				{
				/* leave the talker */
				case 0:
					if (!cp_flags(user->chatprivs, CP_CANZOD, CM_MODE_ANY) && (method == 1))
					{
						method = 0;
						uname = new->from;
					}
					if (!u_god(user->status) && (method == 2))
					{
						method = 0;
						uname = new->from;
					}
					break;
				/* enter the talker */
				case 1:
					if (!cp_flags(user->chatprivs, CP_SUMMON, CM_MODE_ANY) && (method == 1))
					{
						method = 0;
						uname = new->from;
					}
					if (!u_god(user->status) && (method == 2))
					{
						method = 0;
						uname = new->from;
					}
					break;
				/* leave the board */
				case 2:
					if (!cp_flags(user->chatprivs, CP_CANMROD, CM_MODE_ANY) && (method == 3))
					{
						method = 0;
						uname = new->from;
					}
					if (!u_god(user->status) && ((method == 4) || (method == 5) || (method == 6)))
					{
						method = 0;
						uname = new->from;
					}
					break;
				/* enter the board */
				case 3:
					break;
				}

				/* create memory for argument strings */
				aargs[0] = malloc(sizeof(char) * 13);
				aargs[1] = malloc(sizeof(char) * 13);
				aargs[2] = malloc(sizeof(char) * 3);
				aargs[3] = malloc(sizeof(char) * NAMESIZE + 1);
				aargs[4] = malloc(sizeof(char) * 3);
				aargs[5] = malloc(sizeof(char) * MAXTEXTLENGTH);

				/* set up the argument strings */
				snprintf(aargs[0], 12, "%d", ccode);
				snprintf(aargs[1], 12, "%d", method);
				snprintf(aargs[2], 2, "%d", isquiet);
				snprintf(aargs[3], NAMESIZE, "%s", uname);
				snprintf(aargs[4], 2, "3");
				if (reason) snprintf(aargs[5], MAXTEXTLENGTH - 1, "%s", reason);
				else aargs[5][0] = 0;

				/* display the broadcast message */
				ExecEvent2(onoff_name, "CheckOnOff", new->from, 0, 6, aargs);

				/* free up the memory used */
				free(aargs[0]);
				free(aargs[1]);
				free(aargs[2]);
				free(aargs[3]);
				free(aargs[4]);
				free(aargs[5]);
				free(backup);
				break;
			}
			default:
				break;
			}
		}
		else display_message(new->text, new->flags & MST_BEEP, 1);

		free(new->text);
		free(new->from);
		old=new->next;
		free(new);
		MesgStacked--;
		new=old;
	}
	MesgStack=NULL;
}

int MesgIsStacked(void)
{
	return (MesgStacked > 0);
}

void handle_mesg()
{
	static struct person mesg_user;

	mesg_waiting = 0;

	ipc_message_t * msg = read_socket(ipcsock, 1);

	while (msg != NULL) {
		if (msg->head.src == SYSTEM_USER) {
			strcpy(mesg_user.name,"System");
		} else {
			fetch_user(&mesg_user, msg->head.src);
		}
		accept_pipe_cmd(msg, &mesg_user);

		msg = read_socket(ipcsock, 0);
	}
	if (msg == NULL && !ipc_connected()) {
		if (ipcsock->fd != -1) {
			close(ipcsock->fd);
			ipcsock->fd = -1;
		}
		fprintf(stderr, "Server disconnected.\n");
	}
	if (cm_flags(user->chatmode,CM_ONCHAT,CM_MODE_ANY)) set_talk_rights(); else set_rights();
}

static void force_lastread(const char * newbuff) {
	int fol;
	int pos;

	sscanf(newbuff,"%d:%d",&fol,&pos);
	user->lastread[fol]=pos;
}
			
static void display_uptime(ipc_message_t *msg)
{
	json_t * j = json_init(msg);
	const char *version = json_getstring(j, "version");
	const int uptime = json_getint(j, "uptime");

	printf("Server version: %s\n", version);
	printf("Server uptime: %s\n", itime(uptime));
	json_decref(j);
}

static void display_error(ipc_message_t *msg)
{
	json_t * j = json_init(msg);
	const char * type = json_getstring(j, "type");
	const char * text = json_getstring(j, "text");

	char buff[MAXTEXTLENGTH];
	char * tb = buff;
	int len = MAXTEXTLENGTH;

	if (msg->head.type == IPC_TALKERROR) {
		snprintf(tb, len, "Talker Error: %s", text);
	} else {
		snprintf(tb, len, "Unknown %s Error: %s", type, text);
	}

	force_text(msg, buff, "System");
	json_decref(j);
}


/* handle formatted message */
static void display_content(ipc_message_t *msg)
{
	int32_t msg_posn = -1;
	struct person msg_user;
	const char *whom = NULL;

	if (msg->head.src == SYSTEM_USER) {
		whom = "System";
	} else {
		msg_posn = msg->head.src;
		fetch_user(&msg_user, msg_posn);
		whom = msg_user.name;
	}

	json_t * j = json_init(msg);

	if (msg->head.type == IPC_SAYTOROOM) {
		const char * type = json_getstring(j, "type");
		const char * text = json_getstring(j, "text");

		if (type == NULL) type="unknown";
	       	if (text == NULL) {
			printf("Invalid SAYTOROOM message from %s.\n", whom);
			json_decref(j);
			return;
		}

		char buff[MAXTEXTLENGTH];
		char * tb = buff;
		int len = MAXTEXTLENGTH;

		/* we have global on, prepend the channel number */
		if (cm_flags(user->chatmode, CM_GLOBAL, CM_MODE_ALL)) {
			snprintf(tb, len, "%d:", msg->head.dst);
			int s = strlen(tb);
			len -= s;
			tb += s;
		}

		if (strcmp(type, "say")==0) {
			snprintf(tb, len, "%s: %s", whom, text);
		} else
		if (strcmp(type, "raw")==0) {
			snprintf(tb, len, "%s", text);
		} else 
		if (strcmp(type, "emote")==0) {
			int plural = json_getint(j, "plural");
			switch (plural) {
				case 1:
					snprintf(tb, len, "%s's %s", whom, text);
					break;
				case 2:
					snprintf(tb, len, "%s' %s", whom, text);
					break;
				case 3:
					snprintf(tb, len, "%s'd %s", whom, text);
					break;
				case 4:
					snprintf(tb, len, "%s'll %s", whom, text);
					break;
				default:
					snprintf(tb, len, "%s %s", whom, text);
					break;
			}
		} else
		if (strcmp(type, "notsayto")==0) {
			const char *exclude = json_getstring(j, "exclude");
			if (exclude == NULL) exclude="Unknown";
			if (text[strlen(text)-1] == '?')
				snprintf(tb, len, "%s asks (-%s): %s", whom, exclude, text);
			else
				snprintf(tb, len, "%s says (-%s): %s", whom, exclude, text);
		} else {
			/* same as say for now */
			snprintf(tb, len, "%s: %s", whom, text);
		}
		force_text(msg, buff, whom);
	} else
	if (msg->head.type == IPC_SAYTOUSER) {
		const char * type = json_getstring(j, "type");
		const char * text = json_getstring(j, "text");

		if (type == NULL || text == NULL) {
			printf("Invalid SAYTOUSER message from %s.\n", whom);
			json_decref(j);
			return;
		}

		char buff[MAXTEXTLENGTH];
		char * tb = buff;
		int len = MAXTEXTLENGTH;

		/* we have global on, prepend the channel number */
		if (cm_flags(user->chatmode, CM_GLOBAL, CM_MODE_ALL)) {
			if (msg_posn == -1)
				snprintf(tb, len, "?:");
			else
				snprintf(tb, len, "%d:", msg_user.room);
			int s = strlen(tb);
			len -= s;
			tb += s;
		}
	
		if (strcmp(type, "whispers")==0) {
			snprintf(tb, len, "%s whispers: %s", whom, text);
		} else
		if (strcmp(type, "asks")==0) {
			snprintf(tb, len, "%s asks: %s", whom, text);
		} else  {
			snprintf(tb, len, "%s says: %s", whom, text);
		}
		force_text(msg, buff, whom);
	} else
	if (msg->head.type == IPC_SAYTOALL) {
		const char * text = json_getstring(j, "text");

		if (text == NULL) {
			printf("Invalid SAYTOALL message from %s.\n", whom);
			json_decref(j);
			return;
		}

		char buff[MAXTEXTLENGTH];
		char * tb = buff;
		int len = MAXTEXTLENGTH;

		/* we have global on, prepend the channel number */
		if (cm_flags(user->chatmode, CM_GLOBAL, CM_MODE_ALL)) {
			snprintf(tb, len, "*:");
			int s = strlen(tb);
			len -= s;
			tb += s;
		}
	
		snprintf(tb, len, "%s shouts: %s", whom, text);
		force_text(msg, buff, whom);
	} else
	if (msg->head.type == IPC_EVENT) {
		const char * type = json_getstring(j, "type");
		const char * text = json_getstring(j, "text");
		const char * verbose = json_getstring(j, "verbose");
		const char * reason = json_getstring(j, "reason");

		if (verbose) force_text(msg, verbose, whom);
		force_text(msg, text, whom);
		if (reason) {
			AUTOFREE_BUFFER excuse = NULL;
			if (strcasecmp(type, "mrod")==0 || strcasecmp(type, "zod")==0) {
				asprintf(&excuse, "Zebedee says \"%s\".", reason);
			} else {
				asprintf(&excuse, "%s gives the reason \"%s\".", whom, reason);
			}
			force_text(msg, excuse, whom);
		}
	} else {
		printf("Unknown message type %4.4s", (char *)&msg->head.type);
	}

	json_decref(j);

}

static void accept_pipe_cmd(ipc_message_t *msg, struct person *mesg_user)
{
	enum ipc_types state = msg->head.type;
	char *newbuff = msg->body;

	/*printf("\n<message type is %d>\n", state);*/
	switch (state) {
		case IPC_NEWMAIL:
			force_newmail();
			break;
		case IPC_STATUS:
			force_status(newbuff, mesg_user->name);
			break;
		case IPC_GROUPS:
			user->groups=folder_groups(newbuff,user->groups);
			break;
		case IPC_REALNAME:
			strcpy(user->realname,newbuff);
			break;
		case IPC_PASSWD:
			strcpy(user->passwd,newbuff);
			break;
		case IPC_CONTACT:
			strcpy(user->contact,newbuff);
			break;
		case IPC_DOING:
			snprintf(user->doing,DOINGSIZE,"%s",newbuff);
			user->dowhen=time(0);
			break;
		case IPC_SPECIAL:
			user->special=set_special(newbuff,user->special);
			break;
		case IPC_CHATPRIVS:
			user->chatprivs=cp_setbycode(user->chatprivs, newbuff);
			break;
		case IPC_CHATMODE:
			force_chatmode(newbuff, mesg_user->chatprivs, mesg_user->name);
			break;
		case IPC_GAG:
			force_gag(newbuff, mesg_user->chatprivs, mesg_user->name);
			break;
		case IPC_PROTLEVEL:
			force_protlevel(newbuff, mesg_user->chatprivs, mesg_user->name);
			break;
		case IPC_PROTPOWER:
			force_protpower(newbuff);
			break;
		case IPC_USERNAME:
			snprintf(user->name, NAMESIZE+1,"%s",newbuff);
			printf(_("\nYour name has been changed to '%s'\n"),user->name);
			break;
		case IPC_LASTREAD:
			force_lastread(newbuff);
			break;
		case IPC_TIMEOUT:
			user->timeout=atoi(newbuff);
			reset_timeout(user->timeout);
			break;
		case IPC_TEXT:
			if (!is_ignored(mesg_user->name, NULL)) force_text(msg, newbuff, mesg_user->name);
			break;
		case IPC_SCRIPTIPC:
			if (!is_ignored(mesg_user->name, NULL)) force_ipc(newbuff, mesg_user->name);
			break;
		case IPC_SCRIPTRPC:
			if (!is_ignored(mesg_user->name, NULL)) force_rpc(newbuff, mesg_user->name);
			break;
		case IPC_CHECKONOFF:
			force_checkonoff(newbuff, mesg_user->name);
			break;
		case IPC_WIZ:
			force_wiz(newbuff, mesg_user->name);
			break;
		case IPC_CHANNEL:
			force_channel(newbuff, mesg_user->name, mesg_user->chatprivs);
			break;
		case IPC_KICK:
			force_kick(newbuff, mesg_user->name, mesg_user->chatprivs);
			break;
		case IPC_CLEARIGN:
			force_clearign();
			break;
		case IPC_UPTIME:
			display_uptime(msg);
			break;
		case IPC_SAYTOROOM:
		case IPC_SAYTOUSER:
		case IPC_SAYTOALL:
		case IPC_EVENT:
			display_content(msg);
			break;
		case IPC_TALKERROR:
			display_error(msg);
			break;
		default:
			devel_msg("incoming_mesg", "unknown message type %d.\007", state);
	}

	ipcmsg_destroy(msg);
	update_user(user,userposn);	
}

static void force_newmail(void)
{
	new_mail_waiting++;
}

static void force_text(ipc_message_t *msg, const char *text, const char *from)
{
	char tb[MAXTEXTLENGTH];
	struct mstack *mesg;

	mesg = malloc(sizeof(*mesg));
	mesg->flags = MST_EVENT|MST_BEEP;
	
	tb[0]=0;
	if (s_timestamp(user->special))
	{
		time_t t;
		struct tm *tt;
 
		t= msg->head.when;
		tt=localtime(&t);
		strftime(tb, MAXTEXTLENGTH,"%H:%M ", tt);
	}
	if (cm_flags(user->chatmode, CM_SPY, CM_MODE_ALL))
	{
		strncat(tb, from, MAXTEXTLENGTH - strlen(tb) - 1);
		strncat(tb, "> ", MAXTEXTLENGTH - strlen(tb) - 1);
	}
	mesg->preamble = strlen(tb);
	if (cm_flags(user->chatmode, CM_GLOBAL, CM_MODE_ALL))
	{
		char *i = strchr(text, ':');
		if (i != NULL)
		{
			mesg->preamble += (i-text+1);
		}
	}
	strncat(tb, text, MAXTEXTLENGTH - strlen(tb) - 1);
	mesg->text = strdup(tb);
	mesg->from = strdup(from);

	/*printf("\n<Received message from %s: \'%s\'>\n", from, text);*/
	InsertMesg(mesg);
}

static void force_wiz(char *newbuff, char *from)
{
	char tb[MAXTEXTLENGTH];
	tb[0]=0;

	if (s_timestamp(user->special))
	{
		time_t t;
		struct tm *tt;

		t=time(0);
		tt=localtime(&t);
		strftime(tb, MAXTEXTLENGTH,"%H:%M ", tt);
	}
	if (cm_flags(user->chatmode, CM_SPY, CM_MODE_ALL))
	{
		strncat(tb, from, MAXTEXTLENGTH - strlen(tb) - 1);
		strncat(tb, "> ", MAXTEXTLENGTH - strlen(tb) - 1);
	}
	strncat(tb, newbuff, MAXTEXTLENGTH - strlen(tb) - 1);

	StackMesg(tb, from, 0);
}

static void force_checkonoff(char *text, char *from)
{
	if (cp_flags(user->chatprivs,CP_SCRIPT,CM_MODE_ANY))
	{
		StackEvent(text, from, EST_CHECKONOFF);
	}
}

static void force_ipc(char *text, char *from)
{
	if (cp_flags(user->chatprivs,CP_SCRIPT,CM_MODE_ANY))
	{
		StackEvent(text, from, EST_IPC);
	}
}

static void force_rpc(char *text, char *from)
{
	if (cp_flags(user->chatprivs,CP_SCRIPT,CM_MODE_ANY))
	{
		StackEvent(text, from, EST_RPC);
	}
}

static void force_clearign(void)
{
	struct IgnoreList *temp;

	/* unignore everyone in list */
	while (ignored != NULL)
	{
		temp = ignored;
		ignored = ignored->next;
		free(temp->name);
		free(temp);
	}
	display_message(_("*** Your ignore list has just been cleared ***"), 1, 1);
}

static void force_status(char *newbuff, char *from)
{
	user->status = user_stats(newbuff, user->status);
	/* we have received a ban */
	if (u_ban(user->status))
	{
		printf(_("\n\n--> You appear to have been banned. Goodbye... <--\r\n"));
		close_down(4, from, NULL);
	}
	/* we have received a +D status change */
	if (u_del(user->status))
	{
		printf(_("\n\n--> You appear to have been DELETED. Goodbye... <--\r\n"));
		close_down(6, from, NULL);
	}
}

static void force_chatmode(char *newbuff, unsigned long theirprivs, const char *from)
{
	unsigned long	mm;
	int		ourapl = (user->chatprivs & CP_PROTMASK) >> CP_PROTSHIFT;
	int		theirapl = (theirprivs & CP_PROTMASK) >> CP_PROTSHIFT;
	int		oldchat;

	if (!cm_flags(theirprivs, CP_PROTECT, CM_MODE_ALL)) theirapl = 0;
	oldchat = cm_flags(user->chatmode, CM_ONCHAT, CM_MODE_ALL);

	mm=cm_setbycode(user->chatmode, newbuff);
	user->chatmode=chatmode_describe(user->chatmode, mm, ourapl, theirapl, from);

	if (!cm_flags(user->chatmode, CM_ONCHAT, CM_MODE_ANY) && oldchat)
	{
		/* announce leaving talker */
		broadcast_onoffcode(0, 2, from, NULL);
	}
	else if (cm_flags(user->chatmode, CM_ONCHAT, CM_MODE_ANY) && !oldchat)
	{
		/* announce joining the talker */
		broadcast_onoffcode(1, 2, from, NULL);
	}

	disable_rl(1);
}

static void force_gag(char *newbuff, unsigned long theirprivs, const char *from)
{
	int ourapl = (user->chatprivs & CP_PROTMASK) >> CP_PROTSHIFT;
	int theirapl = (theirprivs & CP_PROTMASK) >> CP_PROTSHIFT;
	if (!cm_flags(theirprivs, CP_PROTECT, CM_MODE_ALL)) theirapl = 0;


	unsigned long newcm = user->chatmode & ~CM_GAGMASK;
	if (newbuff[0]=='+') 
		newcm |= atoi(&newbuff[1]) << CM_GAGSHIFT;

	user->chatmode=chatmode_describe(user->chatmode, newcm, ourapl, theirapl, from);

	disable_rl(1);
}

static void force_protlevel(char *newbuff, unsigned long theirprivs, const char* from)
{
	unsigned long cm, pbits;
	int level;
	int ourapl = (user->chatprivs & CP_PROTMASK) >> CP_PROTSHIFT;
	int theirapl = (theirprivs & CP_PROTMASK) >> CP_PROTSHIFT;
	if (!cm_flags(theirprivs, CP_PROTECT, CM_MODE_ALL)) theirapl = 0;

	level = atoi(newbuff);
	/* `pbits' contains both the temporary protect bit and permanent
	 * protection level.  if level==-1 then give temp protection only
	 */
	pbits = (level>=0? level: 0) << CM_PROTSHIFT;
	if (level != 0) pbits |= CM_PROTECTED;
	cm=user->chatmode;
	cm ^= pbits; cm &= ~(CM_PROTMASK|CM_PROTECTED); cm ^= pbits;

	user->chatmode=chatmode_describe(user->chatmode, cm, ourapl, theirapl, from);
}

static void force_protpower(char *newbuff)
{
	unsigned long cp, pbits;

	pbits = atoi(newbuff) << CP_PROTSHIFT;
	cp=user->chatprivs;

	cp ^= pbits; cp &= ~CP_PROTMASK; cp ^= pbits;

	user->chatprivs=cp;
}

static void force_channel(char *newbuff, char *from, unsigned long theirprivs)
{
	int newroom=atoi(&newbuff[1]);
	char mode = newbuff[0];
	char text[MAXTEXTLENGTH];
	int theirapl = (theirprivs & CP_PROTMASK) >> CP_PROTSHIFT;
	int prot = (user->chatmode&CM_PROTMASK) >> CM_PROTSHIFT;
	if (!cm_flags(theirprivs, CP_PROTECT, CM_MODE_ALL)) theirapl = 0;
	if (prot == 0 && cm_flags(user->chatmode, CM_PROTECTED, CM_MODE_ALL))
		prot = 1;

	if (!cm_flags(user->chatmode,CM_ONCHAT,CM_MODE_ANY))
	{
		if ((mode != 's') && (theirapl<prot))
		{
			snprintf(text,MAXTEXTLENGTH-1,"\03315%s just tried to summon you onto the talker into room %d.",from,newroom);
			display_message(text, 1, 1);
		}else
		{
			snprintf(text,MAXTEXTLENGTH-1,"\03315You have been summoned into talker room %d by %s.",newroom,from);
			display_message(text, 1, 1);
			set_talk_rights();
			user->room=newroom;

			snprintf(text,MAXTEXTLENGTH-1,"\03310%s has just been summoned into talker room %d by %s",user->name,newroom,from);
			talk_send_rawbcast(text);
			user->chatmode=cm_flags(user->chatmode,CM_ONCHAT,CM_MODE_SET);

			RoomDestroy(&myroom);
			RoomInit(&myroom);
			LoadRoom(&myroom, newroom);

			/* force update of user information _before_ scripts are run */
			update_user(user, userposn);

			/* run common talker entrance code with logon type set to 'summon' */
			enter_talker(2);

			/* give an entrance broadcast */
			broadcast_onoffcode(1, 1, from, NULL);

			enter_room(newroom);
		}
	}else
	{
		if ((mode != 's') && (theirapl<prot))
		{
			/*
			snprintf(text,MAXTEXTLENGTH-1,"%s just tried to summon you to room %d.",from,newroom);
			display_message(text, 1, 1);
			*/
		}else
		{
			int o;
			snprintf(text,MAXTEXTLENGTH-1,"\03312%s has just arrived in room %d",user->name,newroom);
			talk_send_raw(text,newroom);
			o=user->room;
			user->room=(unsigned short)newroom;
			snprintf(text,MAXTEXTLENGTH-1,"\03313%s has left to room %d",user->name, newroom);
			talk_send_raw(text,o);
			snprintf(text,MAXTEXTLENGTH-1,"\03315You have been summoned to room %d by %s.",newroom,from);
			display_message(text, 1, 1);

			RoomDestroy(&myroom);
			RoomInit(&myroom);
			LoadRoom(&myroom, newroom);
			enter_room(newroom);
		}
	}
	disable_rl(1);

}

static void force_kick(char *newbuff, char *from, unsigned long theirprivs)
{
	char	*msg = NULL;
	char	type;
	
	type = *newbuff;
	if (newbuff[1] == 'r') msg = &newbuff[2];

	if (type=='z')
	{
		zod(from, msg);
	}else
	if (type=='m')
	{
		mrod(from, msg);
	} else
	if (type=='k')
	{
		zod(from, msg);
	}else
	if (type=='r')
	{
		mrod(from, msg);
	}
}

static void zod(char *from, char *msg)
{
	char text[MAXTEXTLENGTH];

	/* remove from talker */
	user->chatmode=cm_flags(user->chatmode, CM_ONCHAT, CM_MODE_CLEAR);

	/* force update of user */
	update_user(user,userposn);

	/* announce leaving talker via zod */
	broadcast_onoffcode(0, 1, from, msg);

	/* put standard message to screen */
	snprintf(text, MAXTEXTLENGTH-1, _("\nBoing, Zebedee arrived.  \"%s\033--\", said Zebedee\n"), (msg!=NULL)?msg:_("Time for bed"));
	display_message(text, 1, 1);
	printf(_("%s just sent the Zebedee of Death to you.\n"),from);
	fflush(stdout);

	/* broadcast leaving message if not quiet */
	if (!quietmode) broadcast(4|0x100, "\03311%s has just left the talker", user->name);

	/* set 'off talker' permissions for commands */
	set_rights();
	disable_rl(1);
}

static void mrod(char *from, char *msg)
{
	char mrod_message[MAXTEXTLENGTH];
	/* make sure we have left talker */
	/*user->chatmode=cm_flags(user->chatmode, CM_ONCHAT, CM_MODE_CLEAR);*/
	update_user(user,userposn);

	printf("\n");
	printf(_("Boing, Zebedee's arrived.  \"Look up!\", says Zebedee\n"));
	printf(_("You look up; a large object is falling towards you very fast,\n"));
	printf(_("very very fast.  It looks like a Magic Roundabout!\n"));
	printf(_("\"I wouldn't stand there if I was you\", says Zebedee\n"));
	printf(_("Boing, Zebedee's left you standing all alone\n"));
	printf(_("WWWHHHEEEEEEEKKKKEEEERRRRRUUUUUNNNNNCCCCCHHHHHH\7\7\7\7\7\n"));
	printf(_("%s has just dropped the Magic Roundabout of Death on you.\n"),from);
	if (msg!=NULL) 
	{
		snprintf(mrod_message, MAXTEXTLENGTH-1, _("\"%s\033--\" says Zebedee\n"), msg);
		display_message(mrod_message, 1, 1);
	}
	fflush(stdout);

	fix_terminal();
	
	mrod_user = malloc(sizeof(char) * (strlen(from) + 1));
	strcpy(mrod_user, from);
	close_down(3, from, msg);
}

