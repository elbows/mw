#ifndef NEW_H
#define NEW_H

#include "user.h"

void list_new_items(struct person *user, int flag);
void new(struct person *user);
void latest(struct person *user);

#endif /* NEW_H */
