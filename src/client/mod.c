/*********************************************************
 *     The Milliways III System is copyright 1992        *
 *      J.S.Mitchell. (arthur@sugalaxy.swan.ac.uk)       *
 *       see licence for furthur information.            *
 *********************************************************/
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include "files.h"
#include "str_util.h"
#include "read.h"
#include "intl.h"
#include "mod.h"
#include "bb.h"
#include "userio.h"

static void add_message(struct folder *fold, struct Header *head, char *text)
{
	char fullpath[PATHSIZE];
	int modindex,modtext;
	
	/* files for general public to read */
	sprintf(fullpath,"%s/%s%s",STATEDIR,fold->name,INDEX_END);
	if ((modindex=open(fullpath,O_WRONLY|O_APPEND|O_CREAT,0600))<0)
		{perror(fullpath);exit(-1);}
	sprintf(fullpath,"%s/%s%s",STATEDIR,fold->name,TEXT_END);
	if ((modtext=open(fullpath,O_WRONLY|O_CREAT,0600))<0)
		{perror(fullpath);exit(-1);}
	Lock_File(modindex);
	Lock_File(modtext);
	head->Ref=fold->last;
	head->datafield=lseek(modtext,0,2);
	if (write(modindex,head,sizeof(*head))<0) {perror("mod: writehead");}
	if (write(modtext,text,head->size)<0) {perror("mod: write text");}
	
	Unlock_File(modtext);
	Unlock_File(modindex);
	close(modtext);
	close(modindex);
}

static void put_back(struct folder *fold, struct Header *head, char *text)
{
	char fullpath[PATHSIZE];
	int newindex,newtext;
	
		/* files to put messages to be approved later */
	sprintf(fullpath,"%s/%s%s%s",STATEDIR,fold->name,INDEX_END,MOD_END);
	if ((newindex=open(fullpath,O_WRONLY|O_APPEND|O_CREAT,0600))<0)
		{perror(fullpath);exit(-1);}
	sprintf(fullpath,"%s/%s%s%s",STATEDIR,fold->name,TEXT_END,MOD_END);
	if ((newtext=open(fullpath,O_WRONLY|O_APPEND|O_CREAT,0600))<0)
		{perror(fullpath);exit(-1);}
	Lock_File(newindex);
	Lock_File(newtext);
	head->datafield=lseek(newtext,0,2);
	head->Ref=0;
	write(newindex,head,sizeof(*head));
	write(newtext,text,head->size);
	Unlock_File(newtext);
	Unlock_File(newindex);
	close(newindex);
	close(newtext);
}

static void store(struct folder *fold, struct Header *head, char *text)
{
	int afile;
	char buff[128];
	char fullpath[PATHSIZE];
	time_t head_date;
	
	sprintf(fullpath,"%s/%s",STATEDIR,"mod.save");
	head_date = (time_t)head->date;
	if ((afile=open(fullpath,O_WRONLY|O_APPEND|O_CREAT,0600))<0)
		{perror(fullpath);exit(-1);}
	
	Lock_File(afile);
	sprintf(buff,_("\nMessage in folder %s\nFrom: %s\nTo: %s\nSubject: %s\nDate: %s"),
	        fold->name, head->from, head->to, head->subject, ctime(&head_date));
	write(afile,buff,strlen(buff));
	write(afile,text,head->size+1);
	Unlock_File(afile);
	close(afile);
}

void moderate(void)
{
	char fullpath[PATHSIZE],oldpath[PATHSIZE];
	struct folder fold;
	struct Header head;
	char *text;
	char answer[11];
		
	int f_file;
	int tmpindex,tmptext;
	
	if ((f_file=open(FOLDERFILE,O_RDWR))<0) 
		{perror(FOLDERFILE);exit(-1);}
	while (get_folder_entry(f_file,&fold))
	{
	sprintf(oldpath,"%s/%s%s%s",STATEDIR,fold.name,INDEX_END,MOD_END);
	if (!access(oldpath,00))
	{
		printf(_("Changing to folder %s\n"),fold.name);
		/* move the unmoderated messages to a temporary file */
		sprintf(fullpath,"%s/%s%s%s",STATEDIR,fold.name,INDEX_END,".tmp");
		rename(oldpath,fullpath);
		sprintf(oldpath,"%s/%s%s%s",STATEDIR,fold.name,TEXT_END,MOD_END);
		sprintf(fullpath,"%s/%s%s%s",STATEDIR,fold.name,TEXT_END,".tmp");
		rename(oldpath,fullpath);
		
		/* files containing messages to be approved */
		sprintf(fullpath,"%s/%s%s%s",STATEDIR,fold.name,INDEX_END,".tmp");
		if ((tmpindex=open(fullpath,O_RDONLY))<0) 
			{perror(fullpath);exit(-1);}
		sprintf(fullpath,"%s/%s%s%s",STATEDIR,fold.name,TEXT_END,".tmp");
		if ((tmptext=open(fullpath,O_RDONLY))<0)
			{perror(fullpath);exit(-1);}
		while (get_data(tmpindex,&head))
		{
			int foo;
			text=(char *)malloc(head.size);
			lseek(tmptext,head.datafield,0);
			if ((foo=read(tmptext,text,head.size))<head.size)
			{
				printf(_("only %d of %d bytes read.\n"),foo,head.size);
				return;
			}
			display_article(&head,tmptext);
			printf(_("[%s] (A)pprove, (L)eave, (S)ave&delete ? (<CR> to delete)"),fold.name);
			get_str(answer,10);
			if (stringcmp(answer,_("approve"),1))
			{
				Lock_File(f_file);
				lseek(f_file,-1*sizeof(struct folder),1);
				fold.last++;
				if (fold.first<=0) fold.first=1;
				write(f_file,&fold,sizeof(fold));
				add_message(&fold,&head,text);
				Unlock_File(f_file);
				printf(_("Message approved as mesg %d folder %s\n"),head.Ref,fold.name);
			}else
			if (stringcmp(answer,_("leave"),1))
			{
				put_back(&fold,&head,text);
				printf(_("Skipping message for now.\n"));
			}else
			if (stringcmp(answer,_("save"),1))
			{
				store(&fold,&head,text);
				printf(_("Saved and Deleted\n"));
			}else
				printf(_("Deleted.\n"));
			free(text);
		}
		printf(_("End of folder %s\n"),fold.name);
		close(tmpindex);
		close(tmptext);
		sprintf(fullpath,"%s/%s%s%s",STATEDIR,fold.name,INDEX_END,".tmp");
		if (unlink(fullpath)) {perror(fullpath);exit(-1);}
		sprintf(fullpath,"%s/%s%s%s",STATEDIR,fold.name,TEXT_END,".tmp");
		if (unlink(fullpath)) {perror(fullpath);exit(-1);}
	}
	}
	close(f_file);
}

