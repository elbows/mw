#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <termcap.h>
#include <sys/types.h>
#include <stdbool.h>

#include <sqlite.h>
#include "bb.h"
#include "talker_privs.h"
#include "special.h"
#include "talker.h"
#include <gags.h>
#include "str_util.h"
#include "strings.h"
#include "special.h"
#include "script_inst.h"
#include "js.h"
#include "sqlite.h"
#include "perms.h"
#include "main.h"
#include "init.h"
#include "echo.h"
#include "rooms.h"
#include "ipc.h"
#include "log.h"
#include "uri.h"
#include "util.h"
#include "intl.h"
#include "userio.h"
#include "user.h"
#include "who.h"

#include "alias.h"
extern Alias bind_list;
extern Alias alias_list;
extern Alias rpc_list;
extern Alias event_list;
extern Alias onoff_list;
extern Alias ipc_list;
extern Alias force_list;
extern Alias shutdown_list;

extern int busy; /* if true dont display messages  i.e. during new/write */
extern unsigned long rights;

extern struct person *user;
extern struct folder *fold;
extern int32_t userposn;
extern unsigned long loggedin; 
extern int quietmode;
static int runautoexec = 1;

extern struct room myroom;

/* ignore list for current user */
extern struct IgnoreList *ignored;

extern CommandList table[];
extern CommandList chattable[];

extern int g_boTermCap;

static void talk_send_to_room(const char * text, int channel, const char * type, int plural) {
	ipc_message_t * msg = ipcmsg_create(IPC_SAYTOROOM, userposn);
	ipcmsg_destination(msg, channel);
	json_t * j = json_init(NULL);
	json_addstring(j, "text", text);
	json_addstring(j, "type", type);
	if (plural > -1) json_addint(j, "plural", plural);
	ipcmsg_json_encode(msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
}

void talk_send_shout(char * text){
	mwlog("SHOUT %s", text);
	ipc_message_t * msg = ipcmsg_create(IPC_SAYTOALL, userposn);
	json_t * j = json_init(NULL);
	json_addstring(j, "text", text);
	ipcmsg_json_encode(msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
}

static void talk_send_say(char * text, int channel){
	mwlog("SAY %s", text);
	catchuri(text);
	talk_send_to_room(text, channel, "say", -1);
}

void talk_send_raw(char * text, int channel){
	mwlog("RAW %s", text);
	catchuri(text);
	talk_send_to_room(text, channel, "raw", -1);
}

void talk_send_rawbcast(char * text){
	mwlog("RAWBCAST %s", text);
	ipc_send_to_all(IPC_TEXT, text);
}

void talk_send_emote(char * text, int channel, int plural){
	catchuri(text);
	mwlog("EMOTE %s", text);
	talk_send_to_room(text, channel, "emote", plural);
}

void talk_sayto(char *text, const char *to, const char *type) 
{
	ipc_message_t * msg = ipcmsg_create(IPC_SAYTOUSER, userposn);
	json_t * j = json_init(NULL);
	json_addstring(j, "target", to);
	json_addstring(j, "type", type);
	json_addstring(j, "text", text);
	ipcmsg_json_encode(msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);

	mwlog("SAYTO %s %s",to,text);
}

int ison(const char * uname)
{
    /*
     * Ask the IPC mechanism if a user is there
     */
    return (ipc_send_to_username(uname, IPC_NOOP, NULL) > 0);
}

int screen_h(void)
{
	char	*szLines;
	int	nLines = -1;

	/* we have terminal information */
	if (g_boTermCap == 1)
	{
		char li[] = "li";
		/* check if line count exists */
		nLines = tgetnum(li);
	}

	/* not using termcap, or lines not found */
	if (nLines == -1)
	{
		/* fall back on trying to read the number of lines from the environment */
		szLines = getenv("LINES");
		/* if the number of lines could not be found, default to 24 */
		if (szLines == NULL) nLines = 24;
		/* otherwise get the number of lines */
		else nLines = atoi(szLines);
	}

	/* return the line count */
	return (nLines);
}

int screen_w(void)
{
	char	*szCols;
	int	nCols = -1;

	/* we have terminal information */
	if (g_boTermCap == 1)
	{
		char co[] = "co";
		/* check if column count exists */
		nCols = tgetnum(co);
	}

	/* not using termcap, or columns not found */
	if (nCols == -1)
	{
		/* fall back on trying to read the number of columns from the environment */
		szCols = getenv("COLUMNS");
		/* if the number of columns could not be found, default to 80 */
		if (szCols == NULL) nCols = 80;
		/* otherwise get the number of columns */
		else nCols = atoi(szCols);
	}

	/* return the column count */
	return (nCols);
}

void t_who(CommandList *cm, int argc, const char **argv, char *args)
{
	who_list(0);
}

void t_what(CommandList *cm, int argc, const const char **argv, char *args)
{
	who_list(1);
}

void t_uptime(CommandList *cm, int argc, const const char **argv, char *args)
{
	ipc_message_t * msg = ipcmsg_create(IPC_UPTIME, userposn);
	ipcmsg_transmit(msg);
}

void t_quit(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];
	user->chatmode=cm_flags(user->chatmode, CM_ONCHAT, CM_MODE_CLEAR);
	update_user(user,userposn);
	snprintf(text,MAXTEXTLENGTH-1,"\03311%s has just left the talker",user->name);
	if (!quietmode) talk_send_rawbcast(text);

	/* announce log off talker */
	broadcast_onoffcode(0, 0, NULL, NULL);

	set_rights();
}

void t_script(CommandList *cm, int argc, const char **argv, char *args)
{
	if (argc<2) ListScript(NULL); else ListScript(argv[1]);
}

void t_showvars(CommandList *cm, int argc, const char **argv, char *args)
{
	if (argc<2) ListVars(NULL); else ListVars(argv[1]);
}

void t_event(CommandList *cm, int argc, const char **argv, char *args)
{
	/* no arguments */
	if (argc < 2)  
	{
		char	*event_text = NULL;
		int	event_count = 0;   

		/* show list of events */
		while((event_text = NextLink(event_list, event_text)) != NULL)
		{
			event_count++;
			if (event_count == 1) printf("List of current event functions:\n");
			printf("  - %s\n", event_text);
		}
		if (!event_count) printf("No text events set\n");
		return;
	}

	if (AddLink(&event_list, argv[1], ""))
		printf("Event bind '%s' already exists!\n", argv[1]);
	else
		printf("Event '%s' added!\n", argv[1]);
}
 
void t_unevent(CommandList *cm, int argc, const char **argv, char *args)
{
	if (!strcasecmp("*", argv[1]))
	{
		DestroyAllLinks(&event_list);
		printf("All Events Destroyed!\n");
	}
	else
	{   
		if (!DestroyLink(&event_list, argv[1]))
			printf("Event '%s' was not found!\n", argv[1]);
		else
			printf("Event '%s' was destroyed...\n", argv[1]);
	}
}

void t_runaway(CommandList *cm, int argc, const char **argv, char *args)
{
	extern unsigned long run_away;
	unsigned long num = 0;

	if (isanumul(argv[1], &num, 1))
	{
		if (cp_flags(user->chatprivs, CP_ADVSCR, CM_MODE_ALL))
		{
			if (!u_god(user->status))
			{
				if (num < 1)
				{
					printf("Invalid Runaway. Must be greater than 0\n");
					return;
				}
			}
			else
			{
				if (num < 0)
				{
					printf("Invalid Runaway. Must be positive (0 disables)\n");
					return;
				}
			}
			run_away = num;
			if (run_away == 0) printf("Runaway disabled\n");
			else printf("Runaway set to value: %lu\n", run_away);
		}
		else
		{
			if ((num < 1) || (num > MAXRUNAWAY))
			{
				printf("Invalid Runaway. Must be between 1 and %d\n", MAXRUNAWAY);
				return;
			}
			run_away = num;
			printf("Runaway set to value: %ld\n", run_away);
		}
	}
	else
	{
		if (cp_flags(user->chatprivs, CP_ADVSCR, CM_MODE_ALL))
			printf("Invalid Runaway. A number greater than 0 must be specified\n");
		else
			printf("Invalid Runaway. A number between 1 and %d must be specified\n", MAXRUNAWAY);
	}
}

void t_flood(CommandList *cm, int argc, const char **argv, char *args)
{
	extern int flood_limit;
	int num = 0;

	if (isanum(argv[1], &num, 1))
	{
		flood_limit = num;
		printf("Flood limit set to value: %d\n", flood_limit);
	}
}

void t_load(CommandList *cm, int argc, const char **argv, char *args)
{
	if (argc<2)
	{
		printf("Specify the file name.\n");
		return;
	}

	LoadInitFile(argv[1]);

	/* run board init functions */
	RunInitFuncs(0);
	/* run talker init functions */
	RunInitFuncs(1);
}

void t_destroy(CommandList *cm, int argc, const char **argv, char *args)
{
	if (argc<2)
	{
		printf("Specify the function to destroy.\n");
		return;
	}
	
	if (!strcmp(argv[1],"*")) DestroyAllFunctions(1); else DestroyFunction(argv[1]);
}

void t_mwrc(CommandList *cm, int argc, const char **argv, char *args)
{
	if (argc < 2) {
	        char * old = userdb_get(USERDB_PRIVATE, user->name, "mwrc");
		if (old==NULL) {
			printf(_("Current mwrc path: %s\n"), _("<unset>"));
		} else {
			printf(_("Current mwrc path: %s\n"), old);
			free(old);
		}
	} else {
		userdb_set(USERDB_PRIVATE, user->name, "mwrc", argv[1]);
	        char * old = userdb_get(USERDB_PRIVATE, user->name, "mwrc");
		printf(_("Setting mwrc path to: %s\n"), old==NULL?"<unset>":old);
		free(old);
	}
}

void t_restart(CommandList *cm, int argc, const char **argv, char *args)
{
	extern var_list_t	var_list;

	DestroyAllFunctions(0);
	DestroyVariables(0);
	VAR_NEWLIST(&var_list);

	DestroyAllLinks(&bind_list); bind_list = NULL;
	DestroyAllLinks(&alias_list); alias_list = NULL;
	DestroyAllLinks(&rpc_list); rpc_list = NULL;
	DestroyAllLinks(&event_list); event_list = NULL;
	DestroyAllLinks(&onoff_list); onoff_list = NULL;
	DestroyAllLinks(&ipc_list); ipc_list = NULL;
	DestroyAllLinks(&force_list); force_list = NULL;
	DestroyAllLinks(&shutdown_list); shutdown_list = NULL;
	
	stop_js();
	setup_js();
	
	RoomDestroy(&myroom);

	RoomInit(&myroom);
	LoadRoom(&myroom, user->room);

	LoadInitFile(NULL);

	/* run board init functions */
	RunInitFuncs(0);
	/* run talker init functions */
	RunInitFuncs(1);
}

void t_shout(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	text[0]=0;
	strncat(text, args, MAXTEXTLENGTH - strlen(text) - 1);
	talk_send_shout(text);
}

void t_whisper(CommandList *cm, int argc, const char **argv, char *args)
{
	talk_sayto(args, argv[1], "whispers");
}


void t_sayto(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	text[0]=0;
	strncat(text, args, MAXTEXTLENGTH - strlen(text) - 1);

	const char *type = "says";

	if (text[strlen(text)-1] == '?') type = "asks";
	talk_sayto(text, argv[1], type);
}

void t_sayto_warn(CommandList *cm, int argc, const char **argv, char *args)
{
	display_message("*** .say as an alias for .sayto is deprecated.  Please use .sa instead.", 1, 1);
	t_sayto(cm, argc, argv, args);
}

void t_notsayto(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	// check that the excluded person is on
	if (!ison(argv[1])) {
	    printf(_("User '%s' is not logged on.\n"), argv[1]);
	    return;
	}
	snprintf(text, MAXTEXTLENGTH, "%s", args);
	mwlog("NSAYTO %s %s", argv[1], text);

	/* variant of say_to_room but with an exclude clause */
	ipc_message_t * msg = ipcmsg_create(IPC_SAYTOROOM, userposn);
	ipcmsg_destination(msg, user->room);
	json_t * j = json_init(NULL);
	json_addstring(j, "text", text);
	json_addstring(j, "type", "notsayto");
	json_addstring(j, "exclude", argv[1]);
	ipcmsg_json_encode(msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
}

void t_ventril(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];
	int i;

	text[0]=0;
	strncat(text, args, MAXTEXTLENGTH - strlen(text) - 1);

	i = 0;
	if (isanum(argv[1], &i, 1))
	{
		if (i<0 || i>65535)
		{
			display_message("Invalid room number. Must be between 0 and 65535", 1, 1);
		}else
		{
			talk_send_say(text,i);
		}
	}
	else
	{
		display_message("Invalid room number. Must be a number between 0 and 65535", 1, 1);
	}
}

void t_ventril_raw(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];
	int i;

	text[0]=0;
	strncat(text, args, MAXTEXTLENGTH - strlen(text) - 1);

	i = 0;
	if (isanum(argv[1], &i, 1))
	{
		if (i<0 || i>65535)
		{
			display_message("Invalid room number. Must be between 0 and 65535", 1, 1);
		}else
		{
			talk_send_raw(text,i);
		}
	}
	else
	{
		display_message("Invalid room number. Must be a number between 0 and 65535", 1, 1);
	}
}

void t_emote(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	text[0] = 0;
	strncat(text, args, MAXTEXTLENGTH - strlen(text) - 1);

	talk_send_emote(text,user->room,0);	
}

void t_emotes(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	text[0] = 0;
	strncat(text, args, MAXTEXTLENGTH - strlen(text) - 1);

	talk_send_emote(text,user->room,1);	
}

void t_emotes2(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	text[0] = 0;
	strncat(text, args, MAXTEXTLENGTH - strlen(text) - 1);

	talk_send_emote(text,user->room,2);	
}

void t_emoted(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	text[0] = 0;
	strncat(text, args, MAXTEXTLENGTH - strlen(text) - 1);

	talk_send_emote(text,user->room,3);	
}

void t_emotell(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	text[0] = 0;
	strncat(text, args, MAXTEXTLENGTH - strlen(text) - 1);

	talk_send_emote(text,user->room,4);	
}

void t_global(CommandList *cm, int argc, const char **argv, char *args)
{
	int z=BoolOpt(argv[1]);
	unsigned long global=cm_flags(user->chatmode,CM_GLOBAL,CM_MODE_ALL);

	if (z==1)
	{
		if (!global)
		{
			user->chatmode=cm_flags(user->chatmode,CM_GLOBAL,CM_MODE_SET);
			display_message("Global receive mode Enabled.", 1, 1);
		}else
			display_message("Already in Global receive mode.", 1, 1);
	}else
	if (z==0)
	{
		if (global)
		{
			user->chatmode=cm_flags(user->chatmode,CM_GLOBAL,CM_MODE_CLEAR);
			display_message("Global receive mode Disabled.", 1, 1);
		}else
			display_message("You are not in Global receive mode.", 1, 1);
	}else
		printf("%s\n",cm->ArgError);
	update_user(user,userposn);
}

void t_sticky(CommandList *cm, int argc, const char **argv, char *args)
{
	int z=BoolOpt(argv[1]);
	unsigned long sticky=cm_flags(user->chatmode,CM_STICKY,CM_MODE_ALL);

	if (z==1)
	{
		if (!sticky)
		{
			user->chatmode=cm_flags(user->chatmode,CM_STICKY,CM_MODE_SET);
			display_message("Sticky room mode Enabled.", 1, 1);
		}else
			display_message("Already in Sticky room mode.", 1, 1);
	}else
	if (z==0)
	{
		if (sticky)
		{
			user->chatmode=cm_flags(user->chatmode,CM_STICKY,CM_MODE_CLEAR);
			display_message("Sticky room mode Disabled.", 1, 1);
		}else
			display_message("You are not in Sticky room mode.", 1, 1);
	}else
		printf("%s\n",cm->ArgError);
	update_user(user,userposn);
}

void t_tcunames(CommandList *cm, int argc, const char **argv, char *args)
{
	int z=BoolOpt(argv[1]);
	if (z==0)
	{
		if (s_tcunames(user->special))
		{
			user->special&=~(1<<12);
			printf("Username Tab-Completion now disabled.\n");
		}else
			printf("Username Tab-Completion was already off.\n");
	}else
	if (z==1)
	{
		if (!s_tcunames(user->special))
		{
			user->special|=(1<<12);
			printf("Username Tab-Completion now enabled.\n");
		}else
			printf("Username Tab-Completion already enabled.\n");
	}else
		printf("%s\n",cm->ArgError);
	update_user(user,userposn);
}

/* SCRIPT HELP function */
void t_scrhelp(CommandList *cm, int argc, const char **argv, char *args)
{
	if (argc>1)
		scr_helpfile(argv[1]);
	else
		scr_helplist();
}

void t_debug(CommandList *cm, int argc, const char **argv, char *args)
{
	int z=BoolOpt(argv[1]);
	extern int script_debug;

	if (z==1)
	{
		if (!script_debug)
		{
			script_debug=1;
			display_message("Script debug mode Enabled.", 1, 1);
		}else
			display_message("Already in script debug mode.", 1, 1);
	}else
	if (z==0)
	{
		if (script_debug)
		{
			script_debug=0;
			display_message("Script debug mode Disabled.", 1, 1);
		}else
			display_message("You are not in script debug mode.", 1, 1);
	}else
		printf("%s\n",cm->ArgError);
	update_user(user,userposn);
}

void t_spy(CommandList *cm, int argc, const char **argv, char *args)
{
	int z=BoolOpt(argv[1]);
	unsigned long global=cm_flags(user->chatmode,CM_SPY,CM_MODE_ALL);

	if (z==1)
	{
		if (!global)
		{
			user->chatmode=cm_flags(user->chatmode,CM_SPY,CM_MODE_SET);
			display_message("Spy mode Enabled.", 1, 1);
		}else
			display_message("Already in Spy mode.", 1, 1);
	}else
	if (z==0)
	{
		if (global)
		{
			user->chatmode=cm_flags(user->chatmode,CM_SPY,CM_MODE_CLEAR);
			display_message("Spy mode Disabled.", 1, 1);
		}
		else
			display_message("You are not in Spy mode.", 1, 1);
	}else
		printf("%s\n",cm->ArgError);
	update_user(user,userposn);
}

void t_sproof(CommandList *cm, int argc, const char **argv, char *args)
{
	if(argc == 1) {
		char * sproof = db_room_get(user->room, "soundproof");
		if (sproof == NULL || sproof[0]!='1')
			display_message("This room is not soundproof.", 0, 1);
		else
			display_message("This room is soundproof.", 0, 1);
		if (sproof != NULL) free(sproof);

		return;
	}

	int z=BoolOpt(argv[1]);
	char * was = db_room_get(user->room, "soundproof");
	if ( z == 1) {
		if (was !=NULL && was[0] == '1') {
			display_message("This room is already soundproof.", 0, 1);
		} else {
			db_room_set(user->room, "soundproof", "1");
			char text[MAXTEXTLENGTH];
			snprintf(text,MAXTEXTLENGTH-1,"\03315%s has just soundproofed this room.",user->name);
			talk_send_raw(text,user->room);
		}
	} else
	if (z == 0) {
		if (was == NULL || was[0]!='1') {
			display_message("This room was not soundproof.", 0, 1);
		} else {
			db_room_set(user->room, "soundproof", "0");
			char text[MAXTEXTLENGTH];
			snprintf(text,MAXTEXTLENGTH-1,"\03315%s has just removed the soundproofing for this room.",user->name);
			talk_send_raw(text,user->room);
		}
	}
	free(was);
	//JSM
}


void t_protect(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];
	int myapl = (user->chatprivs & CP_PROTMASK) >> CP_PROTSHIFT;
	int level = -1;

	/* If the sender has an authorised protection level of 0, they can't
	 * give a 2nd argument (and shouldn't be told it's possible).
	 */
	if (argc < 2 || argc > 3 || (myapl<=0 && argc>2))
	{
	    if (myapl > 0)
	    {
		display_message("Usage: protect <user> [<level>]", 1, 1);
	    } else
	    {
		display_message("Usage: protect <user>", 1, 1);
	    }
	    return;
	}

	/* if protection priv level has been granted to you, then check if we
	 * can append the third protection level argument.
	 */
	if (myapl > 0 && argc >= 3)
	{
		/* if protection level is not a valid number, or it is outside
		 * you priv range, then you cannot grant that level of
		 * protection.
		 */
		if (sscanf(argv[2],"%d", &level) != 1 || level < 0 || level > myapl)
		{
			sprintf(text, "Invalid protection level (available: 0-%d)", myapl);
			display_message(text, 1, 1);
			return;
		}
	}

	/* Note: level may be -1, in which case only temporary protection was
	 * granted (This is now handled by the receiver).
	 */
	snprintf(text, 40, "%d", level);
	ipc_send_to_username(argv[1], IPC_PROTLEVEL, text);

	/* We have given them a protection level of any kind, so give them the
	 * standard protection priv for compatibility.  Even if they are
	 * running an old version (as long as the protect/unprotect is
	 * successful), they won't notice this, because the IPC_PROTLEVEL
	 * will have set the 'p' chatmode flag to the same value.
	 */
	if (level != 0)
	{
		/* give them the old temporary protection as well/instead */
		snprintf(text,40,"+p");
		ipc_send_to_username(argv[1], IPC_CHATMODE, text);
		snprintf(text,MAXTEXTLENGTH-1,"\03315%s has just tried to protect %s",user->name,argv[1]);
	}
	/* protect level is zero (wtf?!?), so handle unprotection (erm.. unprotect anyone?) */
	else
	{
		snprintf(text,40,"-p");
		ipc_send_to_username(argv[1], IPC_CHATMODE, text);
		snprintf(text,MAXTEXTLENGTH-1,"\03315%s has just tried to unprotect %s",user->name,argv[1]);
	}
	talk_send_raw(text,user->room);
}

void t_unprotect(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	snprintf(text,40,"-p");
	ipc_send_to_username(argv[1], IPC_CHATMODE, text);
	snprintf(text,40,"0");
	ipc_send_to_username(argv[1], IPC_PROTLEVEL, text);

	snprintf(text,MAXTEXTLENGTH-1,"\03315%s has just tried to unprotect %s",user->name,argv[1]);
	talk_send_raw(text,user->room);
}

void t_summon(CommandList *cm, int argc, const char **argv, char *args)
{
	char		text[MAXTEXTLENGTH];
	int		newroom;

	if (sscanf(argv[1],"%d",&newroom)!=1)
	{
		display_message("Invalid Room ID (0-65535 only)", 1, 1);
		return;
	}

	if (newroom<0 || newroom>65535)
	{
		display_message("Invalid Room ID (0-65535 only)", 1, 1);
		return;
	}

	for (int i = 2; i < argc; i++)
	{
		int j;
		/* go through all username arguments in front of this one */
		for (j = 2; j < i; j++)
		{
			/* if any usernames match, then break out of the loop */
			if (!strcasecmp(argv[i], argv[j])) break;
		}
		/* if a username match was found, skip this argument */
		if (j != i) continue;

		snprintf(text,MAXTEXTLENGTH-1,"\03315%s has just tried to summon %s to room %d",user->name,argv[i],newroom);
		talk_send_raw(text,user->room);
		snprintf(text,MAXTEXTLENGTH-1,"-%05d",newroom);
		ipc_send_to_username(argv[i], IPC_CHANNEL, text);
	}
}

void t_freeze(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	snprintf(text,40,"+f");
	ipc_send_to_username(argv[1], IPC_CHATMODE, text);
	snprintf(text,MAXTEXTLENGTH-1,"\03315%s has just tried to freeze %s",user->name,argv[1]);
	talk_send_raw(text,user->room);
}

void t_unfreeze(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	snprintf(text,40,"-f");
	ipc_send_to_username(argv[1], IPC_CHATMODE, text);
	snprintf(text,MAXTEXTLENGTH-1,"\03315%s has just tried to unfreeze %s",user->name,argv[1]);
	talk_send_raw(text,user->room);
}

void t_gag(CommandList *cm, int argc, const char **argv, char *args)
{
	ipc_message_t * msg = ipcmsg_create(IPC_ACTION, userposn);
	json_t * j = json_init(NULL);
	json_addstring(j, "target", argv[1]);
	json_addstring(j, "type", "gag");
	json_addstring(j, "gag", argv[2]);
	ipcmsg_json_encode(msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
}

void t_ungag(CommandList *cm, int argc, const char **argv, char *args)
{
	ipc_message_t * msg = ipcmsg_create(IPC_ACTION, userposn);
	json_t * j = json_init(NULL);
	json_addstring(j, "target", argv[1]);
	json_addstring(j, "type", "ungag");
	ipcmsg_json_encode(msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
}

void t_ignorelist(CommandList *cm, int argc, const char **argv, char *args)
{
	int linecount = 1;
	int screen_height = screen_h();
	struct IgnoreList *tail;
 
	tail = ignored;
	if (tail==NULL)
	{
		display_message("You are NOT currently ignoring any users", 0, 1);
	}
	else
	{
		display_message("You are currently ignoring:", 0, 1);

		while (tail!=NULL)
		{
			linecount++;
			printf("%s\n", tail->name);
			if (linecount>=(screen_height-1))
			{
				char foo[6];
				printf("---more---\r");
				echo_off();
				get_str(foo,5);
				echo_on();
				if (foo[0]=='q'|| foo[0]=='Q') break;
				printf("            \r");
				linecount=0;
			}
			tail = tail->next;
		}
	}
}
 
void t_ignore(CommandList *cm, int argc, const char **argv, char *args)
{
	char			text[MAXTEXTLENGTH];
	struct IgnoreList	*newnode;
	int32_t			tempposn;
	struct person		tempusr;
	char			*namebuff[MAX_ARGC];
	int			namecount;
	int			argloop;

	if (strcmp(argv[1], "*") == 0)
	{
		int ufile,wfile;
		struct person p;
		struct who w;

		wfile=who_open(O_RDWR);   
		ufile=userdb_open(O_RDONLY);
		if (wfile<0 || ufile<0) return; /* whoops */
			
		while (read(wfile,&w,sizeof(w)))
		{
			/* Skip invalid entries */
			if (w.posn < 0)
				continue;

			lseek(ufile,w.posn,0);
			read(ufile,&p,sizeof(p));

				if (cm_flags(p.chatmode,CM_ONCHAT,CM_MODE_ANY))
				{
					if (is_ignored(p.name, NULL))
					{
						printf("*** User '%s' already ignored ***\n", p.name);
					}
					else
					{
						newnode = malloc(sizeof(struct IgnoreList));
						newnode->name = malloc(sizeof(char) * (strlen(p.name) + 1));
						strcpy(newnode->name, p.name);
						newnode->next = ignored;
						ignored = newnode;
					}
				}
		}
		close(ufile);
		close(wfile);

		snprintf(text,MAXTEXTLENGTH-1,"\03315%s has just ignored everyone currently logged on", user->name);
		talk_send_raw(text,user->room);
		snprintf(text,MAXTEXTLENGTH-1,"*** You have now ignored everyone currently logged on ***");
		display_message(text, 1, 1);

		return;
	}

	namecount = 0;
	for (argloop = 1; argloop < argc; argloop++)
	{
		if (!is_old(&tempusr,argv[argloop], &tempposn))
		{
			snprintf(text,MAXTEXTLENGTH-1,"*** User '%s' does not exist ***", argv[argloop]);
			display_message(text, 1, 1);
		}
		else if (!ison(argv[argloop])) {
			snprintf(text,MAXTEXTLENGTH-1,"*** User '%s' is not currently logged on ***", tempusr.name);
			display_message(text, 1, 1);
		}
		else if (is_ignored(argv[argloop], NULL))
		{
			snprintf(text,MAXTEXTLENGTH-1,"*** User '%s' is already being ignored ***", tempusr.name);
			display_message(text, 1, 1);
		}
		else
		{   
			newnode = malloc(sizeof(struct IgnoreList));
			newnode->name = malloc(sizeof(char) * (strlen(tempusr.name) + 1));
			strcpy(newnode->name, tempusr.name);
			newnode->next = ignored;
			ignored = newnode;
			namebuff[namecount] = malloc(sizeof(char) * (strlen(tempusr.name) + 1));
			strcpy(namebuff[namecount], tempusr.name);
			namecount++;
			snprintf(text,MAXTEXTLENGTH-1,"*** You are now ignoring user '%s' ***", tempusr.name);
			display_message(text, 1, 1);
		}
	}

	if (namecount == 0) return;

	snprintf(text,MAXTEXTLENGTH-1,"\03315%s has just ignored ", user->name);
	for (argloop = 0; argloop < namecount; argloop++)
	{
		strcat(text, namebuff[argloop]);
		if (argloop == namecount - 2) strcat(text, " and ");
		else if (argloop != namecount - 1) strcat(text, ", ");
		free(namebuff[argloop]);
	}
	talk_send_raw(text,user->room);
}
 
void t_unignore(CommandList *cm, int argc, const char **argv, char *args)
{
	char    text[MAXTEXTLENGTH];
	const char *namebuff[MAX_ARGC];
	int	namecount;
	int	argloop;

	if (strcmp(argv[1], "*") == 0)
	{
		struct IgnoreList *temp;

		/* unignore everyone in list */

		while (ignored != NULL)
		{
			temp = ignored;
			ignored = ignored->next;
			free(temp->name);
			free(temp);
		}

		snprintf(text,MAXTEXTLENGTH-1,"\03315%s has just un-ignored everyone", user->name);
		talk_send_raw(text,user->room);
		snprintf(text,MAXTEXTLENGTH-1,"*** You have unignored everyone ***");
		display_message(text, 1, 1);

		return;
	}

	namecount = 0;
	for (argloop = 1; argloop < argc; argloop++)
	{
		struct IgnoreList *found, *prev = NULL;
		found = is_ignored(argv[argloop], &prev);
		if (found)
		{
			/* if at front */
			if (prev == NULL) ignored = ignored->next;
			/* in middle/end */
			else prev->next = found->next;
			free(found->name);
			free(found);

			/* argv is here for the duration so we can point into it */
			namebuff[namecount] = argv[argloop];
			namecount++;
			snprintf(text,MAXTEXTLENGTH-1,"*** You have just unignored user '%s' ***", argv[argloop]);
			display_message(text, 1, 1);
		}
		else
		{   
			snprintf(text,MAXTEXTLENGTH-1,"*** User '%s' is not ignored ***", argv[argloop]);
			display_message(text, 1, 1);
		}
	}

	if (namecount == 0) return;

	snprintf(text,MAXTEXTLENGTH-1,"\03315%s has just un-ignored ", user->name);
	for (argloop = 0; argloop < namecount; argloop++)
	{
		strcat(text, namebuff[argloop]);
		if (argloop == namecount - 2)
			strcat(text, " and ");
		else if (argloop != namecount - 1)
			strcat(text, ", ");
	}
	talk_send_raw(text, user->room);
}

void t_zod(CommandList *cm, int argc, const char **argv, char *args)
{
	AUTOFREE_BUFFER excuse = remove_first_word(args);
	ipc_message_t * msg = ipcmsg_create(IPC_ACTION, userposn);
	json_t * j = json_init(NULL);
	json_addstring(j, "target", argv[1]);
	json_addstring(j, "type", "zod");
	if (excuse!=NULL && !allspace(excuse))
		json_addstring(j, "reason", excuse);
	ipcmsg_json_encode(msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
}

void t_mrod(CommandList *cm, int argc, const char **argv, char *args)
{
	AUTOFREE_BUFFER excuse = remove_first_word(args);
	ipc_message_t * msg = ipcmsg_create(IPC_ACTION, userposn);
	json_t * j = json_init(NULL);
	json_addstring(j, "target", argv[1]);
	json_addstring(j, "type", "mrod");
	if (excuse!=NULL && !allspace(excuse))
		json_addstring(j, "reason", excuse);
	ipcmsg_json_encode(msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
}

void t_kick(CommandList *cm, int argc, const char **argv, char *args)
{
	AUTOFREE_BUFFER excuse = remove_first_word(args);
	ipc_message_t * msg = ipcmsg_create(IPC_ACTION, userposn);
	json_t * j = json_init(NULL);
	json_addstring(j, "target", argv[1]);
	json_addstring(j, "type", "zod");
	json_addstring(j, "admin", "yes");
	if (excuse!=NULL && !allspace(excuse))
		json_addstring(j, "reason", excuse);
	ipcmsg_json_encode(msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
}

void t_remove(CommandList *cm, int argc, const char **argv, char *args)
{
	AUTOFREE_BUFFER excuse = remove_first_word(args);
	ipc_message_t * msg = ipcmsg_create(IPC_ACTION, userposn);
	json_t * j = json_init(NULL);
	json_addstring(j, "target", argv[1]);
	json_addstring(j, "type", "mrodod");
	json_addstring(j, "admin", "yes");
	if (excuse!=NULL && !allspace(excuse))
		json_addstring(j, "reason", excuse);
	ipcmsg_json_encode(msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
}

void t_raw(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	text[0]=0;
	strncat(text, args, MAXTEXTLENGTH - strlen(text) - 1);
	talk_send_raw(text,user->room);
}

void t_bind(CommandList *cm, int argc, const char **argv, char *args)
{
	if (AddLink(&bind_list, argv[1], argv[2]))
		printf("Bind '%s' already exists. Has now been redefined!\n", argv[1]);
	else
		printf("Bind '%s' added!\n", argv[1]);
}

void t_unbind(CommandList *cm, int argc, const char **argv, char *args)
{
	if (!strcasecmp("*", argv[1]))
	{
		DestroyAllLinks(&bind_list);
		printf("All Binds Destroyed!\n");
	}
	else
	{
		if (!DestroyLink(&bind_list, argv[1]))
			printf("Bind '%s' was not found!\n", argv[1]);
		else
			printf("Bind '%s' was destroyed...\n", argv[1]);
	}
}

void t_room(CommandList *cm, int argc, const char **argv, char *args)
{
	switch(ChangeRoom(args, 0))
	{
	case 0:
		/* success */
		break;
	case 1:
		display_message("You cannot find that room", 1, 1);
		break;
	case 2:
		display_message("Sorry. You're frozen and can't change rooms.", 1, 1);
		break;
	case 3:
		display_message("Sorry, you cannot go in that direction.\n", 1, 1);
		break;
	case 4:
		display_message("Invalid Room ID (0-65535 only)", 1, 1);
		break;
	}	
}

void t_linewrap(CommandList *cm, int argc, const char **argv, char *args)
{
	int z=BoolOpt(argv[1]);
	if (z==0)
	{
		if (!s_nolinewrap(user->special))
		{
			user->special |= (1<<15);
			printf("Linewrapping now disabled.\n");
		}
		else
			printf("Linewrapping was already disabled.\n");
	}
	else if (z==1)
	{
		if (s_nolinewrap(user->special))
		{
			user->special &= ~(1<<15);
			printf("Linewrapping now enabled.\n");
		}
		else
			printf("Linewrapping already enabled.\n");
	}else
		printf("%s\n",cm->ArgError);
	update_user(user,userposn);
}

void t_topic(CommandList *cm, int argc, const char **argv, char *args)
{
	AUTOFREE_BUFFER buff=NULL;

	if (argc == 1) {
		char *topic;

		topic = db_room_get(user->room, "topic");
		if (topic != NULL && *topic != 0) {
			asprintf(&buff, "Topic: %s\n", topic);
		} else {
			asprintf(&buff, "No Topic is set for channel %d.\n", user->room);
		}
		display_message(buff, 0, 1);
		if (topic != NULL) free(topic);
		return;
	} else
	if (u_god(user->status) || s_wizchat(user->special) || cp_flags(user->chatprivs,CP_TOPIC,CM_MODE_ANY)) {
		char text[MAXTEXTLENGTH];
		const char *topic;

		if (argc == 2 && argv[1][0] == 0) args=NULL;

		db_room_set(user->room, "topic", args);
		topic = db_room_get(user->room, "topic");
		if (topic == NULL || *topic == 0) {
			snprintf(text,MAXTEXTLENGTH-1,"\03315%s has cleared the channel topic.",user->name);
			mwlog("TOPIC <empty>");
		} else {
			snprintf(text,MAXTEXTLENGTH-1,"\03315%s has set the topic to \"%s\".",user->name, topic);
			mwlog("TOPIC %s", topic);
		}
		talk_send_raw(text,user->room);
	} else {
		display_message("Sorry, only admins can change the topic.", 0, 1);
	}
}

void t_uri(CommandList *cm, int argc, const char **argv, char *args)
{
	uriActionList *al = uritable;
	int wiz=0, num;
	char c;
	
	if(argc == 1) {
		// default action - list last 10 uris
		uri_list_display(10, NULL);
		return;
	}
	
	if (u_god(user->status))  {
		wiz=1;
	}
		
	while(al->action)
	{
		if( strcasecmp(al->action, argv[1]) == 0 // we have a valid action
		&& argc-1 >= al->min_argc // it has enough args
		&& argc-1 <= al->max_argc // and doesn't have too many args
		&& ( !al->needs_wiz || (al->needs_wiz && wiz)) ) // and it either doesn't need wiz or we are wiz
		{
			al->function(argc -1, &argv[1], al->needs_wiz && wiz);
			return;
		}
		al++;
	}
	
	if(argc == 2) // check for the posibility we have ".uri n" (same as .uri list * n)
	{
		if(sscanf(argv[1], "%d%c", &num, &c) == 1) { // check the arg is an int
			if(num > 0) { 
				uri_list_display(num, NULL);
				return;
			}
		}
	}
		
	printf("%s\n", cm->ArgError);
}

void t_chaton(void)
{
	char text[MAXTEXTLENGTH];
	char mtext[MAXTEXTLENGTH];

	set_talk_rights();

	/* set rights to 'on talker', and update whofile for autoexec function */
	user->chatmode=cm_flags(user->chatmode,CM_ONCHAT,CM_MODE_SET);
	update_user(user,userposn);

	if (myroom.name!=NULL)
		snprintf(mtext,MAXTEXTLENGTH-1,"\03310%s has just joined talker in %s",user->name, myroom.name);
	else
		snprintf(mtext,MAXTEXTLENGTH-1,"\03310%s has just joined talker room %d",user->name, user->room);
	snprintf(text,MAXTEXTLENGTH-1,"\03310%s has just joined talker room %d",user->name, user->room);
	if (!quietmode) talk_send_rawbcast(text);
	printf("\nNow entering the Talker. Use \".quit\" to leave and \".help\" for help.\n");

	/* run the common stuff for entering the talker */
	enter_talker(1);

	/* give an entrance broadcast */
	broadcast_onoffcode(1, 0, NULL, NULL);

	enter_room(user->room);
}

void enter_room(int room)
{
	char *topic = db_room_get(room, "topic");
	if (topic != NULL) {
		if (*topic != 0) {
			AUTOFREE_BUFFER buff=NULL;
			asprintf(&buff, "Topic: %s\n", topic);
			display_message(buff, 0, 1);
		}
		free(topic);
	}
	char * sproof = db_room_get(room, "soundproof");
	if (sproof != NULL) {
		if (strcasecmp(sproof, "1")==0) {
			display_message("This room is soundproofed.\n", 0, 1);
		}
		free(sproof);
	}
}

/**** actual talker utilities *****/

void chat_say(char *text)
{
	talk_send_say(text,user->room);
}

void set_talk_rights(void)
{
	extern int current_rights;

	rights=0;
	if (cp_flags(user->chatprivs,CP_CANRAW,CM_MODE_ANY)) rights|=0x0001;
	if (cp_flags(user->chatprivs,CP_CANGAG,CM_MODE_ANY)) rights|=0x0002;
	if (cp_flags(user->chatprivs,CP_CANZOD,CM_MODE_ANY)) rights|=0x0004;
	if (cp_flags(user->chatprivs,CP_CANMROD,CM_MODE_ANY)) rights|=0x0008;
	if (cp_flags(user->chatprivs,CP_CANGLOBAL,CM_MODE_ANY)) rights|=0x0010;
	if (cp_flags(user->chatprivs,CP_PROTECT,CM_MODE_ANY)) rights|=0x0020;
	if (cp_flags(user->chatprivs,CP_FREEZE,CM_MODE_ANY)) rights|=0x0040;
	if (cp_flags(user->chatprivs,CP_SUMMON,CM_MODE_ANY)) rights|=0x0080;
	if (cp_flags(user->chatprivs,CP_SPY,CM_MODE_ANY)) rights|=0x0100;
	if (cp_flags(user->chatprivs,CP_SCRIPT,CM_MODE_ANY)) rights|=0x0200;
	if (u_god(user->status)) rights|=0x0800;
	if (cp_flags(user->chatprivs,CP_ADVSCR,CM_MODE_ANY)) rights|=0x1000;
	if (cp_flags(user->chatprivs,CP_DEVEL,CM_MODE_ANY)) rights|=0x2000;

	current_rights = RIGHTS_TALK;
}	

struct IgnoreList *is_ignored(const char *name, struct IgnoreList **prev)
{
	struct IgnoreList *tail;

	if (prev != NULL) *prev = NULL;

	tail = ignored;
	while (tail != NULL)
	{
		if (!strcasecmp(tail->name, name)) return tail;
		if (prev != NULL) *prev = tail;
		tail = tail->next;
	}
	return NULL;
}

void sendipc(char *to, char *text, int broadcast) 
{
	int count;

	if (broadcast) 
	{
		count = ipc_send_to_all(IPC_SCRIPTIPC, text);
	}
	else
	{
		count = ipc_send_to_username(to, IPC_SCRIPTIPC, text);
	}
	if (count==0)
	{
		printf("User '%s' is not logged on.\n",to);
	}else
	{
		if (broadcast)
			mwlog("SENDIPB %s", text);
		else
			mwlog("SENDIPC(%s) %s",to,text);
	}
}

void sendrpc(char *to, char *type, char *text, int broadcast) 
{
	char buff[MAXTEXTLENGTH];
	int count;

	snprintf(buff, MAXTEXTLENGTH, "%s %s", type, text);
	if(broadcast)
	{
		count = ipc_send_to_all(IPC_SCRIPTRPC, buff); 
	}
	else
	{
		count = ipc_send_to_username(to, IPC_SCRIPTRPC, buff);
	}
	if (count==0)
	{
		printf("User '%s' is not logged on.\n",to);
	}else
	{
		if (broadcast)
			mwlog("SENDRPB(%s) %s", type, text);
		else
			mwlog("SENDRPC(%s, %s) %s", to, type, text);
	}
}


void enter_talker(int logontype)
{
	extern int	autowho;
	extern int	talker_logontype;

	/* set the 'summoned' logon mask */
	if (logontype == 2) talker_logontype |= LOGONMASK_SUMMONED;

	if (runautoexec)
	{
		/* run all talker init functions */
		if (cp_flags(user->chatprivs, CP_SCRIPT, CM_MODE_ANY)) RunInitFuncs(1);

		runautoexec=0;
	}

	/* if user wishes to automatically display a wholist on talker entry */
	if (autowho)
	{
		update_user(user,userposn);
		who_list(0);
		/* turn off the autowho, so it only occurs on first usage */
		autowho = 0;
	}
}

void t_help(CommandList *cm, int argc, const char **argv, char *args)
{
	CommandList	*c;
	char		*x;
	int		found=false;

	if (argc < 2)
	{
		help_list(chattable, 1);
		return;
	}

	if (args!=NULL && (strchr(args,'.')!=NULL || strchr(args,'/')!=NULL))
	{
		printf("Sorry, no help available on that subject.\n");
		return;
	}

	if (args==NULL)
	{
		x = makepath(TALKHELP,"/","general");
		if (!access(x,00))
			printfile(x);
		else
			printf("No general help available for talker commands.\n");
	}else
	{
		/* find the command in the list */
		c = chattable;
		while (c->Command && strcasecmp(c->Command, args)) c++;

		/* do a permissions check */
		if (c->Command)
		{
			if ((rights & c->Rights) != c->Rights)
			{
				printf("Sorry, no help available on that subject.\n");
				return;
			}
		}

		/* display the file */
		x=makepath(TALKHELP,"/",args);
		if (!access(x,00))
		{
			printfile(x);
			found=true;
		}
		if (!found) printf("Sorry, no help available on that subject.\n");
	}
}

void t_replay(CommandList *cm, int argc, const char **argv, char *args)
{
	if (argc < 2) {
		printf("Insufficient arguments.\n");
		return;
	}

	ipc_message_t * msg = ipcmsg_create(IPC_REPLAY, userposn);
	json_t * j = json_init(NULL);
	json_addint(j, argv[1], atoll(argv[2]));
	ipcmsg_json_encode(msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
}

/* changes room, returning error code on failure
 * 0 - ok
 * 1 - hidden/locked
 * 2 - frozen
 * 3 - invalid direction/room
 * 4 - invalid number
 * 5 - OOM
 */
int ChangeRoom(char *room, int quiet)
{
	struct room tmp;
	int i;
	unsigned short oldroom = user->room;
	AUTOFREE_BUFFER buff=NULL;

	if (cm_flags(user->chatmode,CM_FROZEN,CM_MODE_ANY))
	{
		return(2);
	}

	i = 0;
	if (isanum(room, &i, 1))
	{
		if (i>=0 && i<=65535)
		{
			RoomInit(&tmp);
			if (LoadRoom(&tmp, i))
			{
				int found = 1;

				if (tmp.locked>0) found = 0;

				if (!found)
				{
					RoomDestroy(&tmp);
					return(1);
				}
			}
			else
			{
				user->room=i;
			}
			RoomDestroy(&tmp);
		}
		else
		{
 			return(4);
		}

		if (i==oldroom)
		{
			if (!quiet) printf("You are already in room %d\n", i);
		}
		else
		{
			if (asprintf(&buff, "\03312%s has just arrived in room %d", user->name, i) < 0)
			{
				return 5;
			}
			if (!quiet) talk_send_raw(buff, i);
			user->room = i;
			update_user(user, userposn);

			RoomDestroy(&myroom);
			RoomInit(&myroom);
			LoadRoom(&myroom, user->room);

			SAFE_FREE(buff);
			if (asprintf(&buff, "\03313%s has left to room %d", user->name, i) < 0)
			{
				return 5;
			}
			if (!quiet) talk_send_raw(buff, oldroom);
			SAFE_FREE(buff);
			if (asprintf(&buff, "Leaving room %d for room %d", oldroom, user->room) < 0)
			{
				return 5;
			}
			if (!quiet) display_message(buff, 1, 1);

			enter_room(user->room);
		}
	}
	else
	{
		return(4);
	}

	return(0);
}
