#ifndef CLIENT_USER_H
#define CLIENT_USER_H

#include <stdint.h>
#include <user.h>

struct person * user_get(const char * name);
char *getmylogin(void);
void get_login(char *name, int autochat);
int get_person(int file, struct person *tmp);
void update_user(struct person *record, int32_t userposn);
void list_users(int newonly);
void list_users_since(long date);
void login_ok(struct person *usr, int32_t *userposn, int *autochat);
void strip_name(char *string);
void pick_salt(char *salt);
void search(const char *args, const char *ptr);
int is_old(struct person *usr, const char *name, int32_t *userposn);

#endif
