/*********************************************************
 *     The Milliways III System is copyright 1992        *
 *      J.S.Mitchell. (arthur@sugalaxy.swan.ac.uk)       *
 *       see licence for furthur information.            *
 *********************************************************/

#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <sys/wait.h>
#include <stdarg.h>
#include <termcap.h>
#include <stdbool.h>
#include <sys/stat.h>

#include "command.h"
#include "alarm.h"
#include "strings.h"
#include "str_util.h"
#include "iconv.h"

#include "talker.h"
#include "chattable.h"
#include "completion.h"

#include <readline/readline.h>
#include <readline/history.h>
#include <arpa/inet.h>

#include "talker_privs.h"
#include "special.h"
#include "frl.h"

#include "incoming.h"
#include "script.h"
#include "rooms.h"
#include "perms.h"
#include "ipc.h"
#include "js.h"
#include "mesg.h"
#include "main.h"
#include "new.h"
#include "echo.h"
#include "add.h"
#include "colour.h"
#include "init.h"
#include "intl.h"
#include "ipc.h"
#include "bb.h"
#include "userio.h"
#include "who.h"

extern struct room myroom;

#include "alias.h"
extern Alias bind_list;
extern Alias alias_list;
extern Alias rpc_list;
extern Alias event_list;
extern Alias onoff_list;
extern Alias ipc_list;
extern Alias force_list;
extern Alias shutdown_list;
extern Alias eventin_list;

static char version[]="Milliways III - Release "VER_MAJ"."VER_MIN"."VER_TWK"\n";

/* global termcap usage variable */
int g_boTermCap = 0;

int currentfolder=0,last_mesg=0;
int internet=0;
int autochat=1;
int autowho=0;
const char *autoexec_arg;
const char *targethost = NULL;

struct alarm *timeout_event_1, *timeout_event_2;
extern int script_terminate;
extern int script_running;
int idle_count;
int remote=false;
FILE *output;
int busy=0; /* if true dont display messages  i.e. during new/write */
int quietmode=0;
int inreadline=0;
char *saved_rl_text;
int saved_rl_point;
int saved_rl_mark;
int saved_history;
int current_rights = 0;

char clear_line[]="\033[M\r";

struct person *user = NULL;
struct folder *fold = NULL;
int32_t userposn;
unsigned long loggedin; /* what time did they login */
unsigned long rights=(unsigned long)0;
unsigned long forced;

/* readline stuff */
int UseRL=0;
char **complete_entry(const char *text, int start, int end);
void accept_command(char *comm);
static void interrupt(int sig);

static char prompt[41];
static char comm[MAXTEXTLENGTH];
static int eof_count = 0;

/* are we running it as a privaledged user */
static int god_mode(void)
{
	/* old god mode, run as owner */
	if (getuid()==geteuid()) {
		return 1;
	}

	return 0;
}

void set_rights(void)
{
	rights=0l;
	if (u_god(user->status)) rights=1l;
	if (s_wizchat(user->special)) rights|=2l;
	if (s_superuser(user->special)) rights|=4l;
	if (u_mod(user->status)) rights|=8l;
	if (!internet && getuid()!=500) rights|=16l;
	if (u_reg(user->status)) rights|=32l;
	if (s_timeout(user->special)) rights|=64l;
	if (!forced) rights|=256l;
	current_rights = RIGHTS_BBS;
}

static void set_prompt(void)
{
	char pre[20];

	if (UseRL) 
		sprintf(pre,"%s", "");
		/*sprintf(pre,"%s",clear_line);*/
	else strcpy(pre,"\r");

	if (is_stacked())
	{
		prompt[0]=0;
	}
	else
	{
		if (cm_flags(user->chatmode,CM_ONCHAT,CM_MODE_ANY)) 
			snprintf(prompt,40, "%stalk{%d}-* ",pre,user->room);
		else
		if (u_god(user->status))
			snprintf(prompt,40,"%s{%s}---* ",pre,fold->name);
		else
		if (u_reg(user->status))
			snprintf(prompt,40, "%s<%s>---> ",pre,fold->name);
		else
			snprintf(prompt,40, "%s[%s] _@/ ",pre,fold->name);
	}
}

static void restore_tty(void)
{
	struct termios termios;

	if (UseRL)
	{
		/* Readline may not restore these settings if the
		 * callback interface is used.
		 */
		tcgetattr(0, &termios);
		termios.c_lflag |= ICANON|ECHO;
		termios.c_iflag |= ICRNL;
		tcsetattr(0, TCSANOW, &termios);
	}
}

int disable_rl(int savetext)
{
	if (!inreadline) return 0;
	if (UseRL)
	{
		if (savetext)
		{
			saved_history = where_history();
			saved_rl_text = rl_copy_text(0, rl_end);
			saved_rl_mark = rl_mark;
			saved_rl_point = rl_point;
			rl_delete_text(0, rl_end);
			rl_redisplay();
		}
		rl_callback_handler_remove();
		write(1, clear_line, strlen(clear_line));
		restore_tty();
	}
	inreadline = 0;
	return 1;
}

static void accept_line(char *line)
{
	int conversion_result;
	if (line == NULL)
	{
		*comm=0;
		eof_count++;
		mwlog("EOF");
		if (eof_count>3)
		{
			broadcast(1, "\03304%s%s connection has just dropped." ,user->name, user->name[strlen(user->name)]=='s'?"'":"'s");
			mwlog("EOF(LOGOUT)");
			close_down(2, NULL, NULL);
		}
	} else
	{
		
		//strncpy(comm, line, MAXTEXTLENGTH-1);
		//comm[MAXTEXTLENGTH-1] = '\0';
		conversion_result=convert_string_charset(line, "LOCAL", strlen(line), comm, "UTF-8", MAXTEXTLENGTH-100, NULL, NULL, NULL, NULL, "?");
		if(conversion_result >= 0)
		{
			if( conversion_result & WINVALIDCHARS )
			{
				printf("Warning: Your input contained characters that are invalid in your current locale.\n  Please ensure your terminal and locale are using the same character set\n"); 
			}
			eof_count=0;
			strip_str(comm); 
			if (new_mail_waiting==1)
			{
				display_message(_("\03305*** You have new mail.\n"), 1, 1);
				new_mail_waiting=0;
			}else
			if (new_mail_waiting>1)
			{
				char tbuff[MAXTEXTLENGTH];
				snprintf(tbuff, MAXTEXTLENGTH-1, _("\03305*** You have %d new mail messages.\n"),new_mail_waiting);
				display_message(tbuff, 1, 1);
				new_mail_waiting=0;
			}
			user->idletime=time(0);
			update_user(user,userposn);
			if (*comm == 0) return;
	#ifdef RWHO
			rwho_update();
	#endif 
			disable_rl(0);
			update_user(user,userposn);
			stack_str(comm);
		}
	}
}

static void enable_rl(void)
{
	if (inreadline) return;
	set_prompt();
	inreadline = 1;
	if (!UseRL)
	{
		printf("%s", prompt);
		fflush(stdout);
		return;
	}
	rl_callback_handler_install(prompt, &accept_line);
	if (saved_rl_text)
	{
		history_set_pos(saved_history);
		rl_insert_text(saved_rl_text);
		rl_mark = saved_rl_mark;
		rl_point = saved_rl_point;
		rl_redisplay();
		free(saved_rl_text);
		saved_rl_text = NULL;
	}
}

static int match_arg_str(char *srch, const char *sub)
{
	char *match;
	int value = 0;

	match = malloc(sizeof(char) * (strlen(sub) + 3));
	sprintf(match, "-%s", sub);
	if (!strcasecmp(srch, match)) value = 1;
	sprintf(match, "--%s", sub);
	if (!strcasecmp(srch, match)) value = 1;
	sprintf(match, "/%s", sub);
	if (!strcasecmp(srch, match)) value = 1;
	free(match);

	return(value);
}


static void termcap_screenchange(int n)
{
	/* termcap not available */
	if (g_boTermCap == 0) return;

	/* reinitialise termcap */
	tgetent(NULL, getenv("TERM"));
}


static void init_termcap(void)
{
	char	*szTermType;
	int	nResult;

	/* get terminal type */
	szTermType = getenv("TERM");

	/* no terminal could be found */
	if (szTermType == NULL)
	{
		/* set termcap usage to off */
		g_boTermCap = 0;
		return;
	}

	/* now attempt to get termcap information */
	nResult = tgetent(NULL, szTermType);

	/* failed to get termcap database, or terminal not defined */
	if (nResult <= 0)
	{
		/* set termcap usage to off */
		g_boTermCap = 0;
		return;
	}

	/* use termcap */
	g_boTermCap = 1;

	/* set up a signal for screen size change, to allow termcap to be reinitialised */
	signal(SIGWINCH, termcap_screenchange);
}

static char *dummy_list(const char *text, int state)
{
	return(NULL);
}

int main(int argc, char **argv)
{
	struct sigaction s;

	unsigned long oldchat;
	int al;
	int qflag = 0;
	int tty_timeout = 500; /* milliseconds */
	int view_new = 0;
	int view_who = 0;
	int view_what = 0;
	int view_help = 0;
	int inet_mode = 0;
	int view_since = 0;
	int inarg_num = -1;
	int foldername_num = -1;
	int folderuser_num = -1;
	int folderuser_replyto = -1;
	int msguser_num = -1;

	init_locale();
#if 0
	printf("text domain base is %s\n", bindtextdomain(PACKAGE, LOCALEDIR));
	//bind_textdomain_charset(PACKAGE, "UTF-8");
	printf("text domain is %s\n", textdomain(PACKAGE));
#endif

	/* initialise random seed */
	srand(time(NULL));

	/* initialise termcap */
	init_termcap();

	/* create client user information */
	user=(struct person *)malloc(sizeof(*user));
	fold=(struct folder *)malloc(sizeof(*fold));

	/* init ignore list */
	ignored = NULL;
	output=stdout;

#ifdef GNUTLS_VERSION_MAJOR
	/* Prior to 3.3.0 this is not thread safe
	 * so should be done early to prevent a race.
	 **/
	gnutls_global_init();
#endif

#ifdef NEED_LINEBUF
	setlinebuf(stdout);
	setlinebuf(stderr);
#endif
	setvbuf(stdin, NULL, _IONBF, 0);

	for (al=1; al<argc; al++)
	{
		/* standalone modifier arguments */
		if (match_arg_str(argv[al], "autochat")) autochat = 1;
		if (match_arg_str(argv[al], "noautochat")) autochat = 0;
		if (match_arg_str(argv[al], "autowho")) autowho = 1;
		if (match_arg_str(argv[al], "quiet")) qflag = 1;
		if (match_arg_str(argv[al], "new")) view_new = 1;
		if (match_arg_str(argv[al], "who")) view_who = 1;
		if (match_arg_str(argv[al], "what")) view_what = 1;
		if (match_arg_str(argv[al], "i")) inet_mode = 1;
		if (match_arg_str(argv[al], "since")) view_since = 1;

		/* help listing arguments */
		if (match_arg_str(argv[al], "h")) view_help = 1;
		if (match_arg_str(argv[al], "help")) view_help = 1;
		if (match_arg_str(argv[al], "?")) view_help = 1;

		/* 'autoexec' argument. this are passed into all autoexec/initfunc script functions
		   as the $* argument string */
		if (match_arg_str(argv[al], "ae_arg"))
		{
			if (al < (argc - 1))
			{
				inarg_num = al+1;
				al++;
				continue;
			}
			else view_help = 1;
		}

		/* view all messages as a particular user */
		if (match_arg_str(argv[al], "X"))
		{
			if (al < (argc - 1))
			{
				msguser_num = al+1;
				al++;
				continue;
			}
			else view_help = 1;
		}

		/* view all messages as a particular user */
		if (match_arg_str(argv[al], "server"))
		{
			if (al < (argc - 1))
			{
				targethost = argv[al+1];
				al++;
				continue;
			}
			else view_help = 1;
		}

		/* write message in folder from user */
		if (match_arg_str(argv[al], "f"))
		{
			if (al < (argc - 3))
			{
				foldername_num = al+1;
				folderuser_num = al+2;
				folderuser_replyto = al+3;
				al+=3;
				continue;
			}
			else view_help = 1;
		}
	}

	if (view_help)
	{
		printf(_("\nMilliways accepts the optional arguments of:\n\n"));
		printf(_("  -ae_arg <a> Pass the string 'a' in to all 'initfunc' functions as $*\n"));
		printf(_("  -autochat   Automatically use SUCS username to log onto talker\n"));
		printf(_("  -noautochat Disable -autochat mode.\n"));
		printf(_("  -autowho    Show a 'who' list automatically on log on to talker\n"));
		printf(_("  -i          Internet mode (no readline)\n"));
		printf(_("  -new        Summary of new messages and quit\n"));
		printf(_("  -server <a> Connect to server <a>\n"));
		printf(_("  -since      Lists people logged on between now and when you last logged on\n"));
		printf(_("  -who        Show a list of users logged on and quit\n"));
		printf(_("  -what       Show a list of what users are doing\n"));
		/*
		printf("  -quiet              Don't announce your login to board or talker\n");
		printf("  -X <user>           Display messages for that user\n");
		printf("  -f <folder> <user>  Write message in folder from user\n");
		*/

		printf(_("\nTo view this help message, use the arguments:  -h, -help, or -?\n"));
		printf(_("To specify arguments, you may use '--', or '/' instead of '-'.\n\n"));

		free(user);
		free(fold);
		exit(0);
	}

	if (view_new)
	{
		char *b;
		b=(char *)getmylogin();
		if (b!=NULL)
		{
			if (is_old(user,b,&userposn))
				list_new_items(user,true);
		}else
			printf(_("Username not permitted.\n"));
	}
	if (view_who)
	{
		char *b;
		b=(char *)getmylogin();
		if (b!=NULL)
		{
			/* try and load user - dont care if it fails */
			is_old(user,b,&userposn);
			who_list(0);
		}else
			printf(_("Username not permitted.\n"));
	}
	if (view_what)
	{
		char *b;
		b=(char *)getmylogin();
		if (b!=NULL)
		{
			/* try and load user - dont care if it fails */
			is_old(user,b,&userposn);
			what_list();
		}else
			printf(_("Username not permitted.\n"));
	}
	if (view_since)
	{
		char *b;
		b=(char *)getmylogin();
		if (b!=NULL)
		{
			/* try and load user - if ok, view since info */
			if (is_old(user,b,&userposn))
			{
				/* view since */
				list_users_since(user->lastlogout);
			}
		}else
			printf(_("Username not permitted.\n"));
	}

	/* if any 'view and quit' options specified, then quit */
	if (view_new || view_who || view_since || view_what)
	{
		free(user);
		free(fold);
		exit(0);
	}

	if (inarg_num>-1) autoexec_arg = argv[inarg_num];
	else autoexec_arg = "";

	/* set up internet mode */
	if (inet_mode)
	{
		internet=1;
		force_line_mode();
	}else
	{
		/* setup readline support */
		UseRL=1;
		rl_readline_name = "Milliways";
		rl_attempted_completion_function= (CPPFunction *)complete_entry;
		rl_completion_entry_function= (CPFunction *)dummy_list;
		rl_directory_rewrite_hook= (Function *)expand_script_dir;
	}

	signal(SIGPIPE,SIG_IGN);
	signal(SIGHUP,SIG_IGN);
#ifdef SIGTSTP
	signal(SIGTSTP,SIG_IGN);
#endif
	if(!god_mode())
		signal(SIGQUIT,SIG_IGN);

	/* setup incoming signal handlers */

	s.sa_handler = &interrupt;
	s.sa_flags = SA_RESTART;
	sigemptyset(&s.sa_mask);
	sigaction(SIGINT, &s, NULL);

	timeout_event_1 = timeout_event_2 = NULL;
	alarm_init();

	ipc_close();

	/* display all new messages for given user */
	if (msguser_num>-1 && god_mode())
	{
		int32_t tmp;
		if (!is_old(user,argv[msguser_num],&tmp))
		{
			fprintf(stderr,_("%s: User %s not found.\n"),argv[0],argv[msguser_num]);
			exit(-1);
		}
		remote=true;
		new(user);  
		update_user(user,tmp);
		free(user);
		free(fold);
		exit(0);
	}
	 
	/* jump straight to add_mesg() */
	if (foldername_num>-1 && god_mode())
	{
		int32_t tmp;
		int rt;
		currentfolder=foldernumber(argv[foldername_num]);
		if (currentfolder==-1)
		{
			fprintf(stderr,_("%s: Folder %s not found.\n"),argv[0],argv[foldername_num]);
			free(user);
			free(fold);
			exit(-1);
		}
		if (!is_old(user,argv[folderuser_num],&tmp))
		{
			fprintf(stderr,_("%s: User %s not found.\n"),argv[0],argv[folderuser_num]);
			free(user);
			free(fold);
			exit(-1);
		}
		rt = atoi(argv[folderuser_replyto]);
		remote=true;
		add_msg(currentfolder,user,rt);
		free(user);
		free(fold);
		exit(0);
	}
/*
	if(internet) printf("%c%c%c%c%c%c",IAC,WILL,TELOPT_SGA, IAC,WILL,TELOPT_SGA);
	else
*/
	save_terminal();

	/* show login banner, and version info */
	/* allow disabling of banner in release version */
	if (!qflag) printfile(makepath(LOGIN_BANNER,"",""));
	printf("%s\n",version);

	/* log the user in by asking for their name (and details) */
	login_ok(user, &userposn, &autochat);

	if (!access(LOCKFILE,00))
	{
		if (u_god(user->status))
		{
			printf(_("The board is currently locked to normal users.\n"));
		}else
		{
			printf(_("The Board has been temporarily closed.\n"));
			printf(_("Please call again soon.\n"));
			free(user);
			free(fold);
			exit(0);
		}
	}

	/* we need to be logged in before we connect to the server */
	if (targethost == NULL)
		targethost = "localhost";

	ipc_connect(targethost);

	if ((s_quiet(user->special) || u_god(user->status)) && qflag)
	{
		extern int talker_logontype;
		quietmode=1;
		talker_logontype |= LOGONMASK_QUIET;
	}else
	{
		broadcast(1, _("\03301%s has just entered the board."),user->name);
		quietmode=0;
	}

#ifdef RWHO
	rwho_update();
#endif

	{
		struct sockaddr sa;
		socklen_t ss;

		ss=sizeof(sa);
		/* can we log a hostname ? */
		if (getpeername(fileno(stdin), &sa , &ss))
			mwlog("LOGIN");
		else
		{
			mwlog("LOGIN from %s", inet_ntoa(((struct sockaddr_in *)&sa)->sin_addr) );
		}	
	}	
			
	time_t when = user->lastlogout;
	printf(_("\nLast logged out %s\n\n"),ctime(&when));
	if (u_reg(user->status)) {
		if (!autochat) {
		printf(_("Type 'talker' to enter chat mode.\n"));
		}
	}
	else
	{
		/* Must be an unregistered guest */
		printf(_("You must register before being able to use this system fully.\n"));
		printf(_("Until then you will not be able to write in most folders or use chat mode.\n"));
		printf(_("Wait here for a few minutes and an administrator might register you.\n"));
	}
	if (!autochat) printf(_("Type 'help' for help.\n"));
	loggedin=time(0);

	/* initialise script variables */
	script_init();
	setup_js();

	/* clear transient talker flags */
	oldchat = user->chatmode;
	user->chatmode=0;
	/* except those that stick */
	if (cm_flags(oldchat, CM_SPY, CM_MODE_ANY))
		user->chatmode |= CM_SPY;
	if (cm_flags(oldchat, CM_VERBOSE, CM_MODE_ANY))
		user->chatmode |= CM_VERBOSE;
	if (cm_flags(oldchat, CM_STICKY, CM_MODE_ANY))
		user->chatmode |= CM_STICKY;
	if (cm_flags(oldchat, CM_FROZEN, CM_MODE_ANY))
		user->chatmode |= CM_FROZEN;
	else
	{
		if (!cm_flags(oldchat, CM_STICKY|CM_FROZEN, CM_MODE_ANY)) user->room=0;
	}
	if (cm_flags(oldchat, CM_GAG1, CM_MODE_ANY))
		user->chatmode |= CM_GAG1;
	if (cm_flags(oldchat, CM_GAG2, CM_MODE_ANY))
		user->chatmode |= CM_GAG2;
	if (cm_flags(oldchat, CM_GAG3, CM_MODE_ANY))
		user->chatmode |= CM_GAG3;
	if (cm_flags(oldchat, CM_GAG4, CM_MODE_ANY))
		user->chatmode |= CM_GAG4;
	if (cm_flags(oldchat, CM_GAG5, CM_MODE_ANY))
		user->chatmode |= CM_GAG5;
	if (cm_flags(oldchat, CM_GAG6, CM_MODE_ANY))
		user->chatmode |= CM_GAG6;
	user->chatmode |= (oldchat & CM_PROTMASK);

	user->idletime=time(0);
	update_user(user,userposn);	

	/* list new BBS items */
	list_new_items(user,true);
	if (!get_folder_number(fold,currentfolder))
	{
		currentfolder = -1;
		fold->name[0] = '\0';
	}

	InitParser();
	set_rights();

	init_colour();
	
	RoomInit(&myroom);

/*	if (!(rights&65l))  a pleb - timeout of 10 mins 
		user->timeout=600; */
	update_user(user,userposn);

	/* run all board init functions */
	RunInitFuncs(0);

	/* broadcast board logon signal to other users */
	broadcast_onoffcode(3, autochat, NULL, NULL);

	/* autochat - log onto talker immediately */
	if (autochat && u_reg(user->status)) t_chaton();

	/*enable_rl();*/
	reset_timeout(user->timeout);
	tty_timeout = 50;
	for (;;)
	{
		int tty_ready;

		if (is_stacked())
		{
			disable_rl(1);
			forced = pop_stack(comm,MAXTEXTLENGTH);
			accept_command(comm);
			tty_timeout = 50;
		}
		tty_ready = idle(busy? -1: 0, tty_timeout);
		tty_timeout = 500;
		/* we dont seem to be getting them all */
		if (ferror(stdin) || (tty_ready < 0 && errno != EINTR))
		{
			if (ferror(stdin)) printf("!!!\n");
			printf("<%d>\n", busy);
			fflush(stdout);
			perror("stdin");
			mwlog("ERROR on stdin");
			close_down(5, NULL, NULL);
		}
		if (!busy && MesgIsStacked())
		{
			DisplayStack();
			tty_timeout = 50;
		}
		enable_rl();
		if (tty_ready > 0) {
			if (UseRL)
			{
				rl_callback_read_char();
				reset_timeout(user->timeout);
			}else
			{
				get_str(comm,1023);
				reset_timeout(user->timeout);
				accept_line(comm);
				disable_rl(0);
			}
		}
	}	
}

void accept_command(char *cmd)
{
	char history_comm[MAXTEXTLENGTH];
	int conversion_result;
	
	if (UseRL)
	{
		conversion_result=convert_string_charset(cmd, "UTF-8", strlen(cmd), history_comm, "LOCAL", MAXTEXTLENGTH, NULL, NULL, NULL, NULL, NULL);
		if(conversion_result >= 0)
		{
			add_history(history_comm);
			saved_history++;
		}
	}
	if (cm_flags(user->chatmode,CM_ONCHAT,CM_MODE_ANY))
	{	
		if (cmd[0]==*CMD_BOARD_STR)
		{
			set_rights();
			DoCommand(&cmd[1], table);
			set_talk_rights();
		}else
		if (cmd[0]==*CMD_TALK_STR)
		{
			set_talk_rights();
			DoCommand(&cmd[1], chattable);
		}else
		if (cmd[0]==*CMD_SCRIPT_STR && cp_flags(user->chatprivs,CP_SCRIPT,CM_MODE_ANY))
		{
			set_talk_rights();
			DoScript(&cmd[1]);
		}else {
			char *event_name=NULL;
			script_output=1;
			while ((event_name = NextLink(eventin_list, event_name)) != NULL) {
				if (is_js(event_name)) {
					const char *argv[2];
					argv[0] = cmd;
					argv[1] = NULL;
					busy++;
					js_exec(event_name, 1, argv);
					busy--;
				}
			}
			if (script_output) chat_say(cmd);
		}
	}else
	{
		set_rights();
		if (comm[0]==*CMD_BOARD_STR)
			DoCommand(&cmd[1], table);
		else
			DoCommand(cmd, table);
	}
}

void close_down(int exitmode, char *sourceuser, char *reason)
{
	extern char *event_user;
	extern char *event_body_text;
	extern struct IgnoreList *ignored;
	struct IgnoreList *temp;

	disable_rl(0);

	/* if we are currently on the talker */
	if (cm_flags(user->chatmode,CM_ONCHAT,CM_MODE_ANY))
	{
		/* if scripts are available */
		if (cp_flags(user->chatprivs, CP_SCRIPT, CM_MODE_ANY))
		{
			char	*shutdown_name;

			/* find the shutdown function */
			shutdown_name = NULL;
			if ((shutdown_name = NextLink(shutdown_list, shutdown_name)) != NULL)
			{
				/* if SU, give user who kicked us off if present */
				if (u_god(user->status)) ExecEvent(shutdown_name, "", "ShutDown", mrod_user, 0);
				/* otherwise, no info given */
				else ExecEvent(shutdown_name, "", "ShutDown", NULL, 0);
			}
		}
		/* give 'straight to shell' logoff code */
		broadcast_onoffcode(0, 3, sourceuser, NULL);
	}

	/* send different broadcast depending on exitmode */
	/* 0 - normal */
	/* 1 - timeout */
	/* 2 - too many eof's */
	/* 3 - mrod */
	/* 4 - banned */
	/* 5 - error */
	broadcast_onoffcode(2, exitmode, sourceuser, reason);


	/* update user information */
	user->timeused += time(0) - loggedin;
	user->lastlogout = time(0);
	update_user(user, userposn);

	/* free ignorelist */
	while (ignored != NULL)
	{
		temp = ignored;
		ignored = ignored->next;
		free(temp->name);
		free(temp);
	}

	destroy_colours();
	if (event_user != NULL) free(event_user);
	if (event_body_text != NULL) free(event_body_text);
	if (mrod_user != NULL) free(mrod_user);
	if (saved_rl_text != NULL) free(saved_rl_text);
	colour_free();
	DestroyAllLinks(&bind_list);
	DestroyAllLinks(&rpc_list);
	DestroyAllLinks(&alias_list);
	DestroyAllLinks(&event_list);
	DestroyAllLinks(&onoff_list);
	DestroyAllLinks(&ipc_list);
	DestroyAllLinks(&force_list);
	DestroyAllLinks(&shutdown_list);

	ScriptCleanup();

	RoomDestroy(&myroom);
	ClearStack();
	stop_js();
	alarm_cleanup();

	/* dont display logoff text if quiet, or if dropped */
	if (!quietmode && exitmode!=1 && exitmode!=2)
	{
		broadcast(1, "\03302%s has just left the board.", user->name);
	}

	mwlog("LOGOUT");
	sleep(1); //dodgy hack for race condition in checkonoff, cunningly we currently get woken by the very message we're waiting for.
#ifdef RWHO
	rwhocli_userlogout(user->name);
#endif

	free(user);
	free(fold);
	ipc_close();

	exit(0);
}

void printfile(char *filename)
{
	FILE *afile;
	char *buff;
	int linecount = 0;
	char foo[6];
	int screen_height = screen_h();

	if ((afile=fopen(filename,"r"))==NULL)
	{
		perror(filename);
		exit(-1);
	}

	/*buff=malloc(sizeof(char) * 512);*/
	/*while ((buff=fgets(buff, 512, afile))!=NULL)*/
	while ((buff = frl_line(afile)) != NULL)
	{
		fwrite(buff,1,strlen(buff),stdout);
		fputs("\n", stdout);
		linecount++;
		if (linecount>=(screen_height-2))
		{
			printf(_("---more---\r"));
			echo_off();
			get_str(foo,5);
			echo_on();
			if (foo[0]=='q'|| foo[0]=='Q') break;
			printf("            \r");
			linecount=0;
		}
		free(buff);
		buff = NULL;
	}
	if (buff) free(buff);

	fclose(afile);
}

int idle(int fd, int millis)
{
	static int in_idle = 0;
	fd_set readfds, exceptfds;
	static struct timeval tmout;
	int nfds;
	static int fl=0;
	int select_error;

	static int reconnect_backoff = 1;
	static time_t reconnect_last = 0;

	in_idle++;

	int incoming_pipe = ipc_getfd();
	FD_ZERO(&readfds);
	FD_ZERO(&exceptfds);
	FD_SET(incoming_pipe, &readfds);
	FD_SET(incoming_pipe, &exceptfds);
	if (fd >= 0)
	{
		FD_SET(fd, &readfds);
		FD_SET(fd, &exceptfds);
		fl = fcntl(fd, F_GETFL);
		fcntl(fd, F_SETFL, fl & ~O_NDELAY);
	}

	if (millis >= 0)
	{
		tmout.tv_sec = millis / 1000;
		tmout.tv_usec = (millis % 1000) * 1000;
	}

	nfds = alarm_select((fd>incoming_pipe)?(fd+1):(incoming_pipe+1), &readfds, NULL, &exceptfds, (millis<0)?(NULL):(&tmout));
	select_error = errno;
	if (fd >= 0) fcntl(fd, F_SETFL, fl);
	if (nfds > 0) {
		if (FD_ISSET(incoming_pipe, &exceptfds)) {
			fprintf(stderr, _("\nError reading incoming message pipe. panic.\n"));
			return -1;
		}
		if (fd>=0 && FD_ISSET(fd, &exceptfds)) {
			fprintf(stderr, _("\nError on input terminal, argh.\n"));
			return -1;
		}
		if (FD_ISSET(incoming_pipe, &readfds)) 
		       handle_mesg();
	}
	in_idle--;

	/* we are not connected, attempt reconnect */
	if (ipc_connected() == 0) {
		time_t now = time(0);
		if (now > reconnect_last + reconnect_backoff) {
			ipc_check();
			reconnect_last = now;
			if (!ipc_connected()) {
				/* failed again, backoff */
				reconnect_backoff *= 2;
			}
		}
	} else {
		reconnect_backoff = 1;
	}

	errno = select_error;
	if (nfds<0 && select_error == EINVAL)
	{
		char buf[256];
		snprintf(buf, 255, "\n{EINVAL: fd=%d, incoming_pipe=%d}\n", fd, ipc_getfd());
		write(1,buf,strlen(buf));
	}
	if (nfds > 0 && fd >= 0 && FD_ISSET(fd, &readfds)) return 1;
	else if (nfds < 0) return nfds;
	else return 0;
}

static void printline_in_local(char *line, int *warnings, size_t *not_in_local)
{
	char local_line[MAXTEXTLENGTH];
	int		conversion_result;
	size_t	irreversible=0, outbyte=0, inbyte=0;
	
	conversion_result=convert_string_charset(line, "UTF-8", strlen(line), local_line, "LOCAL//TRANSLIT", MAXTEXTLENGTH, &outbyte, &irreversible, &inbyte, NULL, "#");
	if(conversion_result>=0)
	{
		*not_in_local += irreversible;
		*warnings = *warnings | conversion_result;
		write(1,local_line,strlen(local_line));

	}
	else
	{
		printf("Error: An error of type %d occured occured trying to convert a message into your local charset.\n", conversion_result);
	}

}

void format_message(const char *format, ...)
{
	char * text = NULL;
	va_list va;
	va_start(va, format);
	vasprintf(&text, format, va);
	va_end(va);
	display_message(text, 0, 1);
	free(text);
}

void display_message(const char *text, int beeps, int newline)
{
	static int	count = 0;
	int		len;
	int		ptr;
	int		concealed = 0;
	char		line[MAXTEXTLENGTH];	
	int		i, j, colrstart;
	int		hascolour;
	int		screen_width = screen_w();
	char		*colr = NULL;
	int		endline;
	int		convert_warnings=0;
	size_t	not_in_local=0;

	if (text==NULL || strlen(text)==0)
	{
		sprintf(line,_("Error: Urk, no message to print.\n"));
		write(1,line,strlen(line));
		return;
	}
	if (UseRL && disable_rl(1)) count = 0;

	len=strlen(text);
	ptr=0;
	i=0;
	hascolour=0;
	colrstart=-1;

	while (len-ptr > 0)
	{
		if (text[ptr]==033)
		{
			char str[3];
			ptr++;
			if(len-ptr>0)
			{
				if( ((unsigned char)text[ptr] & 192) == 192 )
				{
					ptr++;
					str[0]='-';
					while( ((unsigned char)text[ptr] & 192) == 128 && len-ptr > 0)
					{
						ptr++;
					}
				}
				else
				{
					str[0]=text[ptr];
					ptr++;
				}
			}
			if(len-ptr>0)
			{
				if(((unsigned char)text[ptr] & 192)==192 )
				{
					ptr++;
					str[1]='-';
					while( ((unsigned char)text[ptr] & 192) == 128 && len-ptr > 0)
					{
						ptr++;
					}
				}
				else
				{
					str[1]=text[ptr];
					ptr++;
				}
			}
				
			/* escape sequence, skip next two chars */
			if (s_colouroff(user->special))
			{
				continue;
			}
			hascolour++;

			str[2]=0;
			
			colr=colour(str, &concealed);

			if (colr!=NULL)
			{
				if (colrstart >= 0)
					i = colrstart;
				else
					colrstart = i;

				for (j=0;j<strlen(colr);j++)
					line[i++]=colr[j];
			}
		}else
		if (text[ptr]>=040 && text[ptr]<=0176)
		{
			if (concealed)
			{
				ptr++;
			}
			else
			{
				line[i++]=text[ptr++];
				count++;
				colrstart = -1;
			}
		}else
		if ( (text[ptr] & 192) == 192 )
		{
			if (concealed)
			{
				ptr++;
			}
			else
			{
				line[i++]=text[ptr++];
				count++;
				colrstart = -1;
			}
			// stops us randomly spliting over a unicode multibyte character
			while( ((unsigned char)text[ptr] & 192) == 128 && len-ptr > 0 )
			{
				if (concealed)
				{
					ptr++;
				}
				else
				{
					line[i++]=text[ptr++];
					colrstart = -1;
				}
			}
			
		}
		else
		{
			ptr++;
		}
		if (i >= (MAXTEXTLENGTH-20))
		{
			line[i]='\0';
			printline_in_local(line, &convert_warnings, &not_in_local);
			i=0;
		}

		if (s_nolinewrap(user->special))
			endline = (ptr >= len);
		else
			endline = ((count >= screen_width) || (ptr >= len));

		if (endline)
		{
			if (!s_colouroff(user->special) && hascolour)
			{
				line[i++]=033;
				line[i++]='[';
				line[i++]='m';
			}

			if (newline || (ptr<len))
			{
				line[i++]='\n';
				count=0;
			}

			line[i]='\0';
			printline_in_local(line, &convert_warnings, &not_in_local);

			if (ptr>=len)
			{
				i=0;
				if (newline) count=0;
			} else
			{
				i=2;
				count=2;
				strcpy(line,"  ");
				/* Restore the colour from the last line */
				if (!s_colouroff(user->special) &&
				    hascolour &&
				    colr!=NULL)
				{
				    for (j=0;j<strlen(colr);j++)
					    line[i++]=colr[j];
				}
			}
		}
	}
	if(convert_warnings & WOUTPUTTOOSHORT)
	{
		printf("Warning: The buffer provided for conversion of the last line to your local charset was not big enough(some chars might have been lost) please kick a developer to fix this.\n");
	}
	if(convert_warnings & WINVALIDCHARS)
	{
		printf("Warning: There were invalid characters in the previous message that were replaced with '#'\n");
	}
	if(convert_warnings & WICONVFAIL)
	{
		printf("Warning: Something went wrong with iconv while printing the last message, some characters may have been lost\n");
	}
	if(not_in_local > 0)
	{
		printf("Warning: %d characters in the last message could not be displayed in your current character set.\n You might want to consider using UTF-8.\n", (int)not_in_local);
	}
	
	/*if (UseRL && inreadline) rl_forced_update_display();*/
	if (!u_beep(user->status) && beeps)
	{
		write(1,"\7",1);
	}
}

void interrupt(int sig)
{
	char msg[] = "\n*** Script Terminating - Please Wait ***\n";

	if (script_running)
	{
		script_terminate = 2;
		write(1, msg, strlen(msg));
	}
	if (js_isrunning())
	{
		js_stop_execution();
		write(1, msg, strlen(msg));
	}
}

static void time_out(void *idle_count_p)
{
	int *icnt = idle_count_p;
	if (idle_count_p == NULL) return;
	if (*icnt==1)
	{
		char msg[128];
		snprintf(msg, sizeof(msg), _("*** Timed Out, Good Bye\r\n"));
		++(*icnt);
		write(1,msg,strlen(msg));

		broadcast(1, _("\03304%s has been timed out.") ,user->name);
		mwlog("TIMEOUT(LOGOUT)");
		close_down(1, NULL, NULL);
	}
	else if (*icnt==0)
	{
		char msg[128];
		snprintf(msg, sizeof(msg), _("%c*** Wakey ! Wakey !\r\n"), 7);
		idle_count++;
		update_user(user,userposn);	
		write(1,msg,strlen(msg));
	}
	else
	{
		mwlog("TIMEOUT(BLOCKED)");
		close_down(1, NULL, NULL);
	}
	
}

void reset_timeout(int secs)
{
	idle_count = 0;
	if (timeout_event_1 != NULL) timeout_event_1->how = NULL;
	if (timeout_event_2 != NULL) timeout_event_2->how = NULL;
	if (secs > 0)
	{
		timeout_event_1 = alarm_after(secs, 0, &idle_count, &time_out);
		timeout_event_2 = alarm_after(secs+120, 0, &idle_count, &time_out);
	}
}
		
/* start of Readline Commands */

char *dupstr(const char *text, const char *prepend)
{
	char *c;

	c=(char *)malloc(strlen(text)+1+strlen(prepend));
	strcpy(c,prepend);
	strcat(c,text);
	return(c);
}

static char *strip_commandname(char *line)
{
	char *l2 = strdup(line);
	char *l2b = l2;
	char *cmd = strsep(&l2, " ");
	char *out = strdup(cmd);

	free(l2b);
	return(out);
}

static int word_count(char *text)
{
	int i;
	int mode;
	int count;

	/* add 1 to count on each space->letter transition */
	count=0;
	/* 0=space 1=text */
	mode=0;
	i=0;

	while (text[i]!=0)
	{
		if (isspace(text[i]))
			mode=0;
		else
		{
			if (mode==0)
			{
				mode=1;
				count++;
			}else
				mode=1;
		}
		i++;
	}
	return(count);
}

char **complete_entry(const char *text, int start, int end)
{
	char **matches;
	int wc;
	char *line;
	int mode=0;
	int oldrights;
	CompletionList *cl;
	int found=0;
	char *cmd = NULL;
	CommandList *tct = NULL;
	int inlen;

	matches=(char **)NULL;

	line=(char *)malloc(strlen(rl_line_buffer)+1);

	oldrights=rights;
	if (cm_flags(user->chatmode,CM_ONCHAT,CM_MODE_ANY))
	{
		if (rl_line_buffer[0]==*CMD_BOARD_STR)
		{
			strcpy(line,&rl_line_buffer[1]);
			mode=0;
			tct = table;
			set_rights();
		}else
		if (rl_line_buffer[0]==*CMD_TALK_STR)
		{
			strcpy(line,&rl_line_buffer[1]);
			mode=1;
			tct = chattable;
			set_talk_rights();
		}else
		if (rl_line_buffer[0]==*CMD_SCRIPT_STR && cp_flags(user->chatprivs,CP_SCRIPT,CM_MODE_ANY))
		{
			strcpy(line, &rl_line_buffer[1]);
			mode=2;
			set_talk_rights();
		}else
		{
			/* normal talker text - username tab-complete */
			if (s_tcunames(user->special)) {
				matches=rl_completion_matches(text, part_who_talk);
			}

			free(line);
			rl_attempted_completion_over=1;
			return(matches);
		}
	}else
	{
		strcpy(line,rl_line_buffer);
		tct = table;
	}

	wc=word_count(line);
	cmd = strip_commandname(line);
	inlen = strlen(cmd);

	if (start==0)
	{
		/* tab complete a command/script name */

		if (mode==0)
			matches=rl_completion_matches(text, list_commands);
		else if (mode==1)
			matches=rl_completion_matches(text, list_chat_commands);
		else
			matches = rl_completion_matches(text, list_bind_rl);
	}else if (mode==2)
	{
		/* scripts - could be anything - only help with user names */
		/* matches = rl_completion_matches(text, part_who_talk); */

		/* new for scripts - allow user to specify tab-completion */
		char *funcname;
		struct function *script;
		extern struct function *function_list;
		int t_c = 0;
		int i;

		/* check for function bind */
		if ((funcname = FindLinks(bind_list, cmd)) == NULL)
		{
			/* no bind match - no tab complete */
			return(matches);
		}

		script = function_list;
		while (script!=NULL && strcasecmp(funcname, script->name))
		{
			script=script->next;
		}

		/* remove the memory used by funcname */
		free(funcname);

		/* check the bound function exists */
		if (script == NULL)
		{
			/* no function - no tab complete */
			return(matches);
		}

		cl = script->complist;
		for (i=0; i<script->numcomp; i++)
		{
			if (cl[i].LArg == -1)
			{
				t_c = 1;
				break;
			}
			else if (wc < (cl[i].LArg+1))
			{
				t_c = 1;
				break;
			}
			else if (wc == (cl[i].LArg+1))
			{
				if (line[strlen(line)-1]!=' ')
				{
					t_c = 1;
					break;
				}
			}
		}

		/* if we can tab-complete - do so */
		if (t_c)
		{
			cl = script->complist;

			for(i=0;i<script->numcomp;i++)
			{
				int check=0;

				if (cl[i].FArg == -1)
				{
					check = 1;
				} else if (wc>cl[i].FArg || (wc==cl[i].FArg && text[0]==0))
				{
					check = 1;
				}

				/* allow 'NULL' function to mean 'skip arg' */
				if (check && cl[i].CPFunction != NULL)
				{
					matches=rl_completion_matches(text, cl[i].CPFunction);
					break;
				}
			}
		}
	}
	else if ((mode==0) || (mode==1))
	{
		/* not a command name, or script, so check number of arguments.
		   if not an argument we wish to tab-complete, then dont :) */
		int t_c = 0;

		if (cmd!=NULL)
		{
			cl = tctable;
			while (cl->Command && !t_c)
			{
				if (command_compare(cmd, cl->Command)==inlen)
				{
					if (cl->LArg == -1)
						t_c = 1;
					else if (wc < (cl->LArg+1))
					{
						t_c = 1;
					}
					else if (wc == (cl->LArg+1))
					{
						if (line[strlen(line)-1]!=' ')
						{
							t_c = 1;
						}
					}
					for (int i = 0; tct[i].Command != NULL; i++)
					{
						if (command_compare(cmd, tct[i].Command)==inlen)
						{
							if (tct[i].Rights!=0 && ((rights&tct[i].Rights)!=tct[i].Rights))
							{
								t_c = 0;
								break;
							}
						}
					}
				}
				cl++;
			}
		}

		/* board/talker commands */
		if (t_c)
		{
			cl = tctable;
			found = 0;

			while (cl->Command && !found)
			{
				if (cl->Mode == mode)
				{
					int check=0;

					if (cl->FArg == -1)
					{
						check = 1;
					} else if (wc>cl->FArg || (wc==cl->FArg && text[0]==0))
					{
						check = 1;
					}

					if (check && (command_compare(cmd, cl->Command)==inlen))
					{
						if (cl->CPFunction != NULL)
						{
							matches=rl_completion_matches(text, cl->CPFunction);
						}
						found = 1;
					}
				}
				cl++;
			}
		}
	}

	free(cmd);
	rl_attempted_completion_over=1;
	free(line);
	rights=oldrights;
	/* Append '/' instead of ' ' after a directory */
	rl_completion_append_character = ' ';
	if (rl_filename_completion_desired &&
	    matches && matches[0] && !matches[1])
	{
		char *match = matches[0];
		struct stat stats;
		char *path = strdup(match);

		(void)(*rl_directory_rewrite_hook)(&path);
		perms_drop();
		if (stat(path, &stats) == 0 &&
		    S_ISDIR(stats.st_mode))
		{
			if (match[strlen(match)-1] == '/')
				rl_completion_append_character = '\0';
			else
				rl_completion_append_character = '/';
		}
		perms_restore();
		free(path);
	}
	return(matches);
}

/* tab completion feature for board commands */
char *list_commands(const char *text, int state)
{
	static int i, len;
	const char *name;
	char *rtext;

	if (state==0)
	{
		i=0;
		len=strlen(text);
		if (cm_flags(user->chatmode,CM_ONCHAT,CM_MODE_ANY)) len--;
	}

	rtext=(char *)malloc(strlen(text)+1);
	if (cm_flags(user->chatmode,CM_ONCHAT,CM_MODE_ANY)) strcpy(rtext,&text[1]);
	else strcpy(rtext,text);

	while ((name=table[i].Command)!=NULL)
	{
		if (!strncasecmp(name, rtext, len) &&
		  ( table[i].Rights==0 || ((rights&table[i].Rights)==table[i].Rights)))
		{
			free(rtext);
			i++;
			return (dupstr(name, cm_flags(user->chatmode,CM_ONCHAT,CM_MODE_ANY)?CMD_BOARD_STR:""));
		}
		i++;
	}
	free(rtext);
	return(NULL);
}	

/* tab completion feature for talker commands */
char *list_chat_commands(const char *text, int state)
{
	static int i, len;
	const char *name;

	if (state==0)
	{
		i=0;
		len=strlen(text);
		len--;
	}

	text++;

	while ((name=chattable[i].Command)!=NULL)
	{
		if (!strncasecmp(name, text, len) &&
		  (chattable[i].Rights==0 || ((rights&chattable[i].Rights)==chattable[i].Rights)) &&
		  (chattable[i].Show == 1))
		{
			i++;
			return (dupstr(name, cm_flags(user->chatmode,CM_ONCHAT,CM_MODE_ANY)?CMD_TALK_STR:""));
		}
		i++;
	}
	return(NULL);
}	

char *find_folder(const char *text, int state)
{
	struct folder folstuff;
	struct folder *fol=&folstuff;

	static int file=0;
	static int len=0;

	if (state==0)
	{
		if (nofolders()) return(NULL);	
		if (file!=0) close(file);
		if ((file=openfolderfile(O_RDONLY))<0) return NULL;
		len=strlen(text);
	}

	while (get_folder_entry(file,fol))
	{
		if (f_active(fol->status) 
		   && (allowed_r(fol,user)
		   || allowed_w(fol,user))) /*allowed*/
		if (len==0 || !strncasecmp(text,fol->name,len))
			return (dupstr(fol->name, ""));
	}

	close(file);	
	file=0;
	return(NULL);
}


char *part_who(const char *text, int status)
{
	struct person uu;
	struct who w;

	static int ufile=0;
	static int wfile=0;
	static int len=0;

	if (status==0)
	{
		if (wfile!=0) close(wfile);
		if (ufile!=0) close(ufile);
		wfile=who_open(O_RDONLY);
		ufile=userdb_open(O_RDONLY);
		if (wfile<0 || ufile<0) return(NULL); /* whoops */

		len=strlen(text);
	}

	while (read(wfile,&w,sizeof(w)))
	{
		/* Skip invalid entries */
		if (w.posn < 0)
			continue;

		lseek(ufile,w.posn,0);
		read(ufile,&uu,sizeof(uu));

		if (len==0 || !strncasecmp(uu.name,text,len))
			return(dupstr(uu.name, ""));
	}

	close(wfile);
	close(ufile);
	return(NULL);
}

char *part_who_talk(const char *text, int status)
{
	struct person uu;
	struct who w;

	static int ufile=0;
	static int wfile=0;
	static int len=0;

	if (status==0)
	{
		if (wfile!=0) close(wfile);
		if (ufile!=0) close(ufile);
		wfile=who_open(O_RDONLY);
		ufile=userdb_open(O_RDONLY);
		if (wfile<0 || ufile<0) return(NULL); /* whoops */

		len=strlen(text);
	}

	while (read(wfile,&w,sizeof(w)))
	{
		/* Skip invalid entries */
		if (w.posn < 0)
			continue;

		lseek(ufile,w.posn,0);
		read(ufile,&uu,sizeof(uu));

		if (cm_flags(uu.chatmode, CM_ONCHAT, CM_MODE_ALL) &&
		   (len==0 || !strncasecmp(uu.name,text,len)))
			return(dupstr(uu.name, ""));
	}

	close(wfile);
	close(ufile);
	return(NULL);
}


char *part_user(const char *text, int status) 
{
	static int file=0;
	static int len=0;
	struct person usr;

	if (status==0)
	{
		if (file!=0) close(file);	
		file=userdb_open(O_RDONLY);
		len=strlen(text);
	}

	while (get_person(file,&usr))
	{
		if (!u_del(usr.status))
		{
			if (len==0 || !strncasecmp(usr.name, text, len))
				return (dupstr(usr.name, ""));
		}
	}
	close(file);
	return (NULL);
}

char *part_comm_user(const char *text, int status)
{
	static int ptr=0;
	static int len=0;
	extern char *partlist_user[];
	char *c;

	if (status==0)
	{
		ptr=0;
		len=strlen(text);
	}

	while (partlist_user[ptr]!=NULL)
	{
		if (len==0 || !strncasecmp(partlist_user[ptr], text, len))
		{
			c=dupstr(partlist_user[ptr],"");
			ptr++;
			return(c);
		}
		ptr++;
	}
	return(NULL);
}

char *part_comm_search(const char *text, int status)
{
	static int ptr=0;
	static int len=0;
	extern char *partlist_search[];
	char *c;

	if (status==0)
	{
		ptr=0;
		len=strlen(text);
	}

	while (partlist_search[ptr]!=NULL)
	{
		if (len==0 || !strncasecmp(partlist_search[ptr], text, len))
		{
			c=dupstr(partlist_search[ptr],"");
			ptr++;
			return(c);
		}
		ptr++;
	}
	return(NULL);
}

char *part_comm_folder(const char *text, int status)
{
	static int ptr=0;
	static int len=0;
	extern char *partlist_folder[];
	char *c;

	if (status==0)
	{
		ptr=0;
		len=strlen(text);
	}

	while (partlist_folder[ptr]!=NULL)
	{
		if (len==0 || !strncasecmp(partlist_folder[ptr], text, len))
		{
			c=dupstr(partlist_folder[ptr],"");
			ptr++;
			return(c);
		}
		ptr++;
	}
	return(NULL);
}

char *part_comm_mesg(const char *text, int status)
{
	static int ptr=0;
	static int len=0;
	extern char *partlist_mesg[];
	char *c;

	if (status==0)
	{
		ptr=0;
		len=strlen(text);
	}

	while (partlist_mesg[ptr]!=NULL)
	{
		if (len==0 || !strncasecmp(partlist_mesg[ptr], text, len))
		{
			c=dupstr(partlist_mesg[ptr],"");
			ptr++;
			return(c);
		}
		ptr++;
	}
	return(NULL);
}

void c_version(CommandList *cm, int argc, const char **argv, char *args)
{
	if (atoi(VER_TWK) > 0)
	printf(_("Version %s.%s.%s\n"), VER_MAJ, VER_MIN, VER_TWK);
	else
	printf(_("Version %s.%s\n"), VER_MAJ, VER_MIN);
	printf(_("Built by %s on %s\n"), BUILD_USER, __DATE__);
}

void devel_msg(const char *func, const char *fmt, ...)
{
	va_list va;
	char text[MAXTEXTLENGTH];

	va_start(va, fmt);
	vsnprintf(text, MAXTEXTLENGTH-1, fmt, va);
	va_end(va);

	if (cp_flags(user->chatprivs,CP_DEVEL,CM_MODE_ANY)) printf("\n*** WARNING (%s): %s\n", func, text);
}


void broadcast_onoffcode(int ocode, int method, const char *sourceuser, const char *reason)
{
	char		logofftext[MAXTEXTLENGTH];
	extern int	talker_logontype;

	/* create the broadcast string */
	snprintf(logofftext, MAXTEXTLENGTH-1, "%d,%d,%d,%s", ocode, method, talker_logontype & LOGONMASK_QUIET, (sourceuser)?(sourceuser):(user->name));

	/* add an optional reason */
	if (reason != NULL)
	{
		char	reasontext[MAXTEXTLENGTH];
		snprintf(reasontext, MAXTEXTLENGTH - 1, "%s,%s", logofftext, reason);
		snprintf(logofftext, MAXTEXTLENGTH - 1, "%s", reasontext);
	}

	/* send the message */
	ipc_send_to_all(IPC_CHECKONOFF, logofftext);
}
