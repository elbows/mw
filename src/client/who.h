#ifndef CLIENT_WHO_H
#define CLIENT_WHO_H

#include <stdint.h>

void check_copies(int32_t where);
void who_list(int mode);
void what_list(void);
char *itime(unsigned long t);
int32_t get_who_userposn(int pid);

#endif /* CLIENT_WHO_H */
