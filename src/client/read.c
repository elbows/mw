/*********************************************************
 *     The Milliways III System is copyright 1992        *
 *      J.S.Mitchell. (arthur@sugalaxy.swan.ac.uk)       *
 *       see licence for furthur information.            *
 *********************************************************/

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>
#include "strings.h"
#include "str_util.h"
#include "perms.h"
#include "read.h"
#include "talker.h"
#include "echo.h"
#include "util.h"
#include "bb.h"
#include "userio.h"

extern int remote;
extern FILE *output;

int get_data(int afile, struct Header *tmp)
{
	int no;
	no=read(afile,tmp,sizeof(*tmp));
	if (!no) return(false); else
	if (no<sizeof(*tmp))
	{
		printf("Failed to read entire record. Folder Damaged.");
		perror("read");
		return(0);
	}
	return(true);
}
	
void display_article(struct Header *tmp, int datafile)
{
	int i,length;
	char *buff,*ptr;
	char title[80];
	time_t when;
	
	buff=(char *)malloc(tmp->size);

	if (!remote)
	{
		when = tmp->date;
		sprintf(title,"Message %d from %s on %s",
		       tmp->Ref,tmp->from,ctime(&when));
		if ((ptr=(char *)strchr(title,'\n'))!=NULL) *ptr='.';
		fprintf(output,"\n%s\n",title);
		length=strlen(title);
		for (i=0;i<length;i++) fputc('-',output);
		fprintf(output,"\n");
	}else
	{
		printf("Message: %d\nFrom: %s\nDate: %ld\n",tmp->Ref,tmp->from,(long)tmp->date);
		show_mesg_stats(tmp->status,title,false);
		printf("Flags: %s\n",title);
	}	
	fprintf(output,"To: %s\nSubject: %s\n",tmp->to,tmp->subject);
	
	if (tmp->replyto>0)
		fprintf(output,"In reply to message %d.\n",tmp->replyto);
	if (tmp->status&(1<<2))
		fprintf(output,"Message has a reply.\n");
	fprintf(output,"\n");
	lseek(datafile,tmp->datafield,0);
	if (read(datafile,buff,tmp->size)<tmp->size)
	{
		printf("Failed to read entire data entry");
		perror("read data");
		return;
	}
	if (!remote)
	{
		if (output!=stdout) fprintf(output,"%s",buff);
		else
		more(buff,tmp->size);
		printf("\n");
	}else
	{
		char *p;
		p=buff;
		while (*p)
		{
			putchar(*p);
			if (*p=='\n' && p[1]=='.') putchar('.');
			p++;
		}	
		printf("\n.\n");
	}
	free(buff);
}

int read_msg(int folnum, int msgnum, struct person *user)
{
	int file,headfile,textfile,i;
	struct folder data;
	struct Header head;
	int posn;
	long tmppos;
	char *x;	

	if (nofolders())
		{printf("There are no folders to read.\n");return(false);}

	file=openfolderfile(O_RDONLY);
	memset(&data, 0, sizeof(struct folder));
	for (i=0;i<=folnum;i++) get_folder_entry(file,&data);

	close(file);
	
	if (!(data.status&1))
		{printf("That folder does not exist.\n");return(false);}
	if (msgnum>data.last || msgnum<data.first)
		{printf("There is no message %d.\n",msgnum);return(false);}
	if (data.last<data.first || data.last==0)
		{printf("Folder %s is empty.\n",data.name);return(false);}
	if ( !u_god(user->status) && !allowed_r(&data,user))
		{printf("You are not permitted to read this folder.\n");return(false);}
		
	x=buildpath(STATEDIR,data.name,INDEX_END,"");
	if (access(x,0)) 
	{
		printf("Error: cannot find index file for folder %s\n",data.name);
		return(false);
	}
	if ((headfile=err_open(x,O_RDONLY,0))<0)
		{exit(-1);}

	x=buildpath(STATEDIR,data.name,TEXT_END,"");
	if (access(x,0)) 
	{
		printf("Error: cannot find text file for folder %s\n",data.name);
		close(headfile);
		return(false);
	}
	if ((textfile=err_open(x,O_RDONLY,0))<0)
		{exit(-1);}
	tmppos=lseek(headfile,0,2);
	posn=(msgnum - data.last - 1)*sizeof(struct Header);
	if (posn < -tmppos)
		lseek(headfile, 0, 0);
	else
		lseek(headfile, posn, 1);
	while (get_data(headfile,&head) && head.Ref<msgnum);
	if (head.Ref==msgnum)
	{
		if (u_god(user->status))
			display_article(&head,textfile);
		else
		if (head.status&(1<<1))
		{
			printf("Message has been marked for deletion.\n");
		}else
		if (is_private(&data,user) && 
		  (!stringcmp(head.to,user->name,-1)&&!stringcmp(head.from,user->name,-1)))
		{
			printf("This message is not addressed to you !\n");
		}else
			display_article(&head,textfile);
	}
	else
	{
		printf("Message %d does not exist.\n",msgnum);
	}
	close(headfile);
	close(textfile);
	return(true);
}

void ls(int folnum, struct person *user, int many)
{
	int afile;
	struct folder fold;
	struct Header head;
	char buff[SUBJECTSIZE+1];	
	int linecount=0;
	int listpoint;
	int screen_height = screen_h();

	if (!get_folder_number(&fold,folnum))
		return;

	AUTOFREE_BUFFER tmp;
	if (asprintf(&tmp, "%s/%s%s", STATEDIR, fold.name, INDEX_END) < 0)
	{
		return;
	}
	if ((afile=open(tmp,O_RDONLY))<0)
	{
		printf("There are no messages in folder %s\n",fold.name);
		return;
	}
	if (many>0)
	{
		listpoint=fold.first+many;
		if (listpoint>fold.last) listpoint=fold.last;
	}else
	if (many<0)
	{
		listpoint=fold.last+many;
		if (listpoint<fold.first) listpoint=fold.first;
	}else
		listpoint=fold.first;
	printf("Listing messages in folder %s\n",fold.name);
	printf(" Msg. %*s -> %*s  %-*s\n",NAMESIZE,"From",NAMESIZE,"To",SUBJECTSIZE,"Subject");
	while (get_data(afile,&head))
	{
		if (((many>0 && head.Ref<listpoint) || many==0
		  ||(many<0 && head.Ref>listpoint) ) && (head.Ref <= fold.last))
		if (!(head.status&(1<<1))
		&& (!is_private(&fold,user)|| u_god(user->status) ||
		(is_private(&fold,user)&&(stringcmp(head.from,user->name,-1)
		                     ||stringcmp(head.to,user->name,-1))))) /*marked for deletion*/
		{
			strncpy(buff,head.to,NAMESIZE);
			buff[NAMESIZE]=0; 
			printf("%4d  %*s -> %*s  ",
			head.Ref,NAMESIZE,head.from,NAMESIZE,buff);
			if (strlen(head.subject)>40)
			{
				strncpy(buff,head.subject,37);
				strcat(buff,"...");
			}else
				strcpy(buff,head.subject);
			printf("%s\n",buff);
			linecount++;
			if (linecount>=(screen_height-1))
			{
				char foo[6];
				printf("---more---\r");
				echo_off();
				get_str(foo,5);
				echo_on();
				if (foo[0]=='q' || foo[0]=='Q')
					break;
				printf("            \r");
				linecount=0;
			}
				
		}
	}
	close(afile); 
}

void more(char *buff, int size)
{
	char *ptr;
	int len=0,found=5;
	char tmp[2];
	int scr_h = screen_h();
	
	ptr=buff;
	while ((ptr+len)<(buff+size))
	{
		/* go through string till EOS, or until max allowed lines per 'more' */
		do {
			if (ptr[len]=='\n') found++;
			len++;
		} while ((ptr+len)<(buff+size) && found<=(scr_h-4));

		/* write string to screen */
		fwrite(ptr,len,1,stdout);

		/* if max allowed lines reached, then 'more' and wait for key */
		if (found>=(scr_h-4))
		{
			printf("--More--\r");
			echo_off();
			get_str(tmp,1);
			echo_on();
			if (tmp[0]=='q' || tmp[0]=='Q') return;
			printf("         \r");
			ptr+=len;
			len=found=0;
		}
	}
}

/* returns true if okay, false if failed for some reason */
int get_mesg_header(struct folder *data, int msgnum, struct Header *head)
{
	int		headfile;
	int		posn;
	long		tmppos;
	char		*x;	

	/* folder data doesnt exist */
	if (!data) return(false);
	if (!(data->status & 1)) return(false);

	/* no message of that number */
	if (msgnum > data->last || msgnum < data->first) return(false);

	/* empty folder */
	if (data->last < data->first || data->last == 0) return(false);

	x=buildpath(STATEDIR,data->name,INDEX_END,"");
	/* no data file */
	if (access(x,0)) return(false);
	/* cant open file */
	if ((headfile=err_open(x,O_RDONLY,0))<0) return(false);

	/* no message - file too small */
	tmppos = lseek(headfile, 0, 2);

	/* locate to file data */
	posn=(msgnum - data->last - 1)*sizeof(struct Header);
	if (posn < -tmppos)
		lseek(headfile, 0, 0);
	else
		lseek(headfile, posn, 1);

	/* read it */
	while (get_data(headfile, head) && head->Ref < msgnum);
	close(headfile);

	/* if we have the correct message, return true */
	return(head->Ref == msgnum);
}
