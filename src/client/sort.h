#ifndef SORT_H
#define SORT_H

#include "user.h"

struct listing
{
	char name[NAMESIZE+1];
	int count;
	int size; 
	struct listing *next;
};

struct listing *Sort(struct listing *head);

#endif /* SORT_H */
