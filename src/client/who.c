/*********************************************************
 *     The Milliways III System is copyright 1992        *
 *      J.S.Mitchell. (arthur@sugalaxy.swan.ac.uk)       *
 *       see licence for furthur information.            *
 *********************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <fcntl.h>
#include <time.h>
#include <stdbool.h>

#include "talker_privs.h"
#include "special.h"
#include "strings.h"
#include "str_util.h"
#include "ipc.h"
#include "perms.h"
#include "talker.h"
#include "userio.h"
#include "who.h"
#include "main.h"

extern int busy;
extern struct person *user;
extern struct room myroom;

/* called only by cmdline flags */
void what_list(void) {
	struct person	u;
	int		ufile;
	time_t		t;
	char *		timestr;
	
	ufile=userdb_open(O_RDONLY);
	if (ufile < 0) return; /* whoops */
	
	while (read(ufile, &u, sizeof(u))) {
		if (! u_del(u.status)) {
			t = (time_t) u.dowhen;
			timestr = ctime(&t);
			timestr[strlen(timestr) - 1] = 0;	// Strip the \n
			if (u.dowhen && u.doing[0] != 0) printf("%s | %s %s (%s ago)\n", timestr, u.name, u.doing, itime(time(0)-u.dowhen));
		}
	}
	close(ufile);
}

void who_list(int mode)
{
	struct person u;
	struct who w;
	int ufile,wfile;
	char stats[10];
	char chat;
	int wizchat;
	int wiz;
	char *divider;

	if (u_god(user->status)) wiz=1; else wiz=0;
	busy++;

	wfile=who_open(O_RDWR);
	ufile=userdb_open(O_RDONLY);
	if (wfile<0 || ufile<0) return; /* whoops */

	/* do we tell them who can hear wiz messages */
	if (s_wizchat(user->special)) wizchat=1; else wizchat=0;

	if (mode==1) 
		format_message("\nUser status...");
	else
	if (wiz) format_message("\nPID    %-*s %-*s  Idle  What...",NAMESIZE,"Name",20,"RealName");
	else format_message("\n %-*s Idle   What...",NAMESIZE,"Name");

	divider = NULL;
	for (int i = 0; i < screen_w(); i++) string_add(&divider, "-");

	format_message("%s", divider);

	while (read(wfile,&w,sizeof(w)))
	{
	    	/* Skip invalid entries */
	    	if (w.posn < 0)
		    	continue;

		lseek(ufile,w.posn,0);
		read(ufile,&u,sizeof(u));

		show_user_stats(u.status,stats,true);

		/* can they hear wiz messages ? */
		if (s_wizchat(u.special) && !s_chatoff(u.special))
			chat='*';
		else
			chat=' ';
	
		/* check they are still alive and show info */
		{
			if (mode == 1) {
				if (u.dowhen && u.doing[0] != 0)
				format_message("%s %s\033-- (%s ago)",u.name, u.doing ,itime(time(0)-u.dowhen));
			} else {
			if (cm_flags(u.chatmode,CM_ONCHAT,CM_MODE_ANY))
			{
				int cml, cpl, pll, myapl;
				char *cmt, *cpt, plt[8];
				int off;
				unsigned long mask;
	
				myapl = (user->chatprivs & CP_PROTMASK) >> CP_PROTSHIFT;
				mask=u.chatmode & 0xfffffffe;
				if (!cp_flags(user->chatprivs,CP_CANGLOBAL,CM_MODE_ALL)) mask&= ~(CM_GLOBAL);
				if (!cp_flags(user->chatprivs,CP_SPY,CM_MODE_ALL)) mask&= ~(CM_SPY);

				if (myapl > 0 && cm_flags(user->chatprivs, CP_PROTECT, CM_MODE_ALL)) /* replaced with better info */
					mask&= ~(CM_PROTECTED);
				else if (u.chatmode & CM_PROTMASK)
					mask |= CM_PROTECTED; /* looks like temp protect */

				snprintf(u.doing,DOINGSIZE, "Room %d",u.room);

				off=strlen(u.doing);
				snprintf(&u.doing[off],DOINGSIZE-off," (");
				/* Show chatmode flags */
				cmt=display_cmflags(mask);
				cml=strlen(display_cmflags(mask));
				off=strlen(u.doing);
				if (strlen(cmt)>0)
				{
					snprintf(&u.doing[off],DOINGSIZE-off,"%-*s", cml, cmt);
				}
				/* Show protection */
				if (myapl > 0 && cm_flags(user->chatprivs, CP_PROTECT, CM_MODE_ALL))
				{
					show_protection(u.chatmode, u.chatprivs, plt, myapl);
					pll=strlen(plt);
					if (plt[0] == '0' && cm_flags(u.chatmode, CM_PROTECTED, CM_MODE_ALL))
						plt[0]='p';
					if (plt[2] == '0' && cm_flags(u.chatprivs, CP_PROTECT, CM_MODE_ALL))
						plt[2]='P';

					off=strlen(u.doing);
					if (strlen(cmt)>0)
					{
						snprintf(&u.doing[off],DOINGSIZE-off,",");
					}
					off=strlen(u.doing);
					snprintf(&u.doing[off],DOINGSIZE-off,"%-*s", pll, plt);
				} else
					plt[0] = '\0';
				/* Show chatpriv flags */
				if (myapl > 0 && cm_flags(user->chatprivs, CP_PROTECT, CM_MODE_ALL)) /* 'P' priv replaced with better info */
					cpt=display_cpflags(u.chatprivs & user->chatprivs & ~CP_PROTECT);
				else
					cpt=display_cpflags(u.chatprivs & user->chatprivs);
				off=strlen(u.doing);
				cpl=strlen(cpt);
				if ((strlen(cmt)+strlen(plt))>0 && cpl>0)
				{
					snprintf(&u.doing[off],DOINGSIZE-off,",");
				}
				off=strlen(u.doing);
				if (cpl>0)
				{
					snprintf(&u.doing[off],DOINGSIZE-off,"%-*s", cpl, cpt);
				}

				off=strlen(u.doing);
				snprintf(&u.doing[off],DOINGSIZE-off,")");
			}

			if (wiz)
			{
				format_message("%-5d %c%-*s %-20.20s %6s %s\033--",w.pid,chat,NAMESIZE,u.name,u.realname,itime(time(0)-u.idletime),u.doing);
			}else
			{
				format_message("%c%-*s %6s %s\033--",wizchat?chat:' ',NAMESIZE,u.name,itime(time(0)-u.idletime),u.doing);
			}
			}
		}
	}
	format_message("%s", divider);
	close(wfile);
	close(ufile);

	free(divider);

	busy--;
}

int32_t get_who_userposn(int pid)
{
	struct who w;
	int wfile;
	long found=-1;
	
	if ((wfile=who_open(O_RDONLY))<0) return(-1);

	while (read(wfile,&w,sizeof(w)))
	{
		if (w.posn >= 0 &&
		    (pid==-w.pid || pid==w.pid ))
		{
			found=w.posn;
			break;
		}
	}
	close(wfile);
	
	return(found);
}

#ifdef RWHO
void rwho_update(void)
{
	static int setup=0;
	static long t=0;

	if (!setup)
	{
		setup=1;
		rwhocli_setup("137.44.12.4","frodperfect","Milliways","THE Bulletin Board");
	}
	if (time(0)-t>240)
	{
		t=time(0);
		rwhocli_pingalive();
		rwho_list();
	}
}

static void rwho_list(void)
{
	struct person user;
	struct who w;
	int ufile,wfile;
	
	wfile=openwhofile(O_RDWR);
	ufile=openuserfile(O_RDONLY);

	if (wfile<0 || ufile<0) return; /* whoops */	
	while (read(wfile,&w,sizeof(w))>0)
	{
		if (w.posn!=-1L)
		{
			lseek(ufile,w.posn,0);
			read(ufile,&user,sizeof(user));

			if (w.pid>-1)
			{
				rwhocli_userlogin(user.name,user.name,user.lastlogout);
			}else
			{
				rwhocli_userlogout(user.name);
			}
		}
	}
	close(wfile);
	close(ufile);
}

#endif

void check_copies(int32_t where)
{
	struct who u;
	int file;
	int count;
	char buff[5];

	file=who_open(O_RDWR);

	if (file<0) return; /* whoops */
	count=0;
	lseek(file,0L,0);
	while(read(file,&u,sizeof(u))>0)
	{
		if (u.pid>0 && u.posn==where) {
			count++;
		}
	}

	if (count==1)
	{
		printf("You are already logged in.\n");
	}else
	if (count>1)
	{
		printf("You are already logged in %d times.\n",count);
	}
	if (count>0)
	{
		printf("Do you want to kill those sessions ?");
		get_str(buff,4);
		if (BoolOpt(buff)>0)
		{
			printf("Killing... ");
			lseek(file,0L,0);
			while(read(file,&u,sizeof(u))>0)
				if (u.pid>0 && u.posn==where)
					kill(u.pid,SIGTERM);
			printf("Done.\n");
		}
	}
	close(file);
}

char *itime(unsigned long t)
{
	static char out[7];

	if (t>=3600*48) sprintf(out,"%2ldd%2ldh",t/86400, (t%86400)/3600);
	else
	if (t>=3600) sprintf(out,"%2ldh%2ldm",t/3600,(t%3600)/60);
	else
	if (t>=60) sprintf(out,"%2ldm%2lds",t/60,t%60);
	else
	sprintf(out,"%5lds",t);
	return(out);
}
