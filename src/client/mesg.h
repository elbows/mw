#ifndef MESG_H
#define MESG_H

#include <mesg.h>

void broadcast(int state, const char *fmt, ...) __attribute__((format(printf,2,3)));
void inform_of_mail(char *to);
void postinfo(struct person *who, struct folder *fol, struct Header *mesg);
void send_mesg(char *from, const char *to, char *text, int wiz);

#endif /* MESG_H */
