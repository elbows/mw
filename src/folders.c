#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "strings.h"
#include "folders.h"
#include "files.h"

int openfolderfile(int mode)
{
	int x;
	x=open(FOLDERFILE,mode);
	if (x<0)
	{
		perror("Open Folder File");
	}
	return(x);
}

int nofolders(void)
{
	int fd;
	struct folder fol;

	fd = open(FOLDERFILE, O_RDONLY);
	if (fd < 0) return 1;

	if (read(fd,&fol,sizeof(struct folder)) < sizeof(struct folder))
	{
		close(fd);
		return 1;
	}

	close(fd);
	return 0;
}

void create_folder_file(void)
{
	int file,i;
	struct folder *record;
	
	record=(struct folder *)malloc(sizeof(*record));
	record->status=0;
	file=open(FOLDERFILE,O_WRONLY|O_CREAT,0600);
	Lock_File(file);
	for (i=0;i<64;i++) 
		if (write(file,record,sizeof(*record))<0)
		{
			perror("creating blank folders");
			exit(-1);
		}
	Unlock_File(file);
	close(file);
	free(record);
}

int foldernumber(const char *name)
{
	/* return number of folder name */
	int file;
	int number=0;
	int no;
	struct folder fold;
	size_t len = strlen(name);

	if (len == 0) return -1;
	if ((file=openfolderfile(O_RDONLY)) < 0) return -1;
	do{
		no=get_folder_entry(file,&fold);
		number++;
	}while (no!=0 && strncasecmp(name,fold.name,len));
	close(file);
	
	if (no==0) return(-1); else return(number-1);
}

int get_folder_entry(int file, struct folder *tmp)
{
	int no;
	if ((no=read(file,tmp,sizeof(*tmp)))<0)
	{
		perror("get_folder_entry");
		exit(-1);
	}
	return(no);
}

int get_folder_number(struct folder *fol, int num)
{
	int file;

	if (nofolders())
		{printf("There are no folders !\n");return 0;}
	if ((file=openfolderfile(O_RDONLY)) < 0) return 0;
	lseek(file,sizeof(*fol)*num,0);
	if (read(file,fol,sizeof(*fol))<0)
	{
		perror("get_folder_number");
		return 0;
	}
	close(file);
	return 1;
}
