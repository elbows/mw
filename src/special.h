#ifndef SPECIAL_H
#define SPECIAL_H

unsigned short set_special(const char *string, unsigned short stat);
void show_special(unsigned short stat, char *tmp, int flag);
int s_coventry(unsigned short stat);
int s_superuser(unsigned short stat);
int s_wizchat(unsigned short stat);
int s_chatoff(unsigned short stat);
int s_timeout(unsigned short stat);
int s_timestamp(unsigned short stat);
int s_postinfo(unsigned short stat);
int s_colouroff(unsigned short stat);
int s_changeinfo(unsigned short stat);
int s_test(unsigned short stat);
int s_quiet(unsigned short stat);
int s_tcunames(unsigned short stat);
int s_devel(unsigned short stat);
int s_fixedcontact(unsigned short stat);
int s_nolinewrap(unsigned short stat);

enum {
	MWSPCL_COVENTRY    =  0,
	MWSPCL_SUPER,
	MWSPCL_WIZCHAT,
	MWSPCL_WIZCHATOFF,
	MWSPCL_TIMEOUT,
	MWSPCL_TIMESTAMP,
	MWSPCL_POSTINFO,
	MWSPCL_UNUSED_7,
	MWSPCL_COLOUROFF,
	MWSPCL_CHGINFO,
	MWSPCL_UNUSED_10,
	MWSPCL_QUIET,
	MWSPCL_TCUNAMES,
	MWSPCL_DEVEL,
	MWSPCL_FIXEDCONTACT,
	MWSPCL_NOWRAP,
	/* Dummy for limit checking */
	MWSPCL_SIZE
};

#define s_coventry(stat)      (stat & (1<<MWSPCL_COVENTRY))
#define s_superuser(stat)     (stat & (1<<MWSPCL_SUPER))
#define s_wizchat(stat)       (stat & (1<<MWSPCL_WIZCHAT))
#define s_chatoff(stat)       (stat & (1<<MWSPCL_WIZCHATOFF))
#define s_timeout(stat)       (stat & (1<<MWSPCL_TIMEOUT))
#define s_timestamp(stat)     (stat & (1<<MWSPCL_TIMESTAMP))
#define s_postinfo(stat)      (stat & (1<<MWSPCL_POSTINFO))
#define s_colouroff(stat)     (stat & (1<<MWSPCL_COLOUROFF))
#define s_changeinfo(stat)    (stat & (1<<MWSPCL_CHGINFO))
#define s_quiet(stat)         (stat & (1<<MWSPCL_QUIET))
#define s_tcunames(stat)      (stat & (1<<MWSPCL_TCUNAMES))
#define s_devel(stat)         (stat & (1<<MWSPCL_DEVEL))
#define s_fixedcontact(stat)  (stat & (1<<MWSPCL_FIXEDCONTACT))
#define s_nolinewrap(stat)    (stat & (1<<MWSPCL_NOWRAP))
 
#endif /* SPECIAL_H */
