#ifndef PERMS_H
#define PERMS_H

#include "folders.h"
#include "user.h"

void mwlog(const char *fmt, ...) __attribute__((format(printf,1,2)));

char user_stats(const char *string, char stat);
char mesg_stats(char *string, char stat);

void show_user_stats(unsigned char stat, char *tmp, int flag);
void show_mesg_stats(unsigned char stat, char *tmp, int flag);
void show_fold_groups(char stat, char *tmp, int flag);
void show_fold_stats(char stat, char *tmp, int flag);

int allowed_r(struct folder *fol, struct person *usr);
int allowed_w(struct folder *fol, struct person *usr);

int is_private(struct folder *fol, struct person *usr);
int is_moderated(struct folder *fol, struct person *usr);

char folder_stats(char *string, char stat);
char folder_groups(const char *string, char stat);

void set_subscribe(struct person *user,int folder,int status);
int get_subscribe(struct person *user, int folder);

#define MWUSR_REGD     0
#define MWUSR_MOD      1
#define MWUSR_SUPER    2
#define MWUSR_BANNED   3
#define MWUSR_MESG     4
#define MWUSR_INFORM   5
#define MWUSR_BEEPS    6
#define MWUSR_DELETED  7
#define MWUSR_SIZE     8 /* Dummy for limit checking */

#define u_ban(user)    (user & (1 << MWUSR_BANNED))
#define u_del(user)    (user & (1 << MWUSR_DELETED))
#define u_god(user)    (user & (1 << MWUSR_SUPER))
#define u_inform(user) (user & (1 << MWUSR_INFORM))
#define u_mesg(user)   (user & (1 << MWUSR_MESG))
#define u_beep(user)   (user & (1 << MWUSR_BEEPS))
#define u_mod(user)    (user & (1 << MWUSR_MOD))
#define u_reg(user)    (user & (1 << MWUSR_REGD))

#define MWFOLDR_ACTIVE    0
#define MWFOLDR_RUNREG    1
#define MWFOLDR_WUNREG    2
#define MWFOLDR_RREG      3
#define MWFOLDR_WREG      4
#define MWFOLDR_PRIVATE   5
#define MWFOLDR_MODERATED 6
#define MWFOLDR_SIZE      7 /* Dummy for limit checking */

#define f_active(stat)    (stat & (1 << MWFOLDR_ACTIVE))
#define f_r_unreg(stat)   (stat & (1 << MWFOLDR_RUNREG))
#define f_w_unreg(stat)   (stat & (1 << MWFOLDR_WUNREG))
#define f_r_reg(stat)     (stat & (1 << MWFOLDR_RREG))
#define f_w_reg(stat)     (stat & (1 << MWFOLDR_WREG))
#define f_private(stat)   (stat & (1 << MWFOLDR_PRIVATE))
#define f_moderated(stat) (stat & (1 << MWFOLDR_MODERATED))

#endif
