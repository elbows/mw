#ifndef SQLITE_H
#define SQLITE_H

/* Functions to access and interact with SQLite */

struct db_data {
	char **field;
	int row;
	struct db_data *next;
};

struct db_result {
	int rows;
	int cols;
	char **colNames;
	struct db_data *data;
};

struct js_db_result {
	struct db_result *query_result;
	int db_error;
	int query_error;
	char *error_text;
};

#define USERDB_PRIVATE	"private"
#define USERDB_PUBLIC	"public"
#define USERDB_ROOMS	"rooms"
#define USERSQL STATEDIR"/users.db"

struct db_result *db_query(const char *dbname, const char *query, int quiet);
void db_free(struct db_result *result);
char * db_item(struct db_result *result, int row, const char *field);
char * db_room_get(int room, const char *opt);
void db_room_set(int room, const char *opt, const char *arg);

#endif /* SQLITE_H */
