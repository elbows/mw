#ifndef WHO_H
#define WHO_H

#include <stdint.h>

#define WHOFILE MSGDIR"/who.bb"

struct who 
{
	int32_t pid;
	int32_t posn;
};

extern int who_open(int mode);
extern void who_add(int wfd, int pid, int32_t posn);
extern void who_delete(int wfd, int pid);
extern long who_find(const char * username);


#endif /* WHO_H */
