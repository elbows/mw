#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "user.h"
#include "files.h"

int userdb_open(int flags)
{
	int ufd = open(USERFILE, flags|O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP);
	if (ufd < 0) {
		perror(USERFILE);
		exit(-1);
	}
	return ufd;
}

void userdb_write(struct person *record, int32_t *userposn)
{
	int outfile = userdb_open(O_WRONLY|O_APPEND|O_CREAT);
	if (outfile < 0)
		exit(-1);
	Lock_File(outfile);
	write(outfile,record,sizeof(*record));
	*userposn = (lseek(outfile,0,SEEK_CUR) - sizeof(*record));
	Unlock_File(outfile);
	close(outfile);
}

void fetch_user(struct person *record, int32_t userposn)
{       
        int outfile;
        
        outfile=userdb_open(O_RDWR|O_CREAT);
        lseek(outfile,userposn,0);
        read(outfile,record,sizeof(*record));
        close(outfile);
}       

