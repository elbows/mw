#ifndef BB_H
#define BB_H

/* bb.h
 * Miscellaneous structures/constants.
 */


#ifndef HOMEPATH
#define HOMEPATH	"/usr/local/lib/mw"
#endif

#ifndef LOGDIR
#define LOGDIR		"/var/log/mw"
#endif

#ifndef STATEDIR
#define STATEDIR	"/var/lib/mw"
#endif

#ifndef MSGDIR
#define MSGDIR		"/run/mw"
#endif

#define LOGFILE		LOGDIR"/log.bb"
#define LOGIN_BANNER	"login.banner"
#define LOCKFILE	"/tmp/bb.locked"
#define EDITOR		"/usr/bin/vim"
#define SECUREEDITOR	"/bin/rnano"

#define HELPDIR		"help"
#define WIZHELP		"wizhelp"
#define SCRIPTHELP	"scripthelp"
#define TALKHELP	"talkhelp"

#define TEXT_END	".t"
#define INDEX_END	".i"
#define MOD_END		".m"
#define PATHSIZE	256

#define CMD_BOARD_STR	"!"
#define CMD_TALK_STR	"."
#define CMD_SCRIPT_STR	","

#define LOGLINESIZE	2048	/* size of a line printed to log file */
#define MAXTEXTLENGTH	2048	/* text buffer size */
#define MAXPIPELENGTH	4096	/* maximum length of a pipe message */

#define RUNAWAY		1200
#define MAXRUNAWAY	5000
#define FLOODLIMIT	15

#define RIGHTS_BBS	0
#define RIGHTS_TALK	1


struct IgnoreList
{
	char			*name;
	struct IgnoreList	*next;
};
                
#define PACKAGE	"mw"
#define LOCALEDIR HOMEPATH"/locale"

#endif /* BB_H */
