#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>

#include <bb.h>
#include <files.h>
#include <perms.h>

int internet=0;
struct person user; 

int main(void)
{
	const char *newpath = USERFILE ".new";
	int newfile,oldfile;
	long tt=0l;
	
	if ((oldfile=open(USERFILE,O_RDONLY))<0)
	{
		perror(USERFILE);
		exit(1);
	}
	if ((newfile=open(newpath,O_WRONLY|O_CREAT,0600))<0)
	{
		perror(newpath);
		exit(1);
	}

	while(read(oldfile,&user,sizeof(user))>0)
	{
		if (user.status&(1<<7) || user.timeused<900 || user.lastlogout<=tt)
		{
			printf("Deleting %s, %s\n",user.name, user.realname);
		}else
		{
			time_t when = user.lastlogout;
			char *line = NULL;
			size_t len = 0;
			char buff[20];
			printf("Name: %s\tRname: %s\n",user.name,user.realname);
			printf("Contact: %s\n",user.contact);
			show_user_stats(user.status,buff,1);
			printf("Status [%s]\n",buff);
			printf("Lastlogin %s", ctime(&when));
			printf("\nDelete(y/n)?");
			while ((getline(&line, &len, stdin) < 1) || line == NULL);
			if (*line != 'y')
			{
				write(newfile,&user,sizeof(user));
			}
			if (*line == 't') tt = user.lastlogout;
		}
	}
	close(newfile);
	close(oldfile);
}
