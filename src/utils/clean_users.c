#include <stdio.h>
#include <fcntl.h>
#include "../bb.h"
int internet=0;
struct person user; 

main()
{
	char oldpath[1024];
	char newpath[1024];
	int newfile,oldfile;
	char buff[20];
	long tt=0l;
	
	sprintf(oldpath,"%s/%s",HOMEPATH,USERFILE);
	sprintf(newpath,"%s.new",oldpath);

	if ((oldfile=open(oldpath,O_RDONLY))<0)
	{
		perror(oldpath);
		exit(0);
	}
	if ((newfile=open(newpath,O_WRONLY|O_CREAT,0600))<0)
	{
		perror(newpath);
		exit(0);
	}

	tt=time(0);
	tt-=3600*24*180;   /* 6 months */
	while(read(oldfile,&user,sizeof(user))>0)
	{
		if ( user.status&(1<<7)	// deleted
		|| !(user.status&1) // not registered
		|| user.timeused<900 	// hardly used (15min)
		|| user.lastlogin<=tt )	// not used recently (6mon)
		{
			printf("Deleting %s, %s\n",user.name, user.realname);
		}else
		{
			printf("%16s (%30s) %s",user.name, user.realname, ctime(&user.lastlogin));
			write(newfile,&user,sizeof(user));
		}
	}
	close(newfile);
	close(oldfile);
}
