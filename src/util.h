#ifndef UTIL_H
#define UTIL_H

#include <stdlib.h>

/* TR's auto-free asprintf type stuff */
__attribute__((unused)) static inline void cleanup_buffer(void *buffer)
{
	char **b = buffer;
	if (NULL != *b)
	{
		free(*b);
	}
}
#define AUTOFREE_BUFFER __attribute__((cleanup(cleanup_buffer))) char *


#define SAFE_FREE(a)	do { if (a) { free(a); (a) = NULL; } } while(0);


#endif /* UTIL_H */
