#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <strings.h>

#include "who.h"
#include "files.h"
#include "user.h"

int who_open(int flags)
{
	int wfd = open(WHOFILE, flags|O_CREAT, 0644);
	if (wfd < 0)
		perror(WHOFILE);
	return wfd;
}

void who_add(int wfd, int pid, int32_t posn)
{
	struct who rec;
	Lock_File(wfd);
	while(read(wfd,&rec,sizeof(rec))==sizeof(rec))
	{
		if(rec.pid<0)
		{
			lseek(wfd,-sizeof(rec), SEEK_CUR);
			break;
		}
	}
	rec.pid=pid;
	rec.posn=posn;
	write(wfd,&rec,sizeof(rec));
	Unlock_File(wfd);
}

void who_delete(int wfd, int pid)
{
	struct who temp;
	Lock_File(wfd);
	while(read(wfd,&temp,sizeof(temp))==sizeof(temp))
	{
		if (temp.pid==pid) {
			lseek(wfd,(long)-sizeof(temp),1);
			temp.pid=-1;
			temp.posn=-1L;
			write(wfd,&temp,sizeof(temp));
		}
	}
	Unlock_File(wfd);
}

/* find userid of logged in users */
long who_find(const char * username)
{
	int             who_fd;
	int             users_fd = -1;
	struct who      who;
	struct person   user;
	long 		where = -1;

	who_fd = who_open(O_RDONLY);
	if (who_fd < 0) return -1;

	users_fd = userdb_open(O_RDONLY);
	if (users_fd < 0) {
		close(who_fd);
		return -1;
	}

	while (read(who_fd, &who, sizeof(who)) > 0) {
		if (who.posn < 0) continue;
		if (who.pid  <= 0) continue;

		lseek(users_fd, who.posn, SEEK_SET);
		if (read(users_fd, &user, sizeof(user)) <= 0) continue;
		if (strcasecmp(user.name, username)==0) {
			where = who.posn;
			break;
		}
	}
	close(who_fd);
	close(users_fd);

	return where;
}
