#ifndef FOLDERS_H
#define FOLDERS_H

#include <stdint.h>

#define FOLDERFILE  STATEDIR"/folders.bb"
#define FOLNAMESIZE 	10	/* length of folder names */
#define TOPICSIZE 	30	/* length of the topic of the folder */

struct folder
{
	unsigned char status; /* status for people not in the same group */
	char name[FOLNAMESIZE+1];
	char topic[TOPICSIZE+1];
	int32_t first; /* Ref no of first message in the folder */
	int32_t last; /* Ref no of last message in the folder */
	char g_status; /* status for people in the same group */
	char groups; /* which groups g_status applies to */
	char spare[10];
};

/* folder.status
bit 	use
0	active
1	read by unregistered
2	wrte by unregistered
3	read by registered
4	wrte by registered
5	private folder
6	moderated folder
7	-
*/

int openfolderfile(int mode);
int nofolders(void);
void create_folder_file(void);
int foldernumber(const char *name);
int get_folder_entry(int file, struct folder *tmp);
int get_folder_number(struct folder *fol, int num);

#endif /* FOLDERS_H */
