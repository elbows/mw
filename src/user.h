#ifndef USER_H
#define USER_H

#include <stdint.h>

#define USERFILE STATEDIR"/users.bb"

#define NAMESIZE 	16	/* username */
#define PASSWDSIZE 	20	/* password (after encryption) */
#define REALNAMESIZE	30	/* real name */
#define CONTACTSIZE 	60	/* contact address */
#define DOINGSIZE	79	/* 'doing' user record field */

struct person 
{
	char name[NAMESIZE+1];
	char passwd[PASSWDSIZE+1];
	uint16_t pad0;
	int32_t lastlogout; 
	int32_t folders[2]; /* which folders are subscribed to */
#define SETALLLONG 0xFFFFFFFF
	/* person.status
	bit	use
	0	registered
	1	is a moderator
	2	is a superuser
	3	is banned
	4	messages on/off(1=off)
	5	inform on/off(1=off)
	6	-
	7	marked for deletion
	*/
	unsigned char status;
	uint8_t pad1;
	uint16_t special;
	int32_t lastread[64]; /* last message read in each folder */
	char realname[REALNAMESIZE+1];
	char contact[CONTACTSIZE+1];
	int32_t timeused;
	int32_t idletime;
	char groups;
	char doing[DOINGSIZE];
	int32_t dowhen; /* unix time of last doing update */
	int32_t timeout;

	unsigned char spare;
	unsigned char colour;
	uint16_t room;
	uint32_t chatprivs;
	uint32_t chatmode;
};

extern int userdb_open(int flags);
extern void userdb_write(struct person *record, int32_t *userposn);
extern void fetch_user(struct person *record, int32_t userposn);


#endif
