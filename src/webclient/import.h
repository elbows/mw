#ifndef IMPORT_H
#define IMPORT_H

#include <user.h>

int get_person(int file, struct person *tmp);
void update_user(struct person *record, int32_t userposn);
unsigned long cm_flags(unsigned long cm, unsigned long flags, int mode);
char *quotetext(const char *a);
void broadcast_onoffcode(int code, int method, const char *sourceuser, const char *reason);
void show_chatmodes(unsigned long cm, char *tmp, int flag);
void show_chatprivs(unsigned long cp, char *tmp, int flag);
char *remove_first_word(char *args) ;
void talk_send_to_room(const char *text, int channel, const char *type, int plural);
int is_old(struct person *usr, const char *name, int32_t *userposn);

#endif /* IMPORT_H */
