#include <files.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <strings.h>
#include <stdbool.h>
#include <ctype.h>

#include <talker_privs.h>
#include <ipc.h>
#include <perms.h>
#include <bb.h>
#include "import.h"

int incoming_pipe = -1;

#define min(a,b) a<b?a:b
#define max(a,b) a>b?a:b

int get_person(int file, struct person *tmp)
{
        int no;
        if (!(no=read(file,tmp,sizeof(*tmp))))
        {
                return(false);
        }else
        if (no<sizeof(*tmp))
        {
                printf("Problem in get_person: my uid=%d my euid=%d", getuid(), geteuid());
                return(false);
        }

        return(true);
}

void update_user(struct person *record, int32_t userposn)
{
        int outfile;

        outfile=userdb_open(O_RDWR|O_CREAT);
        /*Lock_File(outfile); */
        lseek(outfile,userposn,0);
        write(outfile,record,sizeof(*record));
        /*Unlock_File(outfile); */
        close(outfile);
}

/* chatmode flags, as in currently active modes */
unsigned long cm_flags(unsigned long cm, unsigned long flags, int mode)
{       
        if (mode==CM_MODE_CLEAR) return( cm & (~flags) );
        else    
        if (mode==CM_MODE_SET) return(cm | flags);
        else            
        if (mode==CM_MODE_ANY) {
                if ((cm&flags)>0) return(true); else return(false);
        } else  
        if (mode==CM_MODE_ALL) {
                if ((cm&flags)==flags) return(true); else return(false);
        }               
        return(0);
}

void broadcast_onoffcode(int code, int method, const char *sourceuser, const char *reason)
{
        char            logofftext[MAXTEXTLENGTH];

        /* create the broadcast string */
        snprintf(logofftext, MAXTEXTLENGTH-1, "%d,%d,%d,%s", code, method, 0, sourceuser);

        /* add an optional reason */
        if (reason != NULL)
        {
                char    reasontext[MAXTEXTLENGTH];
                snprintf(reasontext, MAXTEXTLENGTH - 1, "%s,%s", logofftext, reason);
                snprintf(logofftext, MAXTEXTLENGTH - 1, "%s", reasontext);
        }

        /* send the message */
        ipc_send_to_all(IPC_CHECKONOFF, logofftext);
}

void show_chatmodes(unsigned long cm, char *tmp, int flag)
{
        int i=0;

        if (cm & CM_ONCHAT) tmp[i++]='c'; else if (flag) tmp[i++]='-';
        if (cm & CM_GLOBAL) tmp[i++]='o'; else if (flag) tmp[i++]='-';
        if (cm & CM_PROTECTED) tmp[i++]='p'; else if (flag) tmp[i++]='-';
        if (cm & CM_FROZEN) tmp[i++]='f'; else if (flag) tmp[i++]='-';
        if (cm & CM_SPY) tmp[i++]='y'; else if (flag) tmp[i++]='-';
        if (cm & CM_VERBOSE) tmp[i++]='v'; else if (flag) tmp[i++]='-';
        if (cm & CM_STICKY) tmp[i++]='k'; else if (flag) tmp[i++]='-';
        if (cm & CM_GAG1) tmp[i++]='1'; else if (flag) tmp[i++]='-';
        if (cm & CM_GAG2) tmp[i++]='2'; else if (flag) tmp[i++]='-';
        if (cm & CM_GAG3) tmp[i++]='3'; else if (flag) tmp[i++]='-';
        if (cm & CM_GAG4) tmp[i++]='4'; else if (flag) tmp[i++]='-';
        if (cm & CM_GAG5) tmp[i++]='5'; else if (flag) tmp[i++]='-';
        if (cm & CM_GAG6) tmp[i++]='6'; else if (flag) tmp[i++]='-';
        tmp[i]=0;
}

void show_chatprivs(unsigned long cp, char *tmp, int flag)
{
        int i=0;

        if (cp & CP_CANRAW) tmp[i++]='R'; else if (flag) tmp[i++]='-';
        if (cp & CP_CANGAG) tmp[i++]='G'; else if (flag) tmp[i++]='-';
        if (cp & CP_CANZOD) tmp[i++]='Z'; else if (flag) tmp[i++]='-';
        if (cp & CP_CANMROD) tmp[i++]='M'; else if (flag) tmp[i++]='-';
        if (cp & CP_CANGLOBAL) tmp[i++]='O'; else if (flag) tmp[i++]='-';
        if (cp & CP_PROTECT) tmp[i++]='P'; else if (flag) tmp[i++]='-';
        if (cp & CP_FREEZE) tmp[i++]='F'; else if (flag) tmp[i++]='-';
        if (cp & CP_SUMMON) tmp[i++]='S'; else if (flag) tmp[i++]='-';
        if (cp & CP_SPY) tmp[i++]='Y'; else if (flag) tmp[i++]='-';
        if (cp & CP_SCRIPT) tmp[i++]='s'; else if (flag) tmp[i++]='-';
        if (cp & CP_ADVSCR) tmp[i++]='A'; else if (flag) tmp[i++]='-';
        if (cp & CP_DEVEL) tmp[i++]='D'; else if (flag) tmp[i++]='-';

        tmp[i]=0;
}

char *remove_first_word(char *args)
{
        char *ptr, *ptr2;

        ptr = args;
        while (ptr && *ptr && isspace(*ptr)) ptr++;
        if (ptr && *ptr)
        {
                ptr2 = strchr(ptr, ' ');
                if (ptr2 != NULL) ptr2++;
                if (ptr2 == NULL) return(NULL); else return(strdup(ptr2));
        } else return(NULL);
}

int is_old(struct person *usr, const char *name, int32_t *userposn)
{
        int file,found=false;

        if (access(USERFILE,00)) return(false);
        file=userdb_open(O_RDONLY);
        while (!found && get_person(file,usr))
                if (!strcasecmp(usr->name,name) && !u_del(usr->status))
                        found=true;
        if (found)
                *userposn=lseek(file,0,1)-sizeof(struct person);
        close(file);
        return(found);
}

extern int userposn;

void talk_send_to_room(const char * text, int channel, const char * type, int plural) {
	ipc_message_t * msg = ipcmsg_create(IPC_SAYTOROOM, userposn);
	ipcmsg_destination(msg, channel);
	json_t * j = json_init(NULL);
	json_addstring(j, "text", text);
	json_addstring(j, "type", type);
	if (plural > -1) json_addint(j, "plural", plural);
	ipcmsg_json_encode(msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
}
